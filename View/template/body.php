<!-- MENU SIDEBAR-->
<aside id="menuSideBar" class="menu-sidebar d-none d-lg-block">
    <div id="logo" class="logo">
        <a href="/formulario">
            <img class="imagemLogo" src="/images/icon/logo_preta.png" width="200px" height="50px" alt="Logotipo" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <?php if (isset($menu)) include $menu ?>
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid header-shadow">
                <div class="header-mobile-inner">
                    <a id="logo" class="logo" href="/formulario">
                        <img class="imagemLogo" src="/images/icon/logo_preta.png" width="200px" height="50px" alt="Logotipo" />
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <?php if (isset($menu)) include $menu ?>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER MOBILE-->
    <!-- HEADER DESKTOP-->
    <header class="header-desktop">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap">
                    <form class="form-header" action="" method="POST">
                        
                    </form>
                    <div class="header-button">
                        <div class="noti-wrap">
                        </div>
                        <div class="account-wrap">
                            <div class="account-item clearfix js-item-menu">
                                <?php if (isset($account)) include_once $account ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER DESKTOP-->

    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div id="main-content" class="container-fluid">
                <?php if (isset($content)) include_once $content ?>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->
    <br>

</div>