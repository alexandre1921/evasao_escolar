
<div class="modal fade bd-example-modal-lg" id="<?=$modalArray['id']?>" tabindex="-1" role="dialog" aria-labelledby="<?=$modalArray['id']?>Label" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">
   
        <div id="modal" class="modal-content modal-lg">
            <div class="modal-header">
                <h5 class="modal-title" id="<?=$modalArray['id']?>Label"><?=$modalArray['title']?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar" alt="fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">    
                <?php if(isset($modalArray['body'])) include $modalArray['body'] ?>
            </div>
            <div class="modal-footer">
                <?php if(isset($modalArray['footer'])) include $modalArray['footer'] ?>
            </div>
            
        </div>
    </div>
</div>
