<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active show" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="true">Conta</a>
        <a class="nav-item nav-link" id="nav-adress-tab" data-toggle="tab" href="#nav-adress" role="tab" aria-controls="nav-adress" aria-selected="false">Endereço</a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contato</a>
    </div>
</nav>
<div class="tab-content pl-3 pt-2" id="nav-tabContent">
    <div class="tab-pane fade active show" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <?php 
            if(isset($modalArray['body'])) include_once $modalArray['body'];
            require $_SERVER['DOCUMENT_ROOT'].'/View/forms/user.php';
        ?>
    </div>
    <div class="tab-pane fade" id="nav-adress" role="tabpanel" aria-labelledby="nav-adress-tab">
        <?php 
            if(isset($modalArray['body'])) include_once $modalArray['body'];
            require $_SERVER['DOCUMENT_ROOT'].'/View/forms/adress.php';
        ?>
    </div>
    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <?php 
            if(isset($modalArray['body'])) include_once $modalArray['body'];
            require $_SERVER['DOCUMENT_ROOT'].'/View/forms/contact.php';  
        ?> 
    </div>
</div>