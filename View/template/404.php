<div id="conteudo" class="page-content--bge5">
    <div class="container">
        <div class="login-wrap">
            <div id="login-content" class="login-content">
                <div class="login-logo">
                    <a href="/">
                        <img class="imagemLogo" src="/images/icon/logo_preta.png" width="200px" height="50px" alt="Logotipo">
                    </a>
                </div>
                <div class="login-form">
                    <div class="row justify-content-center align-self-center">
                        <div class="col mt-4">
                            <h2 class="pb-2 display-5">Página não encontrada :/</h2>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>