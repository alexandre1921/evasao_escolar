<div class="image">
    <img class="mini-session img-put" src="<?=$UserSession['profile_image']?>" alt="<?=$_SESSION['name_user']?>" />
</div>
<div class="content">
    <a class="js-acc-btn name-session" href="#"><?=$_SESSION['name_user']?></a>
</div>
<div id="conta" class="account-dropdown js-dropdown">
    <div class="info clearfix">
        <div class="image">
            <a href="#">
                <img class="mini-session img-put" src="<?=$UserSession['profile_image']?>" alt="<?=$_SESSION['name_user']?>"/>
            </a>
        </div>
        <div class="content">
            <h5 class="name">
                <a class="name-session" href="#"><?=$_SESSION['name_user']?></a>
            </h5>
            <span class="email-session" class="email"><?=$_SESSION['email']?></span>
        </div>
    </div>
    <div class="account-dropdown__body">
        <div class="account-dropdown__item">
            <a href="#" id="user-modal-a" data-toggle="modal" data-target="#user-modal">
                <i class="zmdi zmdi-account" ></i>Minha conta</a>
        </div>
    </div>
    <div class="account-dropdown__footer">
        <a href="/logout">
            <i class="zmdi zmdi-power"></i>Sair</a>
    </div>
</div>