<div class="row">
    <?php include_once "botaoAcess.php" ?>
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-1 m-b-20"><?=isset($tableTitle)?$tableTitle:''?></h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left"> -->
                <!-- <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Today</option>
                        <option value="">3 Days</option>
                        <option value="">1 Week</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters
                </button> -->
            <!-- </div> -->
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn" data-toggle="modal" data-target="#object-modal">
                    <i class="zmdi zmdi-plus"></i>add
                </button>
            </div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2" >
                <thead id="th-lines" >
                    
                </thead>
                <tbody id="tr-lines">
                    
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
        <center>
            <span class="erro" id="message"></span>    
        </center>
    </div>
</div>