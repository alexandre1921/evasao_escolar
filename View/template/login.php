<div id="conteudo" class="page-content--bge5">
    <div class="container">
        <div class="login-wrap">
            <div id="login-content" class="login-content">
                <div class="login-logo">
                    <a href="#">
                        <img class="imagemLogo" src="/images/icon/logo_preta.png" width="200px" height="50px" alt="CoolAdmin">
                    </a>
                </div>
                <div class="login-form">
                    <?php require_once $_SERVER['DOCUMENT_ROOT'].'/View/forms/login.php' ?>
                    <div class="register-link">
                        <p>
                            Ainda não tem uma conta?
                            <a id="register" href="#" data-toggle="modal" data-target="#object-modal">Cadastre-se aqui!</a>
                        </p>
                        <p>
                            <a id="recover" href="#" data-toggle="modal" data-target="#recover-modal">Esqueceu a senha?</a>
                        </p>
                    </div>
                    <br>
                    <?php require_once $_SERVER['DOCUMENT_ROOT'].'/View/template/botaoAcess.php' ?>
                </div> 
            </div>
        </div>
    </div>
</div>
