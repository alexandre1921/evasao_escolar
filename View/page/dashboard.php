<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title><?=isset($title)?$title:''?></title>

    <link rel="shortcut icon" href="/images/icon/mini_logo.ico">
    <!-- Fontfaces CSS-->
    <link href="/css/font-face.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/css/theme.css" rel="stylesheet" media="all">
</head>

<body class="animsition">
    <div id="first-body" class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
    <div class="question-mark">
        <div class="black col-md-8">
            <img id="help-image" style="display:none;" class="help-image hide col-md-8" src="<?=isset($QuestionMarkImg)?$QuestionMarkImg:'/images/help/login.jpg'?>">
        </div>
        <a class="help-a" href="#">
            <i class="far fa-question-circle fa-3x help" alt="Precisa de ajuda? clique aqui"></i>
        </a>
        <?php include_once $_SERVER['DOCUMENT_ROOT'].'/View/template/botaoAcess.php' ?>
    </div>
    <div style="display:none;" id="help-image-fade" class="modal-backdrop fade show hide-fade"></div>
    <div class="page-wrapper" id="pgInteira">
        <?php include_once $page_wrapper ?>
    </div>
    
    <?php
        if (isset($modal))
            foreach ($modal as $modalArray) 
                include $modalArray['modal'];
    ?>

    <!-- Jquery JS -->
    <script src="/vendor/jquery-3.2.1.min.js"></script>
    <script src="/vendor/jquery.mask.js"></script>
    <!-- Bootstrap JS -->
    <script src="/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS -->
    <script src="/vendor/animsition/animsition.min.js"></script>
    <script src="/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/vendor/select2/select2.min.js"></script>
    <!-- Main JS -->
    <script src="/js/main.js"></script>
    <script src="/js/dashboard.js"></script>
    <script src="/vendor/compress/compress.js"></script>
    <script src="/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
    <!-- Php generated JS -->
    <?=isset($jScript)?$jScript:''?>

    <script src="/js/acessibilidade.js"></script>
</body>
</html>
<!-- end document-->
