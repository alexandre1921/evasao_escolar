<form id="weka-form" class="needs-validation" novalidate>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="lowerBoundMinSupport">Suporte mínimo</label>
      <input step="0.01" value="0.1" min="0" max="1" name="lowerBoundMinSupport" id="lowerBoundMinSupport" type="number" placeholder="Suporte mínimo" class="form-control" alt="Insira o lowerBoundMinSupport">
      <small class="form-text text-muted">Insira o valor para o suporte mínimo.</small>
      <div class="invalid-feedback">
        Por favor, insira o suporte mínimo!
      </div>
    </div>
    <div class="form-group col-md-6">
      <label for="upperBoundMinSupport">Suporte máximo</label>
      <input step="0.01" value="1.0" min="0" max="1" name="upperBoundMinSupport" id="upperBoundMinSupport" type="number" placeholder="Limite superior para o suporte" class="form-control" alt="Insira o upperBoundMinSupport">
      <small class="form-text text-muted">Insira o valor para o suporte máximo.</small>
      <div class="invalid-feedback">
        Por favor, insira o limite superior para o suporte!
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="numRules">Número de regras</label>
      <input value="10" min="1" name="numRules" id="numRules" type="number" placeholder="Número de regras" class="form-control" alt="Insira o numRules">
      <small class="form-text text-muted">Insira o valor máximo de regras para serem apresentadas.</small>
      <div class="invalid-feedback">
        Por favor, insira o número de regras!
      </div>
    </div>
    <div class="form-group col-md-4 mb-3">
      <label for="metricType">Tipo de métrica</label><br>
      <select name="metricType" id="metricType" style="width:100%;">
        <option value="Confidence" selected>Confiança</option> 
        <option value="Lift">Lift</option>
        <option value="Leverage">Leverage</option> 
      </select>
      <small class="form-text text-muted">Selecione a medida de interesse que irá ordenar as regras.</small>
    </div>
    <div class="form-group col-md-4">
      <label for="minMetric">Métrica mínima</label>
      <input step=0.01 value="0.9" min="0" max="1" name="minMetric" id="minMetric" type="number" placeholder="Métrica mínima" class="form-control" alt="Insira o minMetric">
      <div class="invalid-feedback">
        Por favor, insira o métrica mínima!
      </div>
      <small class="form-text text-muted">Insira o valor mínimo para a métrica escolhida.</small>
    </div>
  </div>
  <input type="submit" value="Enviar" class="btn btn-primary">
  <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
</form>