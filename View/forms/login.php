<form id="login-form" action="" method="post" class="needs-validation" novalidate>
    <div class="form-group">
        <label>Email</label>
        <input id="login" class="au-input au-input--full form-control" required type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" placeholder="Email" minlength="10" maxlength="45" autofocus alt="Insira o seu login">
        <div class="invalid-feedback">
            Por favor, insira seu e-mail!
        </div>
    </div>
    <div class="form-group">
        <label>Senha</label>
        <input id="password" class="au-input au-input--full form-control" required type="password" name="password" placeholder="Senha" minlength="4" maxlength="60" alt="Insira a sua senha">
        <div class="invalid-feedback">
            Por favor, insira sua senha!
        </div>
    </div>
    
    <button id="botao" class="au-btn au-btn--block au-btn--green m-b-20" type="submit" alt="Enviar">Entrar</button>
    <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
    <center>
        <span class="erro" id="message"></span>    
    </center>
    </div>
</form>