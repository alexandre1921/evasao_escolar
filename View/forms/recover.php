<form id="recover-form" class="needs-validation" novalidate>
    <div class="form-group">
        <div class="col-md-12 mb-3">
            <label>Email</label>
            <input id="login" class="au-input au-input--full form-control" required type="email" name="email" placeholder="Email" minlength="10" maxlength="45" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autofocus alt="Insira o seu login">
            <div class="invalid-feedback">
                Por favor, insira seu e-mail cadastrado no sistema!
            </div>
        </div>
    </div>
    <center>
        <span class="erro" id="erro"></span>    
    </center>
    <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
</form>
<form id="verification-code-form" novalidate style="display:none">
    <div class="form-group">
        <div class="col-md-12 mb-3">
            <label>Código de verificação</label>
            <input id="verification_code" class="au-input au-input--full form-control" required type="text" name="verification_code" minlength="60" maxlength="60" placeholder="Código" maxlength="60" autofocus alt="Insira o seu código">
            <div class="invalid-feedback">
                Por favor, insira um código válido!
            </div> 
        </div> 
    </div>
    <center>
        <span class="erro" id="erro"></span>    
    </center>
    <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
</form>
<form id="change-password-form" novalidate style="display:none">
    <div class="form-group">
        <div class="col-md-12 mb-3">
            <label>Nova senha</label>
            <input id="password" class="au-input au-input--full form-control" required type="password" name="password" placeholder="Senha" minlength="4" maxlength="60" autofocus alt="Insira a sua senha">
            <div class="invalid-feedback">
                Por favor, insira uma senha de no mínimo 4 caracteres!
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <label>Confirme a senha</label>
            <input id="confirm_password" class="au-input au-input--full form-control" required type="password" name="confirm_password" placeholder="Senha novamente" minlength="4" maxlength="60" autofocus alt="Insira a sua senha novamente">
            <div class="invalid-feedback">
                Por favor, confirme a senha!
            </div>
        </div>
    </div>
    <center>
        <span class="erro" id="erro"></span>    
    </center>
    <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
</form>
<div id="message">
</div>