<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="form-group col-md-10">
      <label for="name_course">Nome do curso</label>
      <input type="text" class="form-control" id="name_course_<?=$modalArray['id']?>_course" required pattern="[a-zA-Z\u00C0-\u00FF\s]+$" name="name_course" minlength="3" maxlength="45" alt="Insira o nome do curso">
      <div class="invalid-feedback">
        Por favor, insira o nome do curso!
      </div>
    </div>
    <div class="form-group col-md-2">
      <label for="duration">Duração</label>
      <input type="number" class="form-control" id="duration_<?=$modalArray['id']?>_course" required name="duration" min="1" max="16" alt="Insira a duração em semestres">
      <small id="emailHelp" class="form-text text-muted">Em semestres.</small>
      <div class="invalid-feedback">
        Por favor, insira a duração do curso em semestres!
      </div>
    </div>
  </div>
  <input type="hidden" id="id_course_<?=$modalArray['id']?>_course">
  <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
    <center>
        <span class="erro" id="message"></span>    
    </center>
</form>