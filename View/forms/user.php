<?php
  $user_modal=$modalArray['id']=='user-modal';
?>

<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-12 mb-3">
      <label for="name_user">Nome completo</label>
      <input autofocus name="name_user" type="text" class="form-control" id="name_user_<?=$modalArray['id']?>_user" 
        placeholder="Nome" alt="nome" pattern="[a-zA-Z\u00C0-\u00FF\s]+$" maxlength="50" minlength="2" required <?=$user_modal?'value="'.$_SESSION['name_user'].'"':''?>>
        <div class="invalid-feedback">
            Por favor, insira seu nome completo!
        </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-<?=!$user_modal && ($_SESSION['user_type']===$_ENV['ADMIN_LINKS'] || $_SESSION['user_type']===$_ENV['ASSISTANT_LINKS'])?'8':'12'?> mb-3">
    <label for="email">Email</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend">@</span>
            </div>
            <input name="email" type="text" class="form-control" id="email_<?=$modalArray['id']?>_user" placeholder="Email" alt="email" minlength="10" maxlength="45" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required <?=$user_modal?'value="'.$_SESSION['email'].'"':''?>>
            <div class="invalid-feedback">
              Por favor, insira seu e-mail!
            </div>
        </div>
    </div>
    <?=
      !$user_modal && ($_SESSION['user_type']===$_ENV['ADMIN_LINKS'] || $_SESSION['user_type']===$_ENV['ASSISTANT_LINKS'])?
      '<div class="col-md-4 mb-3">
        <label for="id_user_type">Tipo</label>
        <select name="id_user_type" id="id_user_type_'.$modalArray['id'].'_user" class="form-control" value="'.$_SESSION['id_user_type'].'"'.' required>
        </select>
      </div>':''
    ?>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="password">Senha</label>
      <input name="password" type="password" class="form-control" id="password_<?=$modalArray['id']?>_user" placeholder="Senha" alt="Senha" minlength="4" maxlength="60" <?=($_SESSION['user_type']===$_ENV['UNLOGGED_LINKS'])||!$user_modal?'required':''?>>
      <div class="invalid-feedback">
        Por favor, insira sua senha!
      </div>
    </div>
    <div class="col-md-6 mb-3">
      <label for="password">Confirmar senha</label>
      <input name="confirm_password" type="password" class="form-control" id="confirm_password_<?=$modalArray['id']?>_user" placeholder="Senha" alt="Senha" minlength="4" maxlength="60" <?=($_SESSION['user_type']===$_ENV['UNLOGGED_LINKS'])||!$user_modal?'required':''?>>
      <div class="invalid-feedback">
        Por favor, confirme sua senha!
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-3 mb-3">
      <label for="birth_date">Data de Nascimento</label>
      <input name="birth_date" type="date" class="form-control" id="birth_date_<?=$modalArray['id']?>_user" alt="Data de Nascimento" min="1919-01-01" required <?=$user_modal?'value="'.$_SESSION['birth_date'].'"':''?>>
      <div class="invalid-feedback">
        Por favor, insira sua data de nascimento!
      </div>
    </div>
    <div class="col-md-9 mb-3">
      <label for="id_course">Curso</label>
      <select name="id_course" <?=$_SESSION['user_type']===$_ENV['UNLOGGED_LINKS']?'required':''?> id="id_course_<?=$modalArray['id']?>_user" alt="Curso" class="form-control" <?=$user_modal?'value="'.$_SESSION['id_course'].'"':''?>>
      </select>
      <div class="invalid-feedback">
        Por favor, selecione um curso!
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-3 mb-3 mt-3">
      <label for="entry_year">Ano de Ingresso</label>
      <input name="entry_year" <?=$_SESSION['user_type']===$_ENV['UNLOGGED_LINKS']?'required':''?> type="number" class="form-control" id="entry_year_<?=$modalArray['id']?>_user" alt="Ano de ingresso" min="2010" <?=$user_modal?'value="'.$_SESSION['entry_year'].'"':''?>>
      <div class="invalid-feedback">
        Por favor, insira seu ano de ingresso!
      </div>
    </div>
    <?=
       !$user_modal && ($_SESSION['user_type']===$_ENV['ADMIN_LINKS'] || $_SESSION['user_type']===$_ENV['ASSISTANT_LINKS'])?
      '<div class="col-md-3 mb-3 mt-3">
        <label for="egress_year">Ano de Egresso</label>
        <input name="egress_year" type="number" class="form-control" id="egress_year_'.$modalArray['id'].'_user" alt="Ano de egresso" min="2019">
      </div>':''
    ?>
    <div class="col-md-<?=!$user_modal && ($_SESSION['user_type']===$_ENV['ADMIN_LINKS'] || $_SESSION['user_type']===$_ENV['ASSISTANT_LINKS'])?'4':'7'?> mb-3 mt-3">
        <label class="label-custom-file">Foto de perfil</label>
        <div class="custom-file">
          <input name="profile_image" type="file" accept="image/*" alt="Foto de Perfil" class="custom-file-input" id="profile_image_<?=$modalArray['id']?>_user">
          <label class="custom-file-label" for="profile_image">Escolha a foto</label>
          <div class="invalid-feedback">
            Por favor, insira sua foto de perfil! (máximo 12 mb)
          </div>
        </div>
    </div>
    <div class="col-md-2">
      <label style="margin-left:17px">Preview</label>
      <center>
          <img src="<?=($_SESSION['user_type']!==$_ENV['UNLOGGED_LINKS'])&&$user_modal?$UserSession['profile_image']:'/images/icon/user.svg'?>" class="preview-image" id="preview" style="margin-left:20px">
      </center>
    </div>
  </div>
  <?php
    if ($_SESSION['user_type']!==$_ENV['UNLOGGED_LINKS'])
      echo'<input name="id_user_type" type="hidden" id="id_user_type_'.$modalArray['id'].'_user" '.($user_modal?'value="'.$_SESSION['id_user_type'].'"':'').'>'.
      '<input name="id_user" type="hidden" id="id_user_'.$modalArray['id'].'_user" '.($user_modal?'value="'.$_SESSION['id_user'].'"':'').'>';
  ?>
  <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
  <center>
      <span class="erro" id="message"></span>    
  </center>
</form>