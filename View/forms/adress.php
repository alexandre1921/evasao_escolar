<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="city_<?=$modalArray['id']?>_adress">Cidade</label>
      <input name="city" type="text" class="form-control" id="city_<?=$modalArray['id']?>_adress" pattern="[a-zA-Z\u00C0-\u00FF\s]+$" placeholder="Cidade" maxlength="30" minlength="2" alt="Insira sua cidade">
      <div class="invalid-feedback">
        Por favor, insira sua cidade!
      </div>
    </div>
    <div class="col-md-6 mb-3">
      <label for="neighborhood_<?=$modalArray['id']?>_adress">Bairro</label>
      <input name="neighborhood" type="text" class="form-control" id="neighborhood_<?=$modalArray['id']?>_adress" pattern="[a-zA-Z\u00C0-\u00FF\s]+$" placeholder="Bairro" maxlength="20" minlength="2" alt="Insira seu bairro">
    </div>
    <div class="invalid-feedback">
      Por favor, insira seu bairro!
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="street_<?=$modalArray['id']?>_adress">Rua</label>
      <input name="street" type="text" class="form-control" id="street_<?=$modalArray['id']?>_adress" pattern="[a-zA-Z\u00C0-\u00FF\s]+$" placeholder="Rua" maxlength="20" minlength="2" alt="Insira sua rua">
      <div class="invalid-feedback">
        Por favor, insira sua rua!
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="number_<?=$modalArray['id']?>_adress">Número</label>
      <input name="number" type="text" class="form-control" id="number_<?=$modalArray['id']?>_adress" placeholder="Número" pattern="[0-9]" maxlength="6" minlength="1" alt="Insira seu número">
      <div class="invalid-feedback">
        Por favor, insira o número!
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="complement_<?=$modalArray['id']?>_adress">Complemento</label>
      <input name="complement" type="text" class="form-control" id="complement_<?=$modalArray['id']?>_adress" pattern="[a-zA-Z\u00C0-\u00FF\s]+$" placeholder="Complemento" maxlength="15" alt="Insira o complemento">
      <div class="invalid-feedback">
        Por favor, insira o complemento!
      </div>
    </div>
  </div>
  <input value="" name="id_adress" type="hidden" id="id_adress_<?=$modalArray['id']?>_adress">
  <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
    <center>
        <span class="erro" id="message"></span>    
    </center>
</form>