<form class="needs-validation" novalidate>
  <div class="form-row">
    <div class="col-md-6 mb-1">
      <label for="telephone_<?=$modalArray['id']?>_contact">Telefone</label>
      <input name="telephone" type="text" class="form-control" id="telephone_<?=$modalArray['id']?>_contact" placeholder="Telefone" pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$" maxlength="20" alt="Insira seu telefone">
      <div class="invalid-feedback">
        Por favor, insira seu telefone!
      </div>
    </div>
    <div class="col-md-6 mb-1">
      <label for="cellphone_<?=$modalArray['id']?>_contact">Celular</label>
      <input name="cellphone" type="text" class="form-control" id="cellphone_<?=$modalArray['id']?>_contact" placeholder="Celular" pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$" maxlength="20" alt="Insira seu celular">
      <div class="invalid-feedback">
        Por favor, insira seu celular!
      </div>
    </div>
  </div>
  <input value="" name="id_contact" type="hidden" id="id_contact_<?=$modalArray['id']?>_contact">
  <input name="id_user" type="hidden" id="id_user_'.<?=$modalArray['id']?>.'_user" value="<?=$_SESSION['id_user']?>">
  <div class="loading-anime" style="display:none"><div class="modal-backdrop fade show"></div><div class="animsition-loading"></div></div>
    <center>
        <span class="erro" id="message"></span>    
    </center>
</form>