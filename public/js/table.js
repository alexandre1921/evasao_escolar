"use strict";

const th = (json) => `
<tr>
${(()=>{
    let var_string='';
    for (var [key, value] of Object.entries(json.format)) {
        var_string+=`<th ${typeof json.format[key].hidden!=='undefined'?(json.format[key].hidden?`style="display:none;"`:''):''}>
            ${json.format[key].name}
        </th>`;
    }
    return var_string
})()}
</tr>
`;

const trActions = (id,objectName) =>`
<div class="table-data-feature">
    ${objectName!='user'?`
    <button value="${id}" type="button" class="item item-edit" data-placement="top" title="Edit" data-toggle="modal" data-target="#object-modal">
        <i class="zmdi zmdi-edit"></i>
    </button>`:`
    <button value="${id}" class="item item-more" data-placement="top" title="More" data-toggle="modal" data-target="#more-modal">
        <i class="zmdi zmdi-more"></i>
    </button>`}
</div>
`;

const image = (profile_image_type,profile_image) => `
<img class="mini-image-table" src="${profile_image_type!=undefined?`${profile_image_type}${profile_image}`:''}">
`;

const tr = (json,objectName) => {
    let result_result='';
    for (var [json_key, json_value] of Object.entries(json.response)) {
        result_result+=`<tr class="tr-shadow">
            ${(()=>{
                let var_string='';
                for (var [key, value] of Object.entries(json_value)) {
                    var_string+=`
                    <td ${typeof json.format[key].hidden!=='undefined'?(json.format[key].hidden?`style="display:none;"`:''):''}>
                        ${(() => {
                            let result='';
                            switch (key) {
                                case 'profile_image':
                                    result=image(json_value['profile_image_type'],value);
                                break;
                                case 'status_course':
                                    if (value==1)
                                        result=`<span class="status--process status" value="${json_value['id_course']}">Desativar</span>`;//entries[0]
                                    else
                                        if (value==0)
                                            result=`<span class="status--denied status" value="${json_value['id_course']}">Ativar</span>`;
                                break;
                                case 'status_user':
                                    if (value==1)
                                        result=`<span class="status--process status" value="${json_value['id_user']}">Desativar</span>`;//entries[0]
                                    else
                                        if (value==0)
                                            result=`<span class="status--denied status" value="${json_value['id_user']}">Ativar</span>`;
                                break;
                                case 'description':
                                    result=`<div class="dropdown">
                                    <span id="span_user_type_${json_value['id_user']}" class="btn dropdown-toggle role user`
                                    switch (value.toUpperCase()) {
                                        case 'ALUNO':
                                            result+='role user';
                                        break;
                                        case 'ASSISTENTE':
                                            result+='role member';
                                        break;
                                        case 'ADMIN':
                                            result+='role admin';
                                        break;
                                    }
                                    result+=`" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${value}</span>
                                        <div title="${json_value['id_user']}" class="dropdown-menu menuUser" aria-labelledby="dropdownMenuButton">
                                            ${listUserType.response.map(value=>`<a title="${value.id_user_type}" class="dropdown-item" href="#">${value.description}</a>`).join('')}
                                        </div>
                                    </div>`;
                                break;
                                default:
                                    result=value;
                            }
                            return result;
                        })()}
                    </td>`;
                }
                var_string+=`<td>${trActions(json_value[`id_${objectName}`],objectName)}</td>`;
                return var_string;
            })()}
        </tr>`;
        result_result+='<tr class="spacer"></tr>';
    }
    return result_result
};

var objectState={};
function list(json,objectName,types={}){
    if (json.success==true){
        $('#th-lines').html(th(json));
        $('#tr-lines').html(tr(json,objectName));
        $(".item-edit").on("click",function (event) {
            $("#object-modal form")[0].reset();
            $("#object-modal form").attr("method", "PUT");
            let line=json.response.find(value => value[`id_${objectName}`]==this.value ? value:false);
            line=Object.entries(line);
            objectState={};
            for (var [key, value] of line)
                objectState[key]=$('<textarea/>').html(value).text();
            $(`#id_${objectName}_object-modal_${objectName}`).attr("value", line[0][1]);
            line.map(valueA => {
                let input=$(`#${valueA[0]}_object-modal_${objectName}`);
                if (input.length>0 && valueA[0]!='profile_image')
                    input.val($('<textarea/>').html(valueA[1]).text());
            });
        });
        $('.dropdown-item').on("click",function (event) {
            let lineObject=json.response.find(value => value[`id_${objectName}`]==this.parentElement.title ? value:false);
            let send_json=Object.assign(lineObject, {id_user_type:this.title});
            let tag=this;
            $.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: '/user',
                type: 'PUT',
                data: JSON.stringify(send_json),
                success: function(result) {
                    if (result.success==true){
                        $.ajax({
                            contentType: 'application/x-www-form-urlencoded',
                            url: `/ajax/user`,
                            type: 'POST',
                            data: JSON.stringify({
                                request:{
                                    print:true,
                                    criteria:'all',
                                }
                            }),
                            success: function(result) {
                                if (result.success==true){
                                    jsonList.response=result.response;
                                    list(jsonList,objectName);
                                }
                            }
                        });
                    }else{
                        showMessage("#user-modal #message",result);
                    }
                }
            }).done(function() {
                $('#user-modal .loading-anime').hide();
            });
        });
        $(".item-more").on("click",function (event) {
            let lineObject=json.response.find(value => value[`id_${objectName}`]==this.value ? value:false);
            $("#line-to-show").html(()=>{
                let line_string='<table width="100%">';
                let i=0;
                for (var [key, value] of Object.entries(lineObject)) {
                    if (key!='profile_image_type'){
                        i++;
                        value=HtmlEncode(value);
                        line_string+=`<tr>
                            <td><b>${i}º</b></td> 
                            <td>${json.format[key].name}</td> 
                            <td>${
                                (() => {
                                    let result='';
                                    switch (key) {
                                        case 'profile_image':
                                            result=image(lineObject['profile_image_type'],lineObject['profile_image']);
                                        break;
                                        case 'email_status':
                                            if (parseInt(value)==1)
                                                result=`<span class="status--process status">Ativado</span>`;
                                            else
                                                result=`<span class="status--denied status">Desativado</span>`;
                                        break;
                                        case 'status_user':
                                            if (parseInt(value)==1)
                                                result=`<span class="status--process status">Ativado</span>`;
                                            else
                                                result=`<span class="status--denied status">Desativado</span>`;
                                        break;
                                        case 'number':
                                            if (value!='0')
                                                result='Não inserido';
                                            else
                                                result=value;
                                        break;
                                        case 'egress_year':
                                            if (value==''||value=='0')
                                                result='Não egressou';
                                            else
                                                result=value;
                                        break;
                                        default:
                                            if (value=='')
                                                result='Não inserido';
                                            else
                                                result=value;
                                    }
                                    return result;
                                })()
                            }</td>
                        </tr>`
                    }
                }
                line_string+='</table>';
                return line_string;
            });
        });
        $(".au-btn.au-btn-icon.au-btn--green.au-btn").on("click",function(event) {
            $("#object-modal form").attr("method", "POST");
            if (objectName=="user"){
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: '/ajax/course',
                    type: 'POST',
                    data: JSON.stringify({request:{print:true,criteria:'activated'}}),
                    success: function(result) {
                        if (result.success==true){
                            createOption(result,'#id_course_object-modal_user',value=>`<option value="${value.id_course}">${value.name_course}</option>`);
                        }
                    }
                });
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: '/ajax/user_type',
                    type: 'POST',
                    data: JSON.stringify({request:{print:true,criteria:'all'}}),
                    success: function(result) {
                        if (result.success==true){
                            createOption(result,'#id_user_type_object-modal_user',value=>`<option value="${value.id_user_type}">${value.description} | Tipo ${value.type}</option>`);
                        }else{
                            showMessage("#object-modal #message",result,'danger',5000);
                        }
                    }
                });
            }
        });
        $("span.status").on("click",function (event) {
            let objectName=$("#object-modal .send").val();
            let status,Nclass,Tstatus,span,changeTo;
            span=this;
            Nclass=span.getAttribute("class");
            if (Nclass.search("denied")!=-1){
                changeTo='process';
                Tstatus='Desativar';
                status=1;
            }else if (Nclass.search("process")!=-1){
                changeTo='denied';
                Tstatus='Ativar';
                status=0;
            }else
                status=-1;
            if (status!=-1){
                let line=jsonList.response.find(value => value[`id_${objectName}`]==span.getAttribute("value") ? value:false);
                line=Object.entries(line);
                objectState={};
                for (var [key, value] of line)
                    objectState[key]=value;
                objectState[`status_${objectName}`]=status;
                $('#object-modal .loading-anime').show();
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: `/${objectName}/status`,
                    type: 'PUT',
                    data: JSON.stringify(objectState),
                    success: function(result) {
                        if (result.success==true){
                            span.className=`status--${changeTo} status`;
                            span.innerHTML=Tstatus;
                        }else{
                            showMessage("#main-content #message",result,'danger',2000);
                        }
                    }
                }).done(function() {
                    $('#object-modal .loading-anime').hide();
                });
            }
        });
    }
}

$(document).ready(function () {
    $("#object-modal input, #object-modal select").on("keyup change",function(event) {
        let id=this.id.replace(/_object-modal_.*/g,'');
        if (this.type=='file'){
            getCompressedImage(this,value=>{
                $('#object-modal #preview').attr('src',`${value.prefix}${value.data}`);
                objectState['profile_image_type']=value.prefix;
                objectState[id]=value.data;
            });
        }else{
            objectState[id]=this.value;
        }
    });
    $("#object-modal .send").on("click",function(event) {
        $("#object-modal form").submit();
    });

    $("#object-modal form").on("submit",function(event) {
        event.preventDefault();
        let objectName=$("#object-modal .send").val();
        let method=$("#object-modal form").attr("method");
        if ($("#object-modal form")[0].checkValidity()){
            $('#object-modal .loading-anime').show();
            if (objectName=='user')
                Object.assign(userState, {profile_image:svg_image,profile_image_type:svg_image_type});
            $.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: `/${objectName}`,
                type: method,
                data: JSON.stringify(objectState),
                success: function(result) {
                    if (result.success==true){
                        $(".dismiss").click();
                        $("#object-modal #message").html("");
                        $("#object-modal form").removeClass('was-validated');
                        $.ajax({
                            contentType: 'application/x-www-form-urlencoded',
                            url: `/ajax/${objectName}`,
                            type: 'POST',
                            data: JSON.stringify({
                                request:{
                                    print:true,
                                    criteria:'all',
                                }
                            }),
                            success: function(result) {
                                if (result.success==true){
                                    console.log('result');
                                    jsonList.response=result.response;
                                    list(jsonList,objectName);
                                }
                            }
                        }).done(function() {
                            $('#object-modal .loading-anime').hide();
                        });
                    }else{
                        showMessage("#object-modal #message",result);
                    }
                }
            }).done(function() {
                $('#object-modal .loading-anime').hide();
            });
        }
    });
});