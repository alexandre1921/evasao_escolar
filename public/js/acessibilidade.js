"use strict";

var body = document.querySelectorAll('body');
var divs = document.querySelectorAll('div');
var button = document.querySelectorAll('button');
var header = document.querySelectorAll('header');
var aside = document.querySelectorAll('aside');
var table = document.querySelectorAll('tbody');
var small = document.querySelectorAll('small');
var ul = document.querySelectorAll('ul');
var span = document.querySelectorAll('span');
var pre = document.querySelectorAll('pre');
var h4 = document.querySelectorAll('h4');
var h5 = document.querySelectorAll('h5');
var links = $('a:not(.help-a)');
var inputs = document.querySelectorAll('input');
var selects = document.querySelectorAll('select');
var label = document.querySelectorAll('label');
var p = document.querySelectorAll('p');
var td = document.querySelectorAll('tr');
var tagI = document.querySelectorAll('i');
var li = document.querySelectorAll('#select2-property-9b-results li'); 
var dropdown=document.querySelector("#dropdown-menu-access");
function altoContraste(a){
    for (let i = 0; i < body.length; i++) {
        body[i].classList.toggle("contrast");
    }
    for (let i = 0; i < divs.length; i++) {
        if (divs[i].className!="col-md-2"){
            divs[i].classList.toggle("contrast");
        }
    }
    for (let i = 0; i < links.length; i++) {
        links[i].classList.toggle("contrast3");
    }
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].classList.toggle("contrast");
    }
    for (let i = 0; i < span.length; i++) {
        span[i].classList.toggle("contrast3");
    }
    for (let i = 0; i < selects.length; i++) {
        selects[i].classList.toggle("contrast");
    }
    for (let i = 0; i < label.length; i++) {
        label[i].classList.toggle("contrast");
    }
    for (let i = 0; i < header.length; i++) {
        header[i].classList.toggle("contrast");
    }
    for (let i = 0; i < pre.length; i++) {
        pre[i].classList.toggle("contrast");
    }
    for (let i = 0; i < table.length; i++) {
        table[i].classList.toggle("contrast");
    }
    for (let i = 0; i < aside.length; i++) {
        aside[i].classList.toggle("contrast");
    }
    for (let i = 0; i < tagI.length; i++) {
        tagI[i].classList.toggle("contrast");
    }
    for (let i = 0; i < li.length; i++) {
        li[i].classList.toggle("contrast");
    }
    for (let i = 0; i < td.length; i++) {
        td[i].classList.toggle("contrast");
    }
    
    Array.from(document.querySelectorAll('img.imagemLogo')).forEach(value=>value.src=`/images/icon/logo_${
        typeof(Array.from(dropdown.classList).find(value=>value=='contrast2'))
        === 'undefined'?`branca`:`preta`
    }.png`);

    dropdown.classList.toggle("contrast2");
    $('#tr-lines td').toggleClass('contrast');
    $('#th-lines th').toggleClass('contrast');
    $( "#weka_content button" ).toggleClass( "contrast" );
    $( 'button:not(button[id="botao2"],button[id="botao3"],button[id="botao4"])' ).toggleClass( "contrast2" );
    $('#botao2,#botao3,#botao4').toggleClass("contrast");
    $('#modal div').toggleClass('contrast');
    $('#modal h3').toggleClass('contrast');
    $('h3').toggleClass('contrast');
    $('span').toggleClass('contrast');
    $('#modal h5').toggleClass('contrast');
    $('#pergunta').toggleClass('contrast3');
}
function tamanhoLetra(a,pg){
    if (a==1) {
        document.getElementById('pgInteira').style.fontSize = "medium";
        $('#th-lines th').css('font-size','medium');
        $('td').css('font-size','medium');
        $('h3').css('font-size','30px');
        $('button').addClass('btn-md');
        $('button').removeClass('btn-lg');
        $('button').removeClass('btn-xlg');
        $('#line-to-show table').removeClass('x-large');
        $('#line-to-show table').removeClass('large');
        $('#line-to-show table').addClass('medium');
        for (let i = 0; i < links.length; i++) {
            links[i].classList.remove("x-large");
            links[i].classList.remove("large");
            links[i].className+=" medium";
        }
        for (let i = 0; i < p.length; i++) {
            p[i].classList.remove("x-large");
            p[i].classList.remove("large");
            p[i].className+=" medium";
        }
        for (let i = 0; i < label.length; i++) {
            label[i].classList.remove("x-large");
            label[i].classList.remove("large");
            label[i].className+=" medium";
        }
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].classList.remove("x-large");
            inputs[i].classList.remove("large");
            inputs[i].className+=" medium";
        }
        for (let i = 0; i < selects.length; i++) {
            selects[i].classList.remove("x-large");
            selects[i].classList.remove("large");
            selects[i].className+=" medium";
        }
        for (let i = 0; i < td.length; i++) {
            td[i].classList.remove("x-large");
            td[i].classList.remove("large");
            td[i].className+=" medium";
        }
        for (let i = 0; i < h5.length; i++) {
            h5[i].classList.remove("x-large");
            h5[i].classList.remove("large");
            h5[i].className+=" medium";
        }
        for (let i = 0; i < span.length; i++) {
            span[i].classList.remove("x-large");
            span[i].classList.remove("large");
            span[i].className+=" medium";
        }
        for (let i = 0; i < small.length; i++) {
            small[i].classList.remove("x-large");
            small[i].classList.remove("large");
            small[i].className+=" medium";
        }
        $('#conta div').addClass('medium');
        // for (let i = 0; i < label.length; i++) {
        //     label[i].classList.remove("x-large");
        //     label[i].classList.remove("large");

        // }
    }else if(a==2){
        document.getElementById('pgInteira').style.fontSize = "large";
        $('#th-lines th').css('font-size','large');
        $('#tr-lines td').css('font-size','large');
        $('h3').css('font-size','40px');
        $('button').addClass('btn-lg');
        $('button').removeClass('btn-md');
        $('button').removeClass('btn-xlg');
        $('#line-to-show table').removeClass('x-large');
        $('#line-to-show table').addClass('large');
        $('#line-to-show table').removeClass('medium');
        for (let i = 0; i < links.length; i++) {
            links[i].classList.remove("x-large");
            links[i].classList.remove("medium");
            links[i].className+=" large";
        }
        for (let i = 0; i < p.length; i++) {
            p[i].classList.remove("x-large");
            p[i].classList.remove("medium");
            p[i].className+=" large";
        }
        for (let i = 0; i < label.length; i++) {
            label[i].classList.remove("x-large");
            label[i].classList.remove("medium");
            label[i].className+=" large";
        }
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].classList.remove("x-large");
            inputs[i].classList.remove("medium");
            inputs[i].className+=" large";
        }
        for (let i = 0; i < selects.length; i++) {
            selects[i].classList.remove("x-large");
            selects[i].classList.remove("medium");
            selects[i].className+=" large";
        }
        for (let i = 0; i < td.length; i++) {
            td[i].classList.remove("x-large");
            td[i].classList.remove("medium");
            td[i].className+=" large";
        }
        for (let i = 0; i < h5.length; i++) {
            h5[i].classList.remove("x-large");
            h5[i].classList.remove("medium");
            h5[i].className+=" large";
        }
        for (let i = 0; i < span.length; i++) {
            span[i].classList.remove("x-large");
            span[i].classList.remove("medium");
            span[i].className+=" large";
        }
        for (let i = 0; i < small.length; i++) {
            small[i].classList.remove("x-large");
            small[i].classList.remove("medium");
            small[i].className+=" large";
        }
    }else if(a==3){
        document.getElementById('pgInteira').style.fontSize = "x-large";
        $('#th-lines th').css('font-size','x-large');
        $('#tr-lines td').css('font-size','x-large');
        $('h3').css('font-size','45px');
        $('#th-lines th').css('font-size','x-large');
        $('button').addClass('btn-xlg');
        $('button').removeClass('btn-lg');
        $('button').removeClass('btn-md');
        $('#line-to-show table').addClass('x-large');
        $('#line-to-show table').removeClass('large');
        $('#line-to-show table').removeClass('medium');

        for (let i = 0; i < links.length; i++) {
            links[i].classList.remove("large");
            links[i].classList.remove("medium");
            links[i].className+=" x-large";
        }
        for (let i = 0; i < p.length; i++) {
            p[i].classList.remove("large");
            p[i].classList.remove("medium");
            p[i].className+=" x-large";
        }
        for (let i = 0; i < label.length; i++) {
            label[i].classList.remove("large");
            label[i].classList.remove("medium");
            label[i].className+=" x-large";
        }
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].classList.remove("large");
            inputs[i].classList.remove("medium");
            inputs[i].className+=" x-large";
        }
        for (let i = 0; i < selects.length; i++) {
            selects[i].classList.remove("large");
            selects[i].classList.remove("medium");
            selects[i].className+=" x-large";
        }
        for (let i = 0; i < td.length; i++) {
            td[i].classList.remove("large");
            td[i].classList.remove("medium");
            td[i].className+=" x-large";
        }
        for (let i = 0; i < h5.length; i++) {
            h5[i].classList.remove("large");
            h5[i].classList.remove("medium");
            h5[i].className+=" x-large";
        }
        for (let i = 0; i < span.length; i++) {
            span[i].classList.remove("large");
            span[i].classList.remove("medium");
            span[i].className+=" x-large";
        }
        for (let i = 0; i < small.length; i++) {
            small[i].classList.remove("large");
            small[i].classList.remove("medium");
            small[i].className+=" x-large";
        }
    }
}
$('.far.fa-question-circle.fa-3x.help').on("click",function(event) {
    $('#help-image').show();
    $('#help-image').toggleClass('hide');
    let classList = $('#help-image').attr('class').split(/\s+/);
    $.each(classList, function(index, item) {
        if (item !== 'hide'){
            $('#help-image-fade').show(0);
            $('#help-image-fade').fadeTo('fast',0.5);
        }else
            $('#help-image-fade').fadeTo('fast',0,()=>$('#help-image-fade').css('display','none'));
    });
});
