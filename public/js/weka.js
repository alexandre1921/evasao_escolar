"use strict";

var weka_result;
$(document).ready(function () {
    $('#metricType').on("change",function(event) {
        let $minMetric=$('#minMetric');
        if (this.value==='Lift')
            $minMetric.attr('max','');
        else{
            $minMetric.attr('max',1);
            $minMetric[0].value=0.9;
        }
    });    
    $("#weka-form").submit(function(event) {
        event.preventDefault();
        let formData=$('#weka-form')[0];
        let json=getJsonFromForm(formData);
        if (this.checkValidity()){
            $('#first-body').show();
            $.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: '/weka',
                type: 'POST',
                data: json,
                success: function(result) {
                    weka_result=result;
                    if (result.success==true){
                      for (let [key_question, value_question] of Object.entries(result.response.information.list)) {
                        for (let [key, weight] of Object.entries(value_question.alternative)) {
                            result.response.information.list[key_question].alternative[key]={weight:weight,quantity:0};
                        }
                      }
                      for (let [key_user, value_user] of Object.entries(result.response.information.user)) {
                        for (let [key, value] of Object.entries(value_user)) {
                            result.response.information.list[key].alternative[value.id_alternative].quantity++;
                        }
                      }
                      for (let [key_question, value_question] of Object.entries(result.response.information.list)) {
                        for (let [key, weight] of Object.entries(value_question.alternative)) {
                          if (typeof weight !== 'undefined'){
                            result.response.information.list[key_question].alternative[key].percentage=(100*weight.quantity/result.response.information.answered_All_question_users);
                          }
                        }
                      }
                      let string='';
                      for (let [key, value] of Object.entries(result.response.result['[conf|lift|lev|conv]'])) {
                        string+=
                            `
                            <div class="col-md-12">
                                <div class="au-card chart-percent-card">
                                    <div class="au-card-inner">
                                        <h3 class="title-2 tm-b-5">${parseInt(key)+1}º Regra</h3>
                                        <div class="col-md-12">
                                            <p>As pessoas que respondem 

                                            ${result.response.result.QuestionAndAnswerBeforeArrow[key].map((value)=>
                                            `<b>${value.alternative}</b> para questão <a href="#" data-toggle="modal" data-target="#more-modal">
                                            <b onclick="drawWeka(${value.id_question});" class="minion" 
                                            title='"${value.id_question}"'>${value.question}</b>
                                            </a>`).join(', ')}, 
                
                                            tem <b>${value.lift} vezes</b> mais chances de responder 
                
                                            ${result.response.result.QuestionAndAnswerAfterArrow[key].map((value)=>
                                            `<b>${value.alternative}</b> para questão <a href="#" data-toggle="modal" data-target="#more-modal">
                                            <b onclick="drawWeka(${value.id_question});" class="minion" 
                                            title='"${value.id_question}"'>${value.question}</b>
                                            </a>`).join(', ')}.
                
                                            Confiança de <b>${value.conf*100}%</b>.</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            `;
                      }
                        $('#weka_content').html(
                        `<center><button class="btn btn-warning btn-md click-me ${
                            typeof(Array.from(document.querySelector("#dropdown-menu-access").classList).find(value=>value=='contrast2'))
                            === 'undefined'?``:`contrast`}"
                        
                        " title="Mostrar o id">Mostrar o enunciado</button></center>${string}`
                      );
                      $('.click-me').on('click',function(event){
                        changeTitle(this);
                        $('.minion').each(function(event){changeTitle(this)});
                      });
                    }else{
                        showMessage("#main-content #message",result,'danger',5000);
                    }
                }
            }).done(function(){
                $('#first-body').hide();
            });
        }
    });
});
function changeTitle(element){
    let html=document.createElement('p').textContent=element.textContent;// parse html especial chars to humans
    element.innerHTML=element.title;
    element.title=html;
}
function drawWeka(key){
    $('#more-modal .modal-body').html(
    `<div class="col-md-12">
        <div class="au-card chart-percent-card">
            <div class="au-card-inner">
                <h3 class="title-2 tm-b-5"></h3>
                <div class="row no-gutters">
                    <div class="col-xl-2">
                        <div class="chart-note-wrap" id="chart-note-wrap-question_">
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="percent-chart"><div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                </div>
                            </div>
                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;left:0; top:0">
                                    </div>
                                </div>
                            </div>
                            <canvas id="percent-chart" height="280" style="display: block; width: 253px; height: 280px;" width="253" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                    <div class="col-xl-8">
                    </div>
                </div>
            </div>
        </div>
    </div>`);
    $("#more-modal").on('shown.bs.modal', function() {
        let backgroundArray=[
            '#00e448',
            '#00c5e4',
            '#e40000',
            '#e1e400',
            '#7900e4',
        ];
        // Percent Chart
        $('#more-modal .modal-body').html(`
        <div class="col-md-12">
            <div class="au-card chart-percent-card ${
                        typeof(Array.from(document.querySelector(" #dropdown-menu-access ").classList).find(value=>value=='contrast2'))
                        === 'undefined'?``:`contrast`}">
                <div class="au-card-inner">
                    <h3 class="title-2 tm-b-5" title="${weka_result.response.information.list[key].question}" 
                        ${typeof(Array.from(document.querySelector( "#dropdown-menu-access").classList).find(value=>value=='contrast2'))
                        === 'undefined'?``:`contrast`}">${weka_result.response.information.list[key].question}</h3>
                    <div class="row no-gutters">
                        <div class="col-xl-2">
                            <div class="chart-note-wrap" id="chart-note-wrap-question_${key}">
                                ${(()=>{
                                    let string_dot='';
                                    let i=0;
                                    for (let [key_1, value_1] of Object.entries(weka_result.response.information.list[key].alternative)){
                                        string_dot+=`<div class="chart-note mr-0 d-block"><span class="dot dot--blue" style="background: ${backgroundArray[i]};"></span><span>Peso <b>${value_1.weight}</b></span></div>`
                                        i++;
                                    }
                                    return string_dot;
                                })()}
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="percent-chart">
                                <div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;" class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                        </div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                        <div style="position:absolute;left:0; top:0">
                                        </div>
                                    </div>
                                </div>
                                <canvas id="percent-chart" height="280" style="display: block; width: 253px; height: 280px;" width="253" class="chartjs-render-monitor"></canvas>
                            </div>
                            <div class="col-xl-8">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `);
        let data=[];
        let labels=[];
        for (let value of Object.values(weka_result.response.information.list[key].alternative)) {
            data.push(value.percentage);
            labels.push(value);
        }
        let ctx = $(`#more-modal .modal-body #percent-chart`)[0];
        if (ctx) {
            ctx.height = 280;
            let myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [
                    {
                        label: labels,
                        data: data,
                        backgroundColor: backgroundArray,
                        hoverBackgroundColor: [
                        '#00ff51',
                        '#00f7ff',
                        '#ff0000',
                        '#ffff00',
                        '#7900e4',
                        ],
                        borderWidth: [
                        0, 0, 0, 0, 0
                        ],
                        hoverBorderColor: [
                        'transparent',
                        'transparent',
                        'transparent',
                        'transparent',
                        'transparent',
                        ]
                    }
                    ],
                    labels: labels,
                },
                options: {
                        maintainAspectRatio: false,
                        responsive: true,
                        cutoutPercentage: 55,
                        animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                            let dataset = data.datasets[tooltipItem.datasetIndex];
                            let percentage = ' '+dataset.label[tooltipItem.index].quantity+' de '+weka_result.response.information.answered_All_question_users+' = '+dataset.data[tooltipItem.index].toFixed(2)+'%';         
                            return percentage;
                            }
                        },
                        titleFontFamily: "Poppins",
                        xPadding: 15,
                        yPadding: 10,
                        caretPadding: 0,
                        bodyFontSize: 16
                    }
                }
            });
        }
    });
}