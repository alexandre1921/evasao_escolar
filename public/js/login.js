"use strict";

$(document).ready(function () {
    $("#login-form").submit(function(event) {
        event.preventDefault();
        let formData=$('#login-form')[0];
        let json=getJsonFromForm(formData);
        if (this.checkValidity()){
            $.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: '/auth',
                type: 'POST',
                data: json,
                success: function(result) {
                    if (result.success==true){
                        window.location.href = "/formulario";
                    }else{
                        showMessage("#login-form #message",result);
                    }
                }
            });
        }
    });
    
    $("#recover-modal .send").on("click",function(event) {
        $("#recover-modal form:not(:hidden)").submit();
    });

    $("#recover-modal form").on("submit",function(event) {
        event.preventDefault();
        this.classList.add('was-validated');
        if (this.checkValidity()){
            if ($('#recover-modal #recover-form')[0].style.display==='' && $('#recover-modal #verification-code-form')[0].style.display==='none'){
                let formData=$('#recover-modal #recover-form')[0];
                let json=getJsonFromForm(formData);
                $('#recover-modal .loading-anime').show();
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: '/recover',
                    type: 'POST',
                    data: json,
                    success: function(result) {
                        if (result.success==true){
                            $("#recover-modal #message").hide();
                            $('#recover-form').hide();
                            let tag_code=$('#verification-code-form');
                            tag_code.addClass('needs-validation');
                            tag_code.show();
                        }else{
                            showMessage("#recover-modal #message",result);
                        }
                    }
                }).done(function(){
                    $('#recover-modal .loading-anime').hide();
                });
            }else{
                if ($('#recover-modal #verification-code-form')[0].style.display==='' && $('#recover-modal #change-password-form')[0].style.display==='none'){
                    let formData=$('#recover-modal #recover-form')[0];
                    let formData1=$("#recover-modal #verification-code-form")[0];
                    let json=getJsonFromForm(formData,[formData1]);
                    $('#recover-modal .loading-anime').show();
                    $.ajax({
                        contentType: 'application/x-www-form-urlencoded',
                        url: '/recover',
                        type: 'PUT',
                        data: json,
                        success: function(result) {
                            if (result.success==true){
                                $("#recover-modal #message").hide();
                                $('#verification-code-form').hide();
                                let tag_code=$('#change-password-form');
                                tag_code.addClass('needs-validation');
                                tag_code.show();
                            }else{
                                showMessage("#recover-modal #message",result);
                            }
                        }
                    }).done(function(){
                        $('#recover-modal .loading-anime').hide();
                    });
                }else{
                    let formData=$('#recover-modal #recover-form')[0];
                    let formData1=$("#recover-modal #verification-code-form")[0];
                    let formData2=$("#recover-modal #change-password-form")[0];
                    let json=getJsonFromForm(formData,[formData1,formData2]);
                    $('#recover-modal .loading-anime').show();
                    $.ajax({
                        contentType: 'application/x-www-form-urlencoded',
                        url: '/user',
                        type: 'PUT',
                        data: json,
                        success: function(result) {
                            if (result.success==true){
                                $('#change-password-form').hide();
                                $('#recover-modal .btn.btn-primary.send').hide();
                                $('#recover-modal form').toggleClass("was-validated");
                                $('#recover-modal form').trigger("reset");
                                $('#recover-modal #recover_again').show();
                                $("#recover-modal #message").show();
                                $('#recover-modal #message').html(
                                    `<div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading">Tudo certo!</h4>
                                        <p>Sua senha foi alterada com sucesso.</p>
                                    </div>`
                                );
                            }else{
                                showMessage("#recover-modal #message",result);
                            }
                        }
                    }).done(function(){
                        $('#recover-modal .loading-anime').hide();
                    });
                }
            }
        }
            
    });

    $("#recover-modal #recover_again").on("click",function(event) {
        $('#recover-modal #message').html('');
        $('#recover-modal #recover-form').show();
        $('#recover-modal #recover_again').hide();
        $('#recover-modal .send').show();
    });
});