"use strict";

var formState = {};

const ajaxIni = () => window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

const RequestXML = (method,url,values,callback) => {
	let xml = ajaxIni();
	xml.open(method, url, true);
	xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xml.send(JSON.stringify(values));
	xml.onreadystatechange = () => {
		if (xml.readyState==4 && xml.status==200)
			callback(JSON.parse(xml.responseText));
	}
}

const setFormState = element => {
	if (element.checkValidity()){
		element.className='';
		if (element.type=='file'){
			let reader = new FileReader();
			reader.onloadend = () => {
				formState[element.name]=reader.result;
			}
			reader.readAsDataURL(element.files[0]);
		}else
			formState[element.name]=element.value;
	}else{
		element.className='form-error';
	}
}

const formValidity = form => typeof Array.from(form.elements).find(input => !input.checkValidity()) === 'undefined';