"use strict";

var objectState={};
$(document).ready(function () {

    $("#register").on("click",function(event) {
        event.preventDefault();
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/ajax/course',
            type: 'POST',
            data: JSON.stringify({request:{print:true,criteria:'activated'}}),
            success: function(result) {
                if (result.success==true){
                    createOption(result,'#id_course_object-modal_user',value=>`<option value="${value.id_course}">${value.name_course}</option>`);
                }
            }
        });
    });

    $("#object-modal .custom-file-input").on("change",function(event) {
        getCompressedImage(this,value=>{
            if (value.data.length<16753000){
                $("#object-modal .custom-file-input")[0].setCustomValidity("");
                $('#object-modal #preview').attr('src',`${value.prefix}${value.data}`);
                objectState['profile_image_type']=value.prefix;
                objectState['profile_image']=value.data;
            }else{
                $("#object-modal .custom-file-input")[0].setCustomValidity("Inválido");
            }
        });
    });
    
    $("#object-modal .send").on("click",function(event) {
        $("#object-modal form:not(:hidden)").submit();
    });

    $("#object-modal form").on("submit",function(event) {
        event.preventDefault();
        let form=$("#object-modal form")[0];
        if (form.checkValidity()){
            $('#object-modal .loading-anime').show();
            let json=getJsonFromForm(form,[],!jQuery.isEmptyObject(svg_image)?objectState:{profile_image:svg_image,profile_image_type:svg_image_type});
            $.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: '/user',
                type: 'POST',
                data: json,
                success: function(result) {
                    if (result.success==true){
                        $(".dismiss").click();
                        form.classList.toggle("was-validated");
                        form.reset();
                        $('#object-modal #preview').attr('src',`/images/icon/user.svg`);
                        showMessage("#login-form #message",result,'success');
                    }else{
                        showMessage("#object-modal #message",result);
                    }
                }
            }).done(function() {
                $('#object-modal .loading-anime').hide();
            });
        }
    });
});