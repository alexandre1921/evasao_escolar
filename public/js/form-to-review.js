"use strict";

const RequestXML = (method,url,values,callback) => {
	let xml = (() => window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP"))();
	xml.open(method, url, true);
	xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xml.send(JSON.stringify(values));
	xml.onreadystatechange = () => {
		if (xml.readyState==4 && xml.status==200)
			callback(JSON.parse(xml.responseText));
	}
}

var qtdAlt=[];
var qtdQue=[];

function actuateQuestion(id,status){
	if (status){
		document.getElementById(`input_id_question_${id}`).type='hidden';
		changeDisplay(`label_id_question_${id}`,'');
	}else{
		document.getElementById(`input_id_question_${id}`).type='text';
		document.getElementById(`input_id_question_${id}`).focus();
		changeDisplay(`label_id_question_${id}`,'none');
	}
}

function actuateAlternative(id,status){
	if (status){
		document.getElementById(`input_id_alternative_${id}`).type='radio';
		changeDisplay(`bold_id_alternative_${id}`,'');
	}else{
		document.getElementById(`input_id_alternative_${id}`).type='number';
		document.getElementById(`input_id_alternative_${id}`).focus();
		changeDisplay(`bold_id_alternative_${id}`,'none');
	}
}

function changeDisplay(id,value){
	document.getElementById(id).style.display=value;
}

const QuestionString = value => {
	qtdQue.push(value.id_question);
	qtdAlt[value.id_question]=0;//col-xs-6
	return `
<div id="div_id_question_${value.id_question}">
	<span id="card_header_${qtdQue.length}" style="padding-top:80px"></span>
	<div class="row justify-content-center">
		<div class="col-lg-8 col-md-9 col-sm-8">
			<div id="pergunta" class="card" style="border-radius:10px;margin-bottom:10px;"
				onmouseover="changeDisplay('crud_id_question_${value.id_question}','');"
				onmouseout="changeDisplay('crud_id_question_${value.id_question}','none');">
				<div id="perguntaHeader" class="card-header">
					Questão <strong id="strong_${qtdQue.length}">${qtdQue.length}</strong>
				</div>
				<div class="card-body card-block">
					<form onsubmit="return false" id="id_question_${value.id_question}">
						<div class="form-row">
							<div class="col-md-9 mb-2">
								<label>
									<input 
										id="input_id_question_${value.id_question}" 
										type="hidden" value="${value.statement}"
										onchange="updateQuestion(this)"
										onblur="actuateQuestion(${value.id_question},true)"
									>
								</label>
								<label id="label_id_question_${value.id_question}"
									onclick="actuateQuestion(${value.id_question},false)"><strong>${value.statement}</strong>
								</label>
							</div>
							<label id="label_answered_id_question_${value.id_question}"></label>
							<div class="col-md-3">
								<label style="display:none" id="crud_id_question_${value.id_question}">
									<button
										type="button"
										class="btn btn-danger btn-sm"
										onclick="deleteQuestion(${value.id_question})"
									>
										<i class="fa fa-times"></i> Rm. qut.
									</button>
									<button 
										type="button" 
										class="btn btn-primary btn-sm"
										onclick="createAlternative(${value.id_question},qtdAlt[${value.id_question}]+1)"
									>
										<i class="fa fa-plus"></i> Add. alt.
									</button>
								</label>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
`;
}

const AlternativeString = value => {
	qtdAlt[value.id_question]++;
	return `<span id='erro'></span>
	<div 
		class="form-row"				
		onmouseover="changeDisplay('delete_id_alternative_${value.id_alternative}','');"
		onmouseout="changeDisplay('delete_id_alternative_${value.id_alternative}','none');"
		id="id_alternative_${value.id_alternative}">
		<div class="col-md-2 mb-2">
			<input 
				id="input_id_alternative_${value.id_alternative}" 
				type="radio" value="${value.weight}"
				onchange="updateAlternative(${value.id_alternative},${value.id_question},this.value)"
				onblur="actuateAlternative(${value.id_alternative},true)"
				name='alternative'
			>
			<b
				id="bold_id_alternative_${value.id_alternative}" 
				onclick="actuateAlternative(${value.id_alternative},false)">
				${value.weight}
			</b>
		</div>
		<div class="col-md-2 mb-0">
			<div id="delete_id_alternative_${value.id_alternative}" style="display:none">
				<button
					type="button" 
					class="btn btn-danger btn-sm"
					onclick="deleteAlternative(${value.id_alternative},${value.id_question})"
				>
					<i class="fa fa-times"></i> Remover alternativa
				</button>
			</div>
		</div>
	</div>`;
}

const GetListQuestion = () =>
	RequestXML('POST', '/ajax/question',{request:{criteria:'answered',print:true}}, json => {
		ListQuestion(json);
	})

const GetListAlternative = () =>
	RequestXML('POST', '/ajax/alternative',{request:{criteria:'form',print:true}}, json => {
		ListAlternative(json);
	})


function ListQuestion(Question_json,Alternative_json){
	if (Question_json.success==true){
		for (var value of Object.values(Question_json.response)) {
			document.getElementById("content").innerHTML+=QuestionString(value);
		}
		ListAlternative(Alternative_json);
	}else{
		showMessage("#main-content #message",Question_json,'danger',5000);
	}
}

function ListAlternative(Alternative_json){
	if (Alternative_json.success==true){
		for (var value of Object.values(Alternative_json.response)){
			document.getElementById("id_question_"+value.id_question).innerHTML+=AlternativeString(value);
		}
	}else{
		showMessage("#main-content #message",Alternative_json,'danger',5000);
	}
}

const createQuestion = () =>
	RequestXML('POST', '/question',{statement:'Insira o enunciado'}, json => {
		if (json.success==true){
			let value=json.response[0];
			document.getElementById("content").innerHTML+=QuestionString(value);
			window.location.href = `#div_id_question_${value.id_question}`;
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	})

const updateQuestion = (element) =>{
	let id_question=element.id.replace("input_id_question_","");
	RequestXML('PUT', '/question',{id_question:id_question, statement:element.value}, json => {
		if (json.success==true){
			document.getElementById(`input_id_question_${id_question}`).value=element.value;
			document.getElementById(`label_id_question_${id_question}`).innerHTML=`<strong>${element.value}</strong>`;
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	});
}
const deleteQuestion = (id_question) =>
	RequestXML('DELETE', '/question',{id_question:id_question}, json => {
		if (json.success==true){
			qtdQue.forEach((value,key)=>{ 
				if(value>=id_question){
					document.getElementById(`card_header_${key+1}`).id=`card_header_${key}`;
					if (document.getElementById(`strong_${key+1}`)!=null)
						document.getElementById(`strong_${key+1}`).id=`strong_${key}`;
					if (document.getElementById(`strong_${key}`)!=null)
						document.getElementById(`strong_${key}`).innerHTML=key;
					
				}
			});
			document.getElementById(`div_id_question_${id_question}`).remove();
			qtdQue = qtdQue.filter((value)=>value!=id_question);
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	})

const createAlternative = (id_question,weight) => {
	let formState={weight:weight, id_question:id_question};
	RequestXML('POST', '/alternative',formState, json => {
		if (json.success==true){
			let value=json.response[0];
			formState['id_alternative']=value.id_alternative;
			document.getElementById("id_question_"+id_question).innerHTML+=AlternativeString(formState);
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	});
}

const updateAlternative = (id_alternative,id_question,weight) =>
	RequestXML('PUT', '/alternative',{id_alternative:id_alternative, id_question:id_question, weight:weight}, json => {
		if (json.success==true){
			document.getElementById(`input_id_alternative_${id_alternative}`).value=weight;
			document.getElementById(`bold_id_alternative_${id_alternative}`).innerHTML=weight;
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	})

const deleteAlternative = (id_alternative,id_question) =>
	RequestXML('DELETE', '/alternative',{id_alternative:id_alternative}, json => {
		if (json.success==true){
			qtdAlt[id_question]--;
			document.getElementById(`id_alternative_${id_alternative}`).remove();
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	})