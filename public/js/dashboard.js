"use strict";

var userState={};

$(document).ready(function () {
    $('#telephone_user-modal_contact').mask('(00) 00000-0000', {reverse: false});
    $('#cellphone_user-modal_contact').mask('(00) 00000-0000', {reverse: false});
    if (typeof modal_user!=='undefined')
        userState=modal_user;// caso o usuario esteja logado
    $("#user-modal-a").on("click",function(event) {
        $("#user-modal input, #user-modal select").map((key,value)=>{
            let id=value.id.replace(/_user-modal_.*/g,'');
            if (value.type!='file' && id!='password' && id!='confirm_password' && userState[id]!=undefined)
                value.value=userState[id];
        });
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/ajax/course',
            type: 'POST',
            data: JSON.stringify({request:{print:true,criteria:'activated'}}),
            success: function(result) {
                if (result.success==true){
                    createOption(result,'#id_course_user-modal_user',value=>`<option value="${value.id_course}">${value.name_course} | ${value.duration}</option>`);
                }
            }
        });
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/ajax/adress',
            type: 'POST',
            data: JSON.stringify({id_adress:userState['id_adress'],request:{criteria:'search',print:true}}),
            success: function(result) {
                if (result.success==true){
                    // code...
                }else{
                    showMessage("#user-modal #message",result,'danger',5000);
                }
            }
        });
    });

    $("#user-modal .send").on("click",function(event) {
        $("#user-modal form:not(:hidden)").submit();
    });

    $("#user-modal .custom-file-input").on("change",function(event) {
        getCompressedImage($("#user-modal .custom-file-input")[0],value=>{
            if (value.data.length<16753000){
                $("#user-modal .custom-file-input")[0].setCustomValidity("");
                $('#user-modal #preview').attr('src',`${value.prefix}${value.data}`);
                userState['profile_image_type']=value.prefix;
                userState['profile_image']=value.data;
            }else{
                $("#user-modal .custom-file-input")[0].setCustomValidity("Inválido");
            }
        });
    });

    $("#user-modal #nav-profile form").on("submit",function(event) {
        event.preventDefault();
        let formData=$('#user-modal form')[0];
        let json=getJsonFromForm(formData,[],userState);
        json=cleanJson(json);
        if ($('#user-modal [name="password"]')[0].value!='')
            $('#user-modal [name="confirm_password"]').attr("required","true");
        else
            $('#user-modal [name="confirm_password"]').attr("required","false");
        $('#user-modal .loading-anime').show();
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/user',
            type: 'PUT',
            data: json,
            success: function(result) {
                if (result.success==true){
                    Object.assign(userState, JSON.parse(json));
                    $(".dismiss").click();
                    $("#user-modal #message").html("");
                    if (userState["profile_image_type"]!=undefined && userState["profile_image"]!=undefined)
                        $(".img-put").attr("src",`${userState["profile_image_type"]}${userState["profile_image"]}`);
                    $(".name-session").html(userState["name_user"]);
                    $(".email-session").html(userState["login"]);
                    $("#user-modal form").removeClass('was-validated');
                }else{
                    showMessage("#user-modal #message",result);
                }
            }
        }).done(function() {
            $('#user-modal .loading-anime').hide();
        });
    });
    $("#user-modal #nav-profile form").on("submit",function(event) {
        event.preventDefault();
        let formData=$('#user-modal #nav-profile form')[0];
        let json=getJsonFromForm(formData,[],userState);
        json=cleanJson(json);
        if ($('#user-modal [name="password"]')[0].value!='')
            $('#user-modal [name="confirm_password"]').attr("required","true");
        else
            $('#user-modal [name="confirm_password"]').attr("required","false");
        $('#user-modal .loading-anime').show();
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/user',
            type: 'PUT',
            data: json,
            success: function(result) {
                if (result.success==true){
                    Object.assign(userState, JSON.parse(json));
                    $(".dismiss").click();
                    $("#user-modal #message").html("");
                    if (userState["profile_image_type"]!=undefined && userState["profile_image"]!=undefined)
                        $(".img-put").attr("src",`${userState["profile_image_type"]}${userState["profile_image"]}`);
                    $(".name-session").html(userState["name_user"]);
                    $(".email-session").html(userState["login"]);
                    $("#user-modal form").removeClass('was-validated');
                }else{
                    showMessage("#user-modal #message",result);
                }
            }
        }).done(function() {
            $('#user-modal .loading-anime').hide();
        });
    });
    $("#user-modal #nav-adress form").on("submit",function(event) {
        event.preventDefault();
        let formData=$('#user-modal #nav-adress form')[0];
        let json=getJsonFromForm(formData,[],userState);
        json=cleanJson(json);
        if ($('#user-modal [name="password"]')[0].value!='')
            $('#user-modal [name="confirm_password"]').attr("required","true");
        else
            $('#user-modal [name="confirm_password"]').attr("required","false");
        $('#user-modal .loading-anime').show();
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/adress',
            type: 'PUT',
            data: json,
            success: function(result) {
                if (result.success==true){
                    Object.assign(userState, JSON.parse(json));
                    $(".dismiss").click();
                    $("#user-modal #message").html("");
                    if (userState["profile_image_type"]!=undefined && userState["profile_image"]!=undefined)
                        $(".img-put").attr("src",`${userState["profile_image_type"]}${userState["profile_image"]}`);
                    $("#user-modal form").removeClass('was-validated');
                }else{
                    showMessage("#user-modal #message",result);
                }
            }
        }).done(function() {
            $('#user-modal .loading-anime').hide();
        });
    });
    $("#user-modal #nav-contact form").on("submit",function(event) {
        event.preventDefault();
        let formData=$('#user-modal #nav-contact form')[0];
        let json=getJsonFromForm(formData,[]);
        json=cleanJson(json);
        if ($('#user-modal [name="password"]')[0].value!='')
            $('#user-modal [name="confirm_password"]').attr("required","true");
        else
            $('#user-modal [name="confirm_password"]').attr("required","false");
        $('#user-modal .loading-anime').show();
        $.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: '/contact',
            type: 'PUT',
            data: json,
            success: function(result) {
                if (result.success==true){
                    Object.assign(userState, JSON.parse(json));
                    $(".dismiss").click();
                    $("#user-modal #message").html("");
                    if (userState["profile_image_type"]!=undefined && userState["profile_image"]!=undefined)
                        $(".img-put").attr("src",`${userState["profile_image_type"]}${userState["profile_image"]}`);
                    $("#user-modal form").removeClass('was-validated');
                }else{
                    showMessage("#user-modal #message",result);
                }
            }
        }).done(function() {
            $('#user-modal .loading-anime').hide();
        });
    });
});