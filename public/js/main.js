var svg_image=`data:image/svg+xml;base64`;
var svg_image_type=`PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgo8c3ZnCiAgICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIgogICAgeG1sbnM6bnMxPSJodHRwOi8vc296aS5iYWllcm91Z2UuZnIiCiAgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICAgaWQ9InN2ZzM0MzciCiAgICBzb2RpcG9kaTpkb2NuYW1lPSJhdmF0YXIuc3ZnIgogICAgdmlld0JveD0iMCAwIDU0OS42MiA2MDUuMDUiCiAgICB2ZXJzaW9uPSIxLjEiCiAgICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjQ4LjQgcjk5MzkiCiAgPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICAgaWQ9ImJhc2UiCiAgICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMjUiCiAgICAgIGZpdC1tYXJnaW4tbGVmdD0iMCIKICAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgICBmaXQtbWFyZ2luLXRvcD0iMCIKICAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMCIKICAgICAgaW5rc2NhcGU6em9vbT0iMC43NDIyODc5MiIKICAgICAgaW5rc2NhcGU6d2luZG93LXg9IjAiCiAgICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9Ijk5MSIKICAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgICBib3JkZXJvcGFjaXR5PSIxLjAiCiAgICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImxheWVyMSIKICAgICAgaW5rc2NhcGU6Y3g9IjI5Ny4xNDI4NiIKICAgICAgaW5rc2NhcGU6Y3k9IjIyMC4yMzM3NyIKICAgICAgZml0LW1hcmdpbi1yaWdodD0iMCIKICAgICAgZml0LW1hcmdpbi1ib3R0b209IjAiCiAgICAgIGlua3NjYXBlOndpbmRvdy13aWR0aD0iMTg5OCIKICAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAuMCIKICAgICAgaW5rc2NhcGU6ZG9jdW1lbnQtdW5pdHM9InB4IgogIC8+CiAgPGcKICAgICAgaWQ9ImxheWVyMSIKICAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgIGlua3NjYXBlOmdyb3VwbW9kZT0ibGF5ZXIiCiAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC05MS40MTQgLTE0OS45MykiCiAgICA+CiAgICA8ZwogICAgICAgIGlkPSJnMzE5OSIKICAgICAgICBzdHlsZT0ic3Ryb2tlOiNmMmYyZjIiCiAgICAgICAgdHJhbnNmb3JtPSJtYXRyaXgoMTEuNzA1IDAgMCAxMS43MDUgLTE5NDQuNCAxNTY5LjkpIgogICAgICA+CiAgICAgIDxwYXRoCiAgICAgICAgICBpZD0icGF0aDQ2OTMiCiAgICAgICAgICBzdHlsZT0ic3Ryb2tlOiNmMmYyZjI7c3Ryb2tlLXdpZHRoOjEuMzcwNztmaWxsOiMwMDgwZmYiCiAgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgzLjg1MjggMCAwIC0zLjg1MjggLTM1NTEuNCA0OC40ODkpIgogICAgICAgICAgZD0ibTk3OC40IDMxLjM1MmMwIDIuOTg4Ny0yLjQyMjggNS40MTE1LTUuNDExNSA1LjQxMTVzLTUuNDExNS0yLjQyMjgtNS40MTE1LTUuNDExNWg1LjQxMTV6IgogICAgICAvPgogICAgICA8cGF0aAogICAgICAgICAgaWQ9InBhdGg0Njk1IgogICAgICAgICAgc3R5bGU9InN0cm9rZTojZjJmMmYyO3N0cm9rZS13aWR0aDoyLjA1O2ZpbGw6IzAwODBmZiIKICAgICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDIuNTc2MiAwIDAgMi41NzYyIC0yMzA5LjIgLTE4NS40OCkiCiAgICAgICAgICBkPSJtOTc4LjQgMzEuMzUyYzAgMi45ODg3LTIuNDIyOCA1LjQxMTUtNS40MTE1IDUuNDExNXMtNS40MTE1LTIuNDIyOC01LjQxMTUtNS40MTE1IDIuNDIyOC01LjQxMTUgNS40MTE1LTUuNDExNSA1LjQxMTUgMi40MjI4IDUuNDExNSA1LjQxMTV6IgogICAgICAvPgogICAgPC9nCiAgICA+CiAgPC9nCiAgPgogIDxtZXRhZGF0YQogICAgPgogICAgPHJkZjpSREYKICAgICAgPgogICAgICA8Y2M6V29yawogICAgICAgID4KICAgICAgICA8ZGM6Zm9ybWF0CiAgICAgICAgICA+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0CiAgICAgICAgPgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiCiAgICAgICAgLz4KICAgICAgICA8Y2M6bGljZW5zZQogICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL3B1YmxpY2RvbWFpbi8iCiAgICAgICAgLz4KICAgICAgICA8ZGM6cHVibGlzaGVyCiAgICAgICAgICA+CiAgICAgICAgICA8Y2M6QWdlbnQKICAgICAgICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9vcGVuY2xpcGFydC5vcmcvIgogICAgICAgICAgICA+CiAgICAgICAgICAgIDxkYzp0aXRsZQogICAgICAgICAgICAgID5PcGVuY2xpcGFydDwvZGM6dGl0bGUKICAgICAgICAgICAgPgogICAgICAgICAgPC9jYzpBZ2VudAogICAgICAgICAgPgogICAgICAgIDwvZGM6cHVibGlzaGVyCiAgICAgICAgPgogICAgICA8L2NjOldvcmsKICAgICAgPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICAgcmRmOmFib3V0PSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9saWNlbnNlcy9wdWJsaWNkb21haW4vIgogICAgICAgID4KICAgICAgICA8Y2M6cGVybWl0cwogICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI1JlcHJvZHVjdGlvbiIKICAgICAgICAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjRGlzdHJpYnV0aW9uIgogICAgICAgIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiCiAgICAgICAgLz4KICAgICAgPC9jYzpMaWNlbnNlCiAgICAgID4KICAgIDwvcmRmOlJERgogICAgPgogIDwvbWV0YWRhdGEKICA+Cjwvc3ZnCj4K`;

(function ($) {
    // USE STRICT
    "use strict";
    $(".animsition").animsition({
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 300,
      outDuration: 300,
      linkElement: 'a:not([target="_blank"]):not([href^="#"]):not([class^="chosen-single"])',
      loading: true,
      loadingParentElement: 'html',
      loadingClass: 'page-loader',
      loadingInner: '<div class="page-loader__spin"></div>',
      timeout: false,
      timeoutCountdown: 1000,
      onLoadEvent: true,
      browser: ['animation-duration', '-webkit-animation-duration'],
      overlay: false,
      overlayClass: 'animsition-overlay-slide',
      overlayParentElement: 'html',
      transition: function (url) {
        window.location.href = url;
      }
    });
  
  
  })(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  // Map
  try {

    var vmap = $('#vmap');
    if(vmap[0]) {
      vmap.vectorMap( {
        map: 'world_en',
        backgroundColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#1de9b6',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: [ '#1de9b6', '#03a9f5'],
        normalizeFunction: 'polynomial'
      });
    }

  } catch (error) {
    console.log(error);
  }

  // Europe Map
  try {
    
    var vmap1 = $('#vmap1');
    if(vmap1[0]) {
      vmap1.vectorMap( {
        map: 'europe_en',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        enableZoom: true,
        showTooltip: true
      });
    }

  } catch (error) {
    console.log(error);
  }

  // USA Map
  try {
    
    var vmap2 = $('#vmap2');

    if(vmap2[0]) {
      vmap2.vectorMap( {
        map: 'usa_en',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        enableZoom: true,
        showTooltip: true,
        selectedColor: null,
        hoverColor: null,
        colors: {
            mo: '#001BFF',
            fl: '#001BFF',
            or: '#001BFF'
        },
        onRegionClick: function ( event, code, region ) {
            event.preventDefault();
        }
      });
    }

  } catch (error) {
    console.log(error);
  }

  // Germany Map
  try {
    
    var vmap3 = $('#vmap3');
    if(vmap3[0]) {
      vmap3.vectorMap( {
        map: 'germany_en',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        onRegionClick: function ( element, code, region ) {
            var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();

            alert( message );
        }
      });
    }
    
  } catch (error) {
    console.log(error);
  }
  
  // France Map
  try {
    
    var vmap4 = $('#vmap4');
    if(vmap4[0]) {
      vmap4.vectorMap( {
        map: 'france_fr',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        enableZoom: true,
        showTooltip: true
      });
    }

  } catch (error) {
    console.log(error);
  }

  // Russia Map
  try {
    var vmap5 = $('#vmap5');
    if(vmap5[0]) {
      vmap5.vectorMap( {
        map: 'russia_en',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        hoverOpacity: 0.7,
        selectedColor: '#999999',
        enableZoom: true,
        showTooltip: true,
        scaleColors: [ '#C8EEFF', '#006491' ],
        normalizeFunction: 'polynomial'
      });
    }


  } catch (error) {
    console.log(error);
  }
  
  // Brazil Map
  try {
    
    var vmap6 = $('#vmap6');
    if(vmap6[0]) {
      vmap6.vectorMap( {
        map: 'brazil_br',
        color: '#007BFF',
        borderColor: '#fff',
        backgroundColor: '#fff',
        onRegionClick: function ( element, code, region ) {
            var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
            alert( message );
        }
      });
    }

  } catch (error) {
    console.log(error);
  }
})(jQuery);
(function ($) {
  // Use Strict
  "use strict";
  try {
    var progressbarSimple = $('.js-progressbar-simple');
    progressbarSimple.each(function () {
      var that = $(this);
      var executed = false;
      $(window).on('load', function () {

        that.waypoint(function () {
          if (!executed) {
            executed = true;
            /*progress bar*/
            that.progressbar({
              update: function (current_percentage, $this) {
                $this.find('.js-value').html(current_percentage + '%');
              }
            });
          }
        }, {
            offset: 'bottom-in-view'
          });

      });
    });
  } catch (err) {
    console.log(err);
  }
})(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  // Scroll Bar
  try {
    var jscr1 = $('.js-scrollbar1');
    if(jscr1[0]) {
      const ps1 = new PerfectScrollbar('.js-scrollbar1');      
    }

    var jscr2 = $('.js-scrollbar2');
    if (jscr2[0]) {
      const ps2 = new PerfectScrollbar('.js-scrollbar2');

    }

  } catch (error) {
    console.log(error);
  }

})(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  // Select 2
  try {

    $(".js-select2").each(function () {
      $(this).select2({
        minimumResultsForSearch: 20,
        dropdownParent: $(this).next('.dropDownSelect2')
      });
    });

  } catch (error) {
    console.log(error);
  }


})(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  // Dropdown 
  try {
    var menu = $('.js-item-menu');
    var sub_menu_is_showed = -1;

    for (var i = 0; i < menu.length; i++) {
      $(menu[i]).on('click', function (e) {
        e.preventDefault();
        $('.js-right-sidebar').removeClass("show-sidebar");        
        if (jQuery.inArray(this, menu) == sub_menu_is_showed) {
          $(this).toggleClass('show-dropdown');
          sub_menu_is_showed = -1;
        }
        else {
          for (var i = 0; i < menu.length; i++) {
            $(menu[i]).removeClass("show-dropdown");
          }
          $(this).toggleClass('show-dropdown');
          sub_menu_is_showed = jQuery.inArray(this, menu);
        }
      });
    }
    $(".js-item-menu, .js-dropdown").click(function (event) {
      if (typeof event.target.attributes['data-toggle'] === 'undefined')// conflito do menu com modal do bootstrap
        event.stopPropagation();
    });

    $("body,html").on("click", function () {
      for (var i = 0; i < menu.length; i++) {
        menu[i].classList.remove("show-dropdown");
      }
      sub_menu_is_showed = -1;
    });

  } catch (error) {
    console.log(error);
  }

  var wW = $(window).width();
    // Right Sidebar
    var right_sidebar = $('.js-right-sidebar');
    var sidebar_btn = $('.js-sidebar-btn');

    sidebar_btn.on('click', function (e) {
      e.preventDefault();
      for (var i = 0; i < menu.length; i++) {
        menu[i].classList.remove("show-dropdown");
      }
      sub_menu_is_showed = -1;
      right_sidebar.toggleClass("show-sidebar");
    });

    $(".js-right-sidebar, .js-sidebar-btn").click(function (event) {
      event.stopPropagation();
    });

    $("body,html").on("click", function () {
      right_sidebar.removeClass("show-sidebar");

    });
 

  // Sublist Sidebar
  try {
    var arrow = $('.js-arrow');
    arrow.each(function () {
      var that = $(this);
      that.on('click', function (e) {
        e.preventDefault();
        that.find(".arrow").toggleClass("up");
        that.toggleClass("open");
        that.parent().find('.js-sub-list').slideToggle("250");
      });
    });

  } catch (error) {
    console.log(error);
  }


  try {
    // Hamburger Menu
    $('.hamburger').on('click', function () {
      $(this).toggleClass('is-active');
      $('.navbar-mobile').slideToggle('500');
    });
    $('.navbar-mobile__list li.has-dropdown > a').on('click', function () {
      var dropdown = $(this).siblings('ul.navbar-mobile__dropdown');
      $(this).toggleClass('active');
      $(dropdown).slideToggle('500');
      return false;
    });
  } catch (error) {
    console.log(error);
  }
})(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  // Load more
  try {
    var list_load = $('.js-list-load');
    if (list_load[0]) {
      list_load.each(function () {
        var that = $(this);
        that.find('.js-load-item').hide();
        var load_btn = that.find('.js-load-btn');
        load_btn.on('click', function (e) {
          $(this).text("Loading...").delay(1500).queue(function (next) {
            $(this).hide();
            that.find(".js-load-item").fadeToggle("slow", 'swing');
          });
          e.preventDefault();
        });
      })

    }
  } catch (error) {
    console.log(error);
  }

})(jQuery);
(function ($) {
  // USE STRICT
  "use strict";

  try {
    
    $('[data-toggle="tooltip"]').tooltip();

  } catch (error) {
    console.log(error);
  }

  // Chatbox
  try {
    var inbox_wrap = $('.js-inbox');
    var message = $('.au-message__item');
    message.each(function(){
      var that = $(this);

      that.on('click', function(){
        $(this).parent().parent().parent().toggleClass('show-chat-box');
      });
    });
    

  } catch (error) {
    console.log(error);
  }

})(jQuery);

function createOption(json,id,callback){
  json.success ? $(id).html(`<option disabled selected="selected">Selecione uma opção</option>${json.response.map(callback).join("")}`):null;
}

// Bootstrap validation
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Bootstrap validation
    $('form').on("submit",function(event) {
      this.classList.add('was-validated');
    });
  }, false);
})();

function getCompressedImage(TagImg,callback){
  const compress = new Compress();
  const files = [...TagImg.files];
  compress.compress(files, {
      size: 4, // the max size in MB, defaults to 2MB
      quality: 0.25, // the quality of the image, max is 1,
      maxWidth: 400, // the max width of the output image, defaults to 1920px
      maxHeight: 400, // the max height of the output image, defaults to 1920px
      resize: true // defaults to true, set false if you do not want to resize the image width and height
  }).then((images) => {
      const img = images[0];
      callback(img);
      // returns an array of compressed images
      // const {
      // endSizeInMb,
      // initialSizeInMb,
      // iterations,
      // sizeReducedInPercent,
      // elapsedTimeInSeconds,
      // alt
      // } = img
      // const output = document.getElementById('output');
      // preview.src = `${img.prefix}${img.data}`
      // let type=preview.src.split(',')[0];
      // output.src=`${type},${img.data}`;
      // console.log(`<b>Start Size:</b> ${initialSizeInMb} MB <br/>
      // <b>End Size:</b> ${endSizeInMb} MB <br/>
      // <b>Compression Cycles:</b> ${iterations} <br/>
      // <b>Size Reduced:</b> ${sizeReducedInPercent} % <br/>
      // <b>File Name:</b> ${alt}`);
  })
}

function HtmlEncode(s){
  var el = document.createElement("div");
  el.innerText = el.textContent = s;
  s = el.innerHTML;
  return s;
}

function getJsonFromForm(formData,ArrayFormData=[],img={}){
  formData=new FormData(formData);
  for (var value of Object.values(ArrayFormData)){
    for (var pair of (new FormData(value)).entries()){
      formData.append(pair[0], pair[1]);
    }
  }
  if (!jQuery.isEmptyObject(img)){
    formData.set('profile_image',typeof img['profile_image']!=='undefined'?img['profile_image']:'');
    formData.set('profile_image_type',typeof img['profile_image_type']!=='undefined'?img['profile_image_type']:'');
  }
  return JSON.stringify(Object.fromEntries(formData));
}

function cleanJson(obj){
  obj=JSON.parse(obj);
  for (var [key, value] of Object.entries(obj)) {
    if (jQuery.isEmptyObject(obj) || value === '' || value === null || value === undefined) {
      delete obj[key];
    }
  }
  obj=JSON.stringify(obj);
  return obj
}

function showMessage(selector,json,alertType='danger',time=0){
  $(selector).show();
  let string='';
  if (typeof json.response !== 'object')
      string=`<div class="alert alert-${alertType}" role="alert">${json.response}</div>`;
  else{
      for (var [key, value] of Object.entries(json.response))
          string+=`<div class="alert alert-${alertType}" role="alert">${value.message}</div>`;
  }
  $(selector).html(string);
  let children=$(selector).children();
  children.animate({left: '10px'},'fast');
  children.animate({left: '-10px'},'fast');
  children.animate({left: '0px'},'fast');
  if (time!=0){
    setTimeout(function() {
      children.hide(500,()=>children.remove());
    }, time);
  }
}