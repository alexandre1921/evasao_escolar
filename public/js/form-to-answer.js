"use strict";

const RequestXML = (method,url,values,callback) => {
	let xml = (() => window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP"))();
	xml.open(method, url, true);
	xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xml.send(JSON.stringify(values));
	xml.onreadystatechange = () => {
		if (xml.readyState==4 && xml.status==200)
			callback(JSON.parse(xml.responseText));
	}
}

var form=[],
	quAns=[],
	green_icon='#00ad5f';

const getId_answer=element=>element.form.querySelector('[name="id_answer"]').id.replace("id_answer_","");
const setId_answer=(element,value)=>element.form.querySelector('[name="id_answer"]').id=value;

const QuestionString = value => `
<div class="row justify-content-center">
    <div class="col-lg-9">
        <div class="card" style="border-radius:10px;margin-bottom:10px;">
			<div class="card-header">
				<div class="row">
					<div class="col-md-1" style="max-width:4%;">
							${value.answered==0 ? `
							<div role="alert" class="au-alert" style="border:none;background:transparent;padding:0px;">
								<i class="zmdi zmdi-close-circle" id="icon_id_question_${value.id_question}" style="color:#d51212;"></i>
							</div>
							` : `
							<div role="alert" class="au-alert" style="border:none;background:transparent;padding:0px;">
								<i class="zmdi zmdi-check-circle"></i>
							</div>
							`}
					</div>
					<div class="col-md-10" style="margin-top: 0.15rem !important;">
						Questão <strong>${quAns.length-1}</strong>
					</div>
				</div>
            </div>
            <div class="card-body card-block">
				<form id="id_question_${value.id_question}">
					<div class="form-row">
						<div class="col-md-12 mb-2">
							<input 
								name="id_answer"
								id="id_answer_${value.id_answer}" 
								type="hidden"
							>
							<label id="label_id_question_${value.id_question}">
								<strong>${value.statement}</strong>
								<center>
								<small class="form-text text-muted">1-Discordo totalmente   
								2-Discordo parcialmente   
								3-Indiferente   
								4-Concordo parcialmente   
								5-Concordo totalmente</small>
								</center>
							</label>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>`;

const AlternativeString = value => `
<div class='form-row'>
	<div class="col-md-1 mb-2">
		<div class="custom-control custom-radio custom-control-inline">
			<input 
				onclick="${quAns[value.id_question]==0 ? 'createAnswer' : 'updateAnswer'}(this)" 
				id="id_alternative_${value.id_alternative}" 
				name="${value.id_question}"
				type="radio"
				class="custom-control-input"
				value="${value.weight}"${value.answered==0 ? '' : 'checked'}
			>
			<label class="custom-control-label" for="id_alternative_${value.id_alternative}">${value.weight}</label>
		</div>
	</div>
</div>`;

const GetListQuestion = () =>
	RequestXML('POST', '/ajax/question',{request:{criteria:'answered',print:true}}, json => {
		ListQuestion(json);
	})

const GetListAlternative = () =>
	RequestXML('POST', '/ajax/alternative',{request:{criteria:'form',print:true}}, json => {
		ListAlternative(json);
	})

function ListQuestion(Question_json,Alternative_json){
	if (Question_json.success==true){
		for (var value of Object.values(Question_json.response)) {
			form[value.id_question]=[];
			quAns[value.id_question]=value.answered;
			document.getElementById("content").innerHTML+=QuestionString(value);
		}
		ListAlternative(Alternative_json);
	}else{
		showMessage("#main-content #message",Question_json,'danger',5000);
	}
}

function ListAlternative(Alternative_json){
	if (Alternative_json.success==true){
		for (var value of Object.values(Alternative_json.response)){
			form[value.id_question].push(value.id_alternative);
			document.getElementById("id_question_"+value.id_question).innerHTML+=AlternativeString(value);
		}
	}else{
		showMessage("#main-content #message",Alternative_json,'danger',5000);
	}
}

function changeOnName(id_question){
	if (quAns[id_question]!=1){
		quAns[id_question]=1;
		document.getElementById(`icon_id_question_${id_question}`).style.color=green_icon;
		document.getElementById(`icon_id_question_${id_question}`).className=`zmdi zmdi-check-circle ${
		typeof(Array.from(document.querySelector("#dropdown-menu-access").classList).find(value=>value=='contrast2'))
        === 'undefined'?``:`contrast`}`;
		for (var value of Object.values(form[id_question])){
			document.getElementById("id_alternative_"+value).setAttribute("onclick",`updateAnswer(this)`);
		}
	}
}

function createAnswer(element){
	changeOnName(element.form.id.replace("id_question_",""));
	RequestXML('POST', '/answer',{id_alternative:element.id.replace("id_alternative_","")}, json => {
		if (json.success==true){
			setId_answer(element,json.response[0].id_answer);
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	});
}

function updateAnswer(element){
	RequestXML('PUT', '/answer',{id_answer:getId_answer(element), id_alternative:element.id.replace("id_alternative_","")}, json => {
		if (json.success==true){
			// code...
		}else{
			showMessage("#main-content #message",json,'danger',5000);
		}
	});
}

