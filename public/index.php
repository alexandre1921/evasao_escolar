<?php
    $_SERVER['DOCUMENT_ROOT']=str_replace('/public','',$_SERVER['DOCUMENT_ROOT']);
    register_shutdown_function(function(){
        $last_error = error_get_last();
        if ($last_error!==NULL) {
            include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/logs/logController.php';
            logController::Log([
                'success' => false,
                'message' => 'Erro',
                'response' => $last_error,
            ],'error');
        }
    });
    $_ENV = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/config/.env');
    require_once $_SERVER['DOCUMENT_ROOT'].'/Controller/routerController.php';
    routerController::router();
?>

