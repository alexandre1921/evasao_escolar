<?php

class question
{
	private $id_question;
	private $statement;
	private $status;
	const VAR_ATTR=[
		'id_question' => [
		  'name' => 'id da questão',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		],
		'statement' => [
			'name' => 'enunciado',
			'type' => 'STRING',
			'min' => 1,
			'max' => 80,
			'null' => false,
		],
	  ];
	  
	/**
	 * Get the value of id_question
	 */ 
	public function getId_question()
	{
		return $this->id_question;
	}

	/**
	 * Set the value of id_question
	 *
	 * @return  self
	 */ 
	public function setId_question($id_question)
	{
		$this->id_question = $id_question;

		return $this;
	}

	/**
	 * Get the value of statement
	 */ 
	public function getStatement()
	{
		return $this->statement;
	}

	/**
	 * Set the value of statement
	 *
	 * @return  self
	 */ 
	public function setStatement($statement)
	{
		$this->statement = $statement;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}
}
?>