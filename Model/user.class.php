<?php
date_default_timezone_set('America/Sao_Paulo');
$dateTime = new DateTime('now');
$dateinterval=new DateInterval('P80Y');
define('MAX_YEAR', $dateTime->format('Y'));
define('MAX_DATE', $dateTime->format('Y-m-d'));
$dateTime->sub($dateinterval);
define('MIN_YEAR', $dateTime->format('Y'));
define('MIN_DATE', $dateTime->format('Y-m-d'));
class user
{
	private $id_user;
	private $birth_date;
	private $status_user;
	private $entry_year;
	private $egress_year;
	private $name_user;
	private $email;
	private $password;
	private $verification_code;
	private $email_status;
	private $id_user_type;
	private $id_adress;
	private $id_course;
	private $profile_image;
	private $profile_image_type;
	const VAR_ATTR=[
        'id_user' => [
          'name' => 'id do usuário',
          'type' => 'NUMERIC',
          'min' => 1,
          'max' => 999999,
		  'null' => false,
		  'hidden' => true,
        ],
        'birth_date' => [
            'name' => 'data de nascimento',
            'type' => 'DATE',
            'min' => MIN_DATE,
            'max' => MAX_DATE,
			'null' => false,
			'hidden' => true,
        ],
        'status_user' => [
            'name' => 'status do usuário',
            'type' => 'NUMERIC',
            'min' => 0,
            'max' => 1,
			'null' => false,
			'hidden' => false,
        ],
        'entry_year' => [
            'name' => 'data de ingresso',
            'type' => 'NUMERIC',
            'min' => MIN_YEAR,
            'max' => MAX_YEAR,// ano atual
			'null' => true,
			'hidden' => true,
        ],
        'egress_year' => [
            'name' => 'data de egresso',
            'type' => 'NUMERIC',
            'min' => MIN_YEAR,
            'max' => MAX_YEAR,// ano atual
			'null' => true,
			'hidden' => true,
        ],
        'name_user' => [
            'name' => 'nome do usuário',
            'type' => 'STRING',
            'min' => 1,
            'max' => 50,
			'null' => false,
			'hidden' => false,
        ],
        'email' => [
            'name' => 'email',
            'type' => 'STRING',
            'min' => 10,
            'max' => 45,
			'null' => false,
			'hidden' => true,
			'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
        ],
        'password' => [
            'name' => 'senha',
            'type' => 'STRING',
            'min' => 4,
            'max' => 60,
			'null' => false,
			'hidden' => true,
		],
		'confirm_password' => [
            'name' => 'confirmar senha',
            'type' => 'STRING',
            'min' => 4,
            'max' => 60,
			'null' => false,
			'hidden' => true,
			'same' => ['attr'=>'password'],
		],
		'verification_code' => [
            'name' => 'código de verificação',
            'type' => 'STRING',
            'min' => 60,
            'max' => 60,
			'null' => true,
			'hidden' => true,
		],
		'email_status' => [
            'name' => 'status do email',
            'type' => 'NUMERIC',
            'min' => 0,
            'max' => 1,
			'null' => false,
			'hidden' => true,
		],
		'profile_image' => [//aproximadamente 12,5 mb
            'name' => 'imagem de perfil',
            'type' => 'BLOB',
            'min' => 1,
            'max' => 16753000,
			'null' => false,
			'hidden' => false,
		],
		'profile_image_type' => [
            'name' => 'tipo da imagem de perfil',
            'type' => 'STRING',
            'min' => 1,
            'max' => 30,
			'null' => false,
			'hidden' => true,
        ],
        'id_user_type' => [
            'name' => 'id do tipo de usuário',
            'type' => 'NUMERIC',
            'min' => 1,
            'max' => 99999999999,
			'null' => false,
			'hidden' => true,
        ],
        'id_adress' => [
            'name' => 'id do endereço',
            'type' => 'NUMERIC',
            'min' => 1,
            'max' => 99999999999,
			'null' => true,
			'hidden' => true,
        ],
        'id_course' => [
            'name' => 'id do curso',
            'type' => 'NUMERIC',
            'min' => 1,
            'max' => 99999999999,
			'null' => false,
			'hidden' => true,
		],
      ];

	/**
	 * Get the value of id_user
	 */ 
	public function getId_user()
	{
		return $this->id_user;
	}

	/**
	 * Set the value of id_user
	 *
	 * @return  self
	 */ 
	public function setId_user($id_user)
	{
		$this->id_user = $id_user;

		return $this;
	}

	/**
	 * Get the value of birth_date
	 */ 
	public function getBirth_date()
	{
		return $this->birth_date;
	}

	/**
	 * Set the value of birth_date
	 *
	 * @return  self
	 */ 
	public function setBirth_date($birth_date)
	{
		$this->birth_date = $birth_date;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus_user()
	{
		return $this->status_user;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus_user($status_user)
	{
		$this->status_user = $status_user;

		return $this;
	}

	/**
	 * Get the value of entry_year
	 */ 
	public function getEntry_year()
	{
		return $this->entry_year;
	}

	/**
	 * Set the value of entry_year
	 *
	 * @return  self
	 */ 
	public function setEntry_year($entry_year)
	{
		$this->entry_year = $entry_year;

		return $this;
	}


	/**
	 * Get the value of egress_year
	 */ 
	public function getEgress_year()
	{
		return $this->egress_year;
	}

	/**
	 * Set the value of egress_year
	 *
	 * @return  self
	 */ 
	public function setEgress_year($egress_year)
	{
		$this->egress_year = $egress_year;

		return $this;
	}

	/**
	 * Get the value of name
	 */ 
	public function getName_user()
	{
		return $this->name_name;
	}

	/**
	 * Set the value of name
	 *
	 * @return  self
	 */ 
	public function setName_user($name_name)
	{
		$this->name_name = $name_name;

		return $this;
	}

	/**
	 * Get the value of email
	 */ 
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @return  self
	 */ 
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get the value of password
	 */ 
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * Set the value of password
	 *
	 * @return  self
	 */ 
	public function setPassword($password)
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * Get the value of id_user_type
	 */ 
	public function getId_user_type()
	{
		return $this->id_user_type;
	}

	/**
	 * Set the value of id_user_type
	 *
	 * @return  self
	 */ 
	public function setId_user_type($id_user_type)
	{
		$this->id_user_type = $id_user_type;

		return $this;
	}

	/**
	 * Get the value of id_adress
	 */ 
	public function getId_adress()
	{
		return $this->id_adress;
	}

	/**
	 * Set the value of id_adress
	 *
	 * @return  self
	 */ 
	public function setId_adress($id_adress)
	{
		$this->id_adress = $id_adress;

		return $this;
	}

	/**
	 * Get the value of id_course
	 */ 
	public function getId_course()
	{
		return $this->id_course;
	}

	/**
	 * Set the value of id_course
	 *
	 * @return  self
	 */ 
	public function setId_course($id_course)
	{
		$this->id_course = $id_course;

		return $this;
	}

	/**
	 * Get the value of profile_image
	 */ 
	public function getProfile_image()
	{
		return $this->profile_image;
	}

	/**
	 * Set the value of profile_image
	 *
	 * @return  self
	 */ 
	public function setProfile_image($profile_image)
	{
		$this->profile_image = $profile_image;

		return $this;
	}

	/**
	 * Get the value of profile_image_type
	 */ 
	public function getProfile_image_type()
	{
		return $this->profile_image_type;
	}

	/**
	 * Set the value of profile_image_type
	 *
	 * @return  self
	 */ 
	public function setProfile_image_type($profile_image_type)
	{
		$this->profile_image_type = $profile_image_type;

		return $this;
	}

	/**
	 * Get the value of verification_code
	 */ 
	public function getVerification_code()
	{
		return $this->verification_code;
	}

	/**
	 * Set the value of verification_code
	 *
	 * @return  self
	 */ 
	public function setVerification_code($verification_code)
	{
		$this->verification_code = $verification_code;

		return $this;
	}

	/**
	 * Get the value of email_status
	 */ 
	public function getEmail_status()
	{
		return $this->email_status;
	}

	/**
	 * Set the value of email_status
	 *
	 * @return  self
	 */ 
	public function setEmail_status($email_status)
	{
		$this->email_status = $email_status;

		return $this;
	}

}
?>