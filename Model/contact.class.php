<?php

class contact
{
	private $id_contact;
	private $telephone;
	private $cellphone;
	private $email;
	private $id_user;
	private $status;
	const VAR_ATTR=[
		'id_contact' => [
		  'name' => 'id do contato',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		  'hidden' => true,
		],
		'id_user' => [
		  'name' => 'id do usuário',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		  'hidden' => true,
		],
		'telephone' => [
			'name' => 'telefone',
			'type' => 'STRING',
			'min' => 0,
			'max' => 15,
			'null' => true,
			'pattern' => '\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$',
			'hidden' => true,
		],
		'cellphone' => [
			'name' => 'celular',
			'type' => 'STRING',
			'min' => 0,
			'max' => 15,
			'null' => true,
			'pattern' => '\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$',
			'hidden' => true,
		],
	  ];

	/**
	 * Get the value of id_contact
	 */ 
	public function getId_contact()
	{
		return $this->id_contact;
	}

	/**
	 * Set the value of id_contact
	 *
	 * @return  self
	 */ 
	public function setId_contact($id_contact)
	{
		$this->id_contact = $id_contact;

		return $this;
	}

	/**
	 * Get the value of telephone
	 */ 
	public function getTelephone()
	{
		return $this->telephone;
	}

	/**
	 * Set the value of telephone
	 *
	 * @return  self
	 */ 
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;

		return $this;
	}

	/**
	 * Get the value of cellphone
	 */ 
	public function getCellphone()
	{
		return $this->cellphone;
	}

	/**
	 * Set the value of cellphone
	 *
	 * @return  self
	 */ 
	public function setCellphone($cellphone)
	{
		$this->cellphone = $cellphone;

		return $this;
	}

	/**
	 * Get the value of id_user
	 */ 
	public function getId_user()
	{
		return $this->id_user;
	}

	/**
	 * Set the value of id_user
	 *
	 * @return  self
	 */ 
	public function setId_user($id_user)
	{
		$this->id_user = $id_user;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}
}
?>