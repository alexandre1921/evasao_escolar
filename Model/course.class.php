<?php

class course
{
	private $id_course;
	private $name_course;
	private $duration;
	private $status_course;
	const VAR_ATTR=[
		'id_course' => [
		  'name' => 'id do curso',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		  'hidden' => true,
		],
		'name_course' => [
			'name' => 'nome do curso',
			'type' => 'STRING',
			'min' => 1,
			'max' => 45,
			'null' => false,
			'hidden' => false,
			'pattern' => '[a-zA-Z\u00C0-\u00FF\s]+$',
		],
		'duration' => [
			'name' => 'duração',
			'type' => 'NUMERIC',
			'min' => 1,
			'max' => 16,
			'null' => false,
			'hidden' => false,
		],
		'status_course' => [
			'name' => 'status do curso',
			'type' => 'NUMERIC',
			'min' => 0,
			'max' => 1,
			'null' => false,
			'hidden' => false,
		],
	  ];
	  
	/**
	 * Get the value of id_course
	 */ 
	public function getId_course()
	{
		return $this->id_course;
	}

	/**
	 * Set the value of id_course
	 *
	 * @return  self
	 */ 
	public function setId_course($id_course)
	{
		$this->id_course = $id_course;

		return $this;
	}

	/**
	 * Get the value of name
	 */ 
	public function getName_course()
	{
		return $this->name_course;
	}

	/**
	 * Set the value of name
	 *
	 * @return  self
	 */ 
	public function setName_course($name_course)
	{
		$this->name_course = $name_course;

		return $this;
	}

	/**
	 * Get the value of duration
	 */ 
	public function getDuration()
	{
		return $this->duration;
	}

	/**
	 * Set the value of duration
	 *
	 * @return  self
	 */ 
	public function setDuration($duration)
	{
		$this->duration = $duration;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus_course()
	{
		return $this->status_course;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus_course($status_course)
	{
		$this->status_course = $status_course;

		return $this;
	}
}
?>