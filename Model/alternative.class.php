<?php

class alternative
{
	private $id_alternative;
	private $weight;
	private $id_question;
	private $status;
	const VAR_ATTR=[
		'id_alternative' => [
		  'name' => 'id da alternativa',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		],
		'weight' => [
			'name' => 'peso',
			'type' => 'NUMERIC',
			'min' => 1,
			'max' => 9,
			'null' => false,
		],
		'id_question' => [
			'name' => 'id da questão',
			'type' => 'NUMERIC',
			'min' => 1,
			'max' => 99999999999,
			'null' => false,
		],
	  ];
	  
	/**
	 * Get the value of id_alternative
	 */ 
	public function getId_alternative()
	{
		return $this->id_alternative;
	}

	/**
	 * Set the value of id_alternative
	 *
	 * @return  self
	 */ 
	public function setId_alternative($id_alternative)
	{
		$this->id_alternative = $id_alternative;

		return $this;
	}

	/**
	 * Get the value of weight
	 */ 
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * Set the value of weight
	 *
	 * @return  self
	 */ 
	public function setWeight($weight)
	{
		$this->weight = $weight;

		return $this;
	}

	/**
	 * Get the value of id_question
	 */ 
	public function getId_question()
	{
		return $this->id_question;
	}

	/**
	 * Set the value of id_question
	 *
	 * @return  self
	 */ 
	public function setId_question($id_question)
	{
		$this->id_question = $id_question;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}
}
?>