<?php

class user_type
{
	private $id_user_type;
	private $description;
	private $type;
	const VAR_ATTR=[
		'id_user_type' => [
		  'name' => 'id do tipo de usuário',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		  'hidden' => true,
		],
		'description' => [
			'name' => 'descrição',
			'type' => 'STRING',
			'min' => 1,
			'max' => 20,
			'null' => false,
			'hidden' => false,
		  ],
		'type' => [
			'name' => 'tipo',
			'type' => 'NUMERIC',
			'min' => 1,
			'max' => 3,
			'null' => false,
			'hidden' => true,
		  ],
	  ];
	  
	/**
	 * Get the value of id_user_type
	 */ 
	public function getId_user_type()
	{
		return $this->id_user_type;
	}

	/**
	 * Set the value of id_user_type
	 *
	 * @return  self
	 */ 
	public function setId_user_type($id_user_type)
	{
		$this->id_user_type = $id_user_type;

		return $this;
	}

	/**
	 * Get the value of description
	 */ 
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set the value of description
	 *
	 * @return  self
	 */ 
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get the value of type
	 */ 
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Set the value of type
	 *
	 * @return  self
	 */ 
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}
}
?>