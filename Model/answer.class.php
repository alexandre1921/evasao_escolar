<?php

class answer
{
	private $id_answer;
	private $id_alternative;
	private $status;
	const VAR_ATTR=[
		'id_answer' => [
		  'name' => 'id da resposta',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 99999999999,
		  'null' => false,
		],
		'id_alternative' => [
			'name' => 'id da alternativa',
			'type' => 'NUMERIC',
			'min' => 1,
			'max' => 99999999999,
			'null' => false,
		],
	  ];
	  
	/**
	 * Get the value of id_answer
	 */ 
	public function getId_answer()
	{
		return $this->id_answer;
	}

	/**
	 * Set the value of id_answer
	 *
	 * @return  self
	 */ 
	public function setId_answer($id_answer)
	{
		$this->id_answer = $id_answer;

		return $this;
	}


	/**
	 * Get the value of id_alternative
	 */ 
	public function getId_alternative()
	{
		return $this->id_alternative;
	}

	/**
	 * Set the value of id_alternative
	 *
	 * @return  self
	 */ 
	public function setId_alternative($id_alternative)
	{
		$this->id_alternative = $id_alternative;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}
}
?>