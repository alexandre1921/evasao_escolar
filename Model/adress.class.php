<?php

class adress
{
	private $id_adress;
	private $city;
	private $neighborhood;
	private $street;
	private $number;
	private $complement;
	private $status;
	const VAR_ATTR=[
		'id_adress' => [
		  'name' => 'id do endereço',
		  'type' => 'NUMERIC',
		  'min' => 1,
		  'max' => 999999,
		  'null' => false,
		  'hidden' => true,
		],
		'city' => [
			'name' => 'cidade',
			'type' => 'STRING',
			'min' => 0,
			'max' => 30,
			'null' => true,
			'hidden' => true,
		],
		'neighborhood' => [
			'name' => 'bairro',
			'type' => 'STRING',
			'min' => 0,
			'max' => 20,
			'null' => true,
			'hidden' => true,
		],
		'street' => [
			'name' => 'rua',
			'type' => 'STRING',
			'min' => 0,
			'max' => 20,
			'null' => true,
			'hidden' => true,
		],
		'number' => [
			'name' => 'número',
			'type' => 'NUMERIC',
			'min' => 0,
			'max' => 999999,
			'null' => true,
			'hidden' => true,
		],
		'complement' => [
			'name' => 'complemento',
			'type' => 'STRING',
			'min' => 0,
			'max' => 15,
			'null' => true,
			'hidden' => true,
		],
	  ];

	/**
	 * Get the value of id_adress
	 */ 
	public function getId_adress()
	{
		return $this->id_adress;
	}

	/**
	 * Set the value of id_adress
	 *
	 * @return  self
	 */ 
	public function setId_adress($id_adress)
	{
		$this->id_adress = $id_adress;

		return $this;
	}

	/**
	 * Get the value of city
	 */ 
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set the value of city
	 *
	 * @return  self
	 */ 
	public function setCity($city)
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * Get the value of neighborhood
	 */ 
	public function getNeighborhood()
	{
		return $this->neighborhood;
	}

	/**
	 * Set the value of neighborhood
	 *
	 * @return  self
	 */ 
	public function setNeighborhood($neighborhood)
	{
		$this->neighborhood = $neighborhood;

		return $this;
	}

	/**
	 * Get the value of street
	 */ 
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * Set the value of street
	 *
	 * @return  self
	 */ 
	public function setStreet($street)
	{
		$this->street = $street;

		return $this;
	}

	/**
	 * Get the value of number
	 */ 
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * Set the value of number
	 *
	 * @return  self
	 */ 
	public function setNumber($number)
	{
		$this->number = $number;

		return $this;
	}

	/**
	 * Get the value of complement
	 */ 
	public function getComplement()
	{
		return $this->complement;
	}

	/**
	 * Set the value of complement
	 *
	 * @return  self
	 */ 
	public function setComplement($complement)
	{
		$this->complement = $complement;

		return $this;
	}

	/**
	 * Get the value of status
	 */ 
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set the value of status
	 *
	 * @return  self
	 */ 
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}
}
?>