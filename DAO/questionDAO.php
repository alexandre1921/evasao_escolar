<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/question.class.php';

class questionDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }

   public function create(question $question){
		try{
			$stmt = $this->con->prepare('INSERT INTO question (statement) VALUES(:statement)');
			$response=$stmt->execute(array(
				':statement' => $question->getStatement(),
			));
			$response=array();
			$response[0]=[
				'id_question' => $this->con->lastInsertId(),
				'statement' => $question->getStatement(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar questão',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar questão',
				'response' => $e->getMessage(),
			];
		}
		return $result;
    }

	public function read($criteria,question $question=null) {
		try{
			switch ($criteria) {
				case "all":
					$stmt = $this->con->prepare('select * from question');
					$stmt->execute();
					break;
				case "answered":
					$stmt = $this->con->prepare('
					select 1 as answered, t1.id_question, t1.statement, id_answer from 
					(select q.*, an.id_answer from question q 
					inner join (
					alternative al, 
					answer an, 
					user_has_answer ua) on 
					q.id_question=al.id_question &&
					an.id_alternative=al.id_alternative &&
					an.id_answer=ua.id_answer 
					where ua.id_user=:id_user) t1 
					UNION
					select 0 as answered, q.id_question, q.statement, null id_answer from question q where id_question not in
					(select q.id_question from question q 
					inner join (
					alternative al, 
					answer an, 
					user_has_answer ua) on 
					q.id_question=al.id_question &&
					an.id_alternative=al.id_alternative &&
					an.id_answer=ua.id_answer 
					where ua.id_user=:id_user) order by id_question
					');
					$response=$stmt->execute(array(
						':id_user' => $_SESSION['id_user'],
					));
					break;
				case "AnsweredHowManyTimes":
					$stmt = $this->con->prepare('select q.id_question from question q 
					inner join (
					alternative al, 
					answer an) on 
					q.id_question=al.id_question &&
					an.id_alternative=al.id_alternative where q.id_question=:id_question');
					$response=$stmt->execute(array(
						':id_question' => $question->getId_question(),
					));
					break;
				default:
					return $result=[
						'success' => false,
						'message' => 'Falha ao listar questão',
						'response' => 'Critério indefinido',
					];
			}
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar questão',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar questão',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}

  	public function update(question $question){
		$result=$this->read('AnsweredHowManyTimes',$question);
		if ($result['success']==true)
			if (count($result['response'])<1){
				try{
					$stmt = $this->con->prepare('UPDATE question SET statement=:statement WHERE id_question=:id_question');
					$response=$stmt->execute(array(
						':id_question' => $question->getId_question(),
						':statement' => $question->getStatement(),
					));
					$result=[
						'success' => true,
						'message' => 'Sucesso ao atualizar questão',
						'response' => $response,
					];
				}catch(PDOException $e){
					$result=[
						'success' => false,
						'message' => 'Falha ao atualizar questão',
						'response' => $e->getMessage(),
					];
				}
			}else{
				$result['success']=false;
				$result['message']='Falha ao atualizar questão';
				$result['response']='Esta questão já foi respondida';
			}
		return $result;
	}

	public function delete(question $question){
		try{
		 	$stmt = $this->con->prepare('DELETE FROM question WHERE id_question=:id_question');
		 	$response=$stmt->execute(array(
			 	':id_question' => $question->getId_question(),
		 	));
		 	$result=[
			 	'success' => true,
			 	'message' => 'Sucesso ao deletar questão',
			 	'response' => $response,
		 	];
	 	}catch(PDOException $e){
		 	$result=[
			 	'success' => false,
			 	'message' => 'Falha ao deletar questão',
			 	'response' => $e->getMessage(),
		 	];
	 	}
		return $result;
 	}
}
?>