<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/contact.class.php';

class contactDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }
   public function create(contact $contact){
		try{
			$stmt = $this->con->prepare('INSERT INTO contact (telephone,cellphone,id_user) VALUES(:telephone,:cellphone,:id_user)');
			$stmt->execute(array(
				':telephone' => $contact->getTelephone(),
				':cellphone' => $contact->getCellphone(),
				':id_user' => $contact->getId_user(),
			));
			$response=array();
			$response[0]=[
				'id_contact' => $this->con->lastInsertId(),
				'telephone' => $contact->getTelephone(),
				'cellphone' => $contact->getCellphone(),
				'id_user' => $contact->getId_user(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar contato',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar contato',
				'response' => $e->getMessage(),
			];
		}
		return $result;
   }

   public function read() {
		try{
			$stmt = $this->con->prepare('select * from contact');
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar contato',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar contato',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}

  public function update(contact $contact){
		try{
			$stmt = $this->con->prepare('UPDATE contact SET telephone=:telephone , cellphone=:cellphone , id_user=:id_user WHERE id_contact=:id_contact');
			$stmt->execute(array(
				':id_contact' => $contact->getId_contact(),
				':telephone' => $contact->getTelephone(),
				':cellphone' => $contact->getCellphone(),
				':id_user' => $contact->getId_user(),
			));
			$current_user=$response[0];
			$_SESSION['cellphone']=$contact->getCellphone();
			$_SESSION['telephone']=$contact->getTelephone();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao atualizar contato',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar contato',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function delete(contact $contact){
		try{
		 $stmt = $this->con->prepare('DELETE FROM contact WHERE id_contact=:id_contact');
		 $response=$stmt->execute(array(
			 ':id_contact' => $contact->getId_contact(),
		 ));
		 $result=[
			 'success' => true,
			 'message' => 'Sucesso ao deletar contato',
			 'response' => $response,
		 ];
	 }catch(PDOException $e){
		 $result=[
			 'success' => false,
			 'message' => 'Falha ao deletar contato',
			 'response' => $e->getMessage(),
		 ];
	 }
	 return $result;
 }
}
?>