<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/alternative.class.php';

class alternativeDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }
   public function create(alternative $alternative){
		try{
			$stmt = $this->con->prepare('INSERT INTO alternative (weight,id_question) VALUES(:weight,:id_question)');
			$stmt->execute(array(
				':weight' => $alternative->getWeight(),
				':id_question' => $alternative->getId_question(),
			));
			$response[0]=[
				'id_alternative' => $this->con->lastInsertId(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar opção',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar opção',
				'response' => $e->getMessage(),
			];
		}
		return $result;
   }

   public function read($criteria) {
		try{
			switch ($criteria) {
				case 'all':
					$stmt = $this->con->prepare('select * from alternative');
					$stmt->execute();
					break;
				case 'answered':
					$stmt = $this->con->prepare('
					select 1 as answered,al.* from alternative al inner join (user_has_answer ua, answer an) 
on al.id_alternative=an.id_alternative &&
ua.id_answer=an.id_answer 
where id_user=:id_user
union
select 0 as answered, al.* from alternative al where
al.id_alternative NOT IN (select al.id_alternative from alternative al inner join (user_has_answer ua, answer an) 
on al.id_alternative=an.id_alternative &&
ua.id_answer=an.id_answer 
where id_user=:id_user) order by id_question');
					$response=$stmt->execute(array(
						':id_user' => $_SESSION['id_user'],
					));
					break;
				case 'form':
						$stmt = $this->con->prepare('
						select 1 as answered,al.* from alternative al inner join (user_has_answer ua, answer an) 
on al.id_alternative=an.id_alternative &&
ua.id_answer=an.id_answer 
where id_user=:id_user
union
select 0 as answered, al.* from alternative al where
al.id_alternative NOT IN (select al.id_alternative from alternative al inner join (user_has_answer ua, answer an) 
on al.id_alternative=an.id_alternative &&
ua.id_answer=an.id_answer 
where id_user=:id_user) order by weight');
						$response=$stmt->execute(array(
							':id_user' => $_SESSION['id_user'],
						));
					break;
				case 'arff':
					$stmt = $this->con->prepare('
					select ua.id_user, al.id_alternative, al.weight, al.id_question 
from alternative al inner join (user_has_answer ua, answer an) 
on al.id_alternative=an.id_alternative &&
ua.id_answer=an.id_answer order by id_question');
					$response=$stmt->execute();
					break;
				default:
					return $result=[
						'success' => false,
						'message' => 'Falha ao listar opção',
						'response' => 'Critério indefinido',
					];
			}
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar opção',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar opção',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}

  public function update(alternative $alternative){
		try{
			$stmt = $this->con->prepare('UPDATE alternative SET weight=:weight , id_question=:id_question WHERE id_alternative=:id_alternative');
			$stmt->execute(array(
				':id_alternative' => $alternative->getId_alternative(),
				':weight' => $alternative->getWeight(),
				':id_question' => $alternative->getId_question(),
			));
			$response[0]=[
				'id_alternative' => $this->con->lastInsertId(),
				'weight' => $alternative->getWeight(),
				'id_question' => $alternative->getId_question(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao atualizar opção',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar opção',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function delete(alternative $alternative){
		try{
		 $stmt = $this->con->prepare('DELETE FROM alternative WHERE id_alternative=:id_alternative');
		 $response=$stmt->execute(array(
			 ':id_alternative' => $alternative->getId_alternative(),
		 ));
		 $result=[
			 'success' => true,
			 'message' => 'Sucesso ao deletar opção',
			 'response' => $response,
		 ];
	 }catch(PDOException $e){
		 $result=[
			 'success' => false,
			 'message' => 'Falha ao deletar opção',
			 'response' => $e->getMessage(),
		 ];
	 }
	 return $result;
 }
}
?>