<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/user_type.class.php';

class user_typeDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }
   public function create(user_type $user_type){
		try{
			$stmt = $this->con->prepare('INSERT INTO user_type (description,type) VALUES(:description,:type)');
			$response=$stmt->execute(array(
				':description' => $user_type->getDescription(),
				':type' => $user_type->getType(),
			));
			$response=array();
			$response[0]=[
				'id_user_type' => $this->con->lastInsertId(),
				'description' => $user_type->getDescription(),
				'type' => $user_type->getType(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar tipo de usuário',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar tipo de usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
   }

   public function read() {
		try{
			$stmt = $this->con->prepare('SELECT * FROM user_type');
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar tipo de usuário',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar tipo de usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function getIdStudent(){
		try{
			$stmt = $this->con->prepare('SELECT id_user_type from user_type WHERE type=:type');
			$response=$stmt->execute(array(
				':type' => 3,
			));
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			if ($response!=[]){
				$result=[
					'success' => true,
					'message' => 'Sucesso ao buscar tipo de usuário',
					'response' => $response,
				];
			}else{
				$result=[
					'success' => false,
					'message' => 'Falha ao buscar tipo de usuário',
					'response' => 'Não há registros há registros no banco de dados',
				];
			}
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao buscar tipo de usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	 }
  public function update(user_type $user_type){
		try{
			$stmt = $this->con->prepare('UPDATE user_type SET description=:description , type=:type WHERE id_user_type=:id_user_type');
			$response=$stmt->execute(array(
				':id_user_type' => $user_type->getId_user_type(),
				':description' => $user_type->getDescription(),
				':type' => $user_type->getType(),
			));
			$result=[
				'success' => true,
				'message' => 'Sucesso ao atualizar tipo de usuário',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar tipo de usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function delete(user_type $user_type){
		try{
		 $stmt = $this->con->prepare('DELETE FROM user_type WHERE id_user_type=:id_user_type');
		 $response=$stmt->execute(array(
			 ':id_user_type' => $user_type->getId_user_type(),
		 ));
		 $result=[
			 'success' => true,
			 'message' => 'Sucesso ao deletar tipo de usuário',
			 'response' => $response,
		 ];
	 }catch(PDOException $e){
		 $result=[
			 'success' => false,
			 'message' => 'Falha ao deletar tipo de usuário',
			 'response' => $e->getMessage(),
		 ];
	 }
	 return $result;
 }
}
?>