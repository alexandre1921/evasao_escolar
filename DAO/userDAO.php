<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/connection.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/sqlfactory/tsqlupdate.class.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/sqlfactory/tsqlselect.class.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/sqlfactory/tsqlinsert.class.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/sqlfactory/tsqldelete.class.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/user.class.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/logs/logController.php';

class userDAO{
   
   private $con;
   private $tsqlSelect;

   function __construct(){
		$this->con=Connection::conectar();
   }

   public function create(user $user){
		$result=$this->read('search_if_exists',$user);
		if (count($result['response'])>0){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar usuário',
				'response' => 'Este email já está sendo usado',
			];
		}else{
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar usuário',
				'response' => 'Email disponivel',
			];
			try{
				$tsql = new Tsqlselect;
				$tsql->setEntity('user_type');
				$tsql->addColumn('id_user_type');
				$tsql->setCriteria('type=3');
				$stmt = $this->con->prepare($tsql->getInstruction());
				$stmt->execute();
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				$id_user_type = $stmt->fetchAll()[0]['id_user_type'];

				$verification_code=password_hash(uniqid(), PASSWORD_BCRYPT);

				include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/adressController.php';
				$adressController=new adressController;
				$adressController->contents=['city'=>'','neighborhood'=>'','street'=>'','number'=>0,'complement'=>''];
				$resultAdress=$adressController->create();

				$tsql = new TsqlInsert;
				$tsql->setEntity('user');
				$tsql->setRowData([
					'birth_date'         => $user->getBirth_date(),
					'status_user'        => 1,
					'entry_year'         => $user->getEntry_year(),
					'name_user'          => $user->getName_user(),
					'email'              => $user->getEmail(),
					'email_status'       => 0,
					'verification_code'  => $verification_code,
					'password'           => password_hash($user->getPassword(), PASSWORD_BCRYPT),
					'id_adress'          => $resultAdress['response'][0]['id_adress'],
					'id_user_type'       => $_SESSION['user_type']<3?$user->getId_user_type():$id_user_type,
					'id_course'          => $user->getId_course(),
					'profile_image'      => $user->getProfile_image(),
					'profile_image_type' => $user->getProfile_image_type(),
				]);
				if ($user->getEgress_year()!=null){
					$tsql->setRowData(['egress_year'=>$user->getEgress_year()]);
				}
				$stmt = $this->con->prepare($tsql->getInstruction());
				$stmt->execute();
				
				$response[0]=[
					'id_user' => $this->con->lastInsertId(),
					'status_user' => $user->getStatus_user(),
				];
				
				include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/contactController.php';
				$contactController=new contactController;
				$contactController->contents=['id_user'=>$response[0]['id_user'],'telephone'=>'','cellphone'=>''];
				$contactController->create();

				$result=[
					'success' => true,
					'message' => 'Sucesso ao criar usuário',
					'response' => 'Sucesso, agora ative sua conta acessando o link enviado no email cadastrado',
				];
				$mail = new PHPMailer();
				try {
					$mail->isSMTP();                                            // Send using SMTP
					$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
					$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					$mail->SMTPSecure = 'ssl';
					$mail->Username   = $_ENV['EMAIL_ADRESS'];                     // SMTP username
					$mail->Password   = $_ENV['EMAIL_PASSWORD'];                               // SMTP password
					$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
					$mail->Port       = 587;                                    // TCP port to connect to
					$mail->CharSet = 'UTF-8';
					//Recipients
					$mail->setFrom($_ENV['EMAIL_ADRESS'], $_ENV['SITE_NAME']);
					$mail->addAddress($user->getEmail());     // Add a recipient
				
					// Content
					$mail->isHTML(true);                                  // Set email format to HTML
					$mail->Subject = 'Ativação da conta';
					$mail->Body    = 'Para ativar a sua conta acesse: <a>'.$_ENV['SITE_URL'].'/AtivarMinhaConta?email='.$user->getEmail().'&verification_code='.$verification_code.'</a>';
					$mail->AltBody = 'Para ativar a sua conta acesse: <a>'.$_ENV['SITE_URL'].'/AtivarMinhaConta?email='.$user->getEmail().'&verification_code='.$verification_code.'</a>';
				
					$mail->send();
				} catch (Exception $e) {
					$result=[
						'success' => false,
						'message' => 'A mensagem não pode ser enviada. Mailer Error',
						'response' => ['error'=>$mail->ErrorInfo],
					];
				}
			}catch(PDOException $e){
				$result=[
					'success' => false,
					'message' => 'Falha ao criar usuário',
					'response' => [$e->getMessage(),$this->con->prepare($tsql->getInstruction())->debugDumpParams()],
				];
			}
		}
		return $result;
   }

   public function read($criteria,user $user=null) {
		try{
			$tsql = new TsqlSelect;
			$tsql->setEntity('user u');
			switch ($criteria) {
				case "all":
					$tsql->addColumn(['u.id_user','u.birth_date','u.status_user','u.entry_year','u.egress_year','u.name_user',
					'u.email','u.email_status','u.profile_image','u.profile_image_type','ut.description','c.name_course',
					'co.telephone','co.cellphone','ad.city','ad.neighborhood','ad.street','ad.number','ad.complement']);
					$tsql->addInnerJoin(['user_type ut','course c','contact co','adress ad']);
					$tsql->addOnAttr(['u.id_user_type=ut.id_user_type','u.id_course=c.id_course','u.id_user=co.id_user','ad.id_adress=u.id_adress']);
					$tsql->setCriteria('u.id_user<>'.$_SESSION['id_user']);
					break;
				case "search":
					$tsql->addColumn('*');
					$tsql->setCriteria('u.id_user='.$_SESSION['id_user']);
					break;//select name_user from user where id_course=1;
				case "course_has_user":
					$tsql->addColumn('name_user');
					$tsql->setCriteria('u.id_course='.$_SESSION['id_user']);
					break;
				case "search_if_exists":
					$tsql->addColumn('id_user');
					$tsql->setCriteria('u.email="'.$user->getEmail().'"');
					break;
				case "search_by_verification_code":
					$tsql->addColumn('id_user');
					$tsql->setCriteria('u.email="'.$user->getEmail().'" && u.verification_code="'.$user->getVerification_code().'" && verification_code<>"0"');
					break;
				default:
					return $result=[
						'success' => false,
						'message' => 'Falha ao listar usuário',
						'response' => 'Critério indefinido',
					];
			}
			$stmt = $this->con->prepare($tsql->getInstruction());
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar usuário',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar usuário',
				'response' => [$e->getMessage(),$tsql->getInstruction()],
			];
		}
		return $result;
	}

  public function update(user $user){
		try{
			$tsql = new TsqlUpdate;
			$tsql->setEntity('user');
			$current_user=array();
			if ($user->getBirth_date()!==null)
				$current_user['birth_date']=$user->getBirth_date();
			if ($user->getStatus_user() !== null)
				$current_user['status_user']=$user->getStatus_user();
			if ($user->getEgress_year()!==null)
				$current_user['egress_year']=$user->getEgress_year();
			if ($user->getEntry_year()!==null)
				$current_user['entry_year']=$user->getEntry_year();
			if ($user->getName_user()!==null)
				$current_user['name_user']=$user->getName_user();
			if ($user->getEmail()!==null)
				$current_user['email']=$user->getEmail();
			if ($user->getPassword()!==null)
				$current_user['password']=password_hash($user->getPassword(), PASSWORD_BCRYPT);
			if ($user->getId_user_type()!==null)
				$current_user['id_user_type']=$user->getId_user_type();
			if ($user->getId_course()!==null)
				$current_user['id_course']=$user->getId_course();	
			if ($user->getProfile_image()!==null)
				$current_user['profile_image']=$user->getProfile_image();
			if ($user->getProfile_image_type()!==null)
				$current_user['profile_image_type']=$user->getProfile_image_type();
			if ($user->getVerification_code()!==null)
				$current_user['verification_code']=$user->getVerification_code();
			if ($user->getEmail_status()!==null)
				$current_user['email_status']=$user->getEmail_status();
			$tsql->setRowData($current_user);
			$tsql->setCriteria('id_user='.$user->getId_user());//var_dump($tsql->getInstruction());
			$response = $this->con->prepare($tsql->getInstruction())->execute();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao atualizar usuário',
				'response' => null,
			];
			if (isset($_SESSION['id_user']) && $user->getId_user()==$_SESSION['id_user'])
				foreach ($current_user as $key => $value) {
					if ($value!==null)
						$_SESSION[$key]=$value;
				}
			
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	
	public function delete(user $user){
		if ($_SESSION['id_user']!=$user->getId_user()){
			try{
				$tsql = new TsqlDelete;
				$tsql->setEntity('user');
				$tsql->setCriteria('id_user='.$user->getId_user());
				$response = $this->con->prepare($tsql->getInstruction())->execute();
				$result=[
					'success' => true,
					'message' => 'Sucesso ao deletar usuário',
					'response' => $response,
				];
			}catch(PDOException $e){
				$result=[
					'success' => false,
					'message' => 'Falha ao deletar usuário',
					'response' => $e->getMessage(),
				];
			}
		}else{
			$result=[
				'success' => false,
				'message' => 'Falha ao deletar usuário',
				'response' => 'O usuário a ser apagado não pode se apagar',
			];
		}
		return $result;
 	}

 public function login(user $user) {
		try{
			$tsql = new TsqlSelect;
			$tsql->setEntity('user u');
			$tsql->addColumn(['*']);
			$tsql->addInnerJoin(['user_type ut','contact co','adress ad']);
			$tsql->addOnAttr(['u.id_user_type=ut.id_user_type','u.id_user=co.id_user','u.id_adress=ad.id_adress']);
			$tsql->setCriteria('email="'.$user->getEmail().'"');
			$stmt = $this->con->prepare($tsql->getInstruction());
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			if ($response!=[]){
				$current_user=$response[0];
				if ($current_user['status_user']==1){
					if ($current_user['email_status']==1){
						if (password_verify($user->getPassword(), $response[0]['password'])){
							$result=[
								'success' => true,
								'message' => 'Sucesso ao fazer login do usuário',
								'response' => null,
							];
							$_SESSION['id_user']=$current_user['id_user'];
							$_SESSION['name_user']=$current_user['name_user'];  
							$_SESSION['email']=$current_user['email'];
							$_SESSION['user_type']=$current_user['type']; 
							$_SESSION['birth_date']=$current_user['birth_date'];
							$_SESSION['entry_year']=$current_user['entry_year'];
							$_SESSION['egress_year']=$current_user['egress_year'];
							$_SESSION['id_user_type']=$current_user['id_user_type']; 
							$_SESSION['id_adress']=$current_user['id_adress'];
							$_SESSION['city']=$current_user['city'];
							$_SESSION['neighborhood']=$current_user['neighborhood'];
							$_SESSION['street']=$current_user['street'];
							$_SESSION['number']=$current_user['number'];
							$_SESSION['complement']=$current_user['complement'];
							$_SESSION['id_contact']=$current_user['id_contact'];
							$_SESSION['cellphone']=$current_user['cellphone'];
							$_SESSION['telephone']=$current_user['telephone'];
							logController::Log([
								'success' => true,
								'message' => 'Sucesso ao fazer login do usuário',
								'response' => ['Id do usuário'=>$_SESSION['id_user']],
							],'access');
						}else{
							$result=[
								'success' => false,
								'message' => 'Falha ao fazer login do usuário',
								'response' => 'Login ou senha inválidos',
							];
						}
					}else{
						$result=[
							'success' => false,
							'message' => 'Falha ao fazer login do usuário',
							'response' => 'Email não foi confirmado',
						];
					}
				}else{
					$result=[
						'success' => false,
						'message' => 'Falha ao fazer login do usuário',
						'response' => $current_user,
					];
				}
			}else
				$result=[
					'success' => false,
					'message' => 'Falha ao fazer login',
					'response' => 'Usuário não cadastrado',
				];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao login do usuário',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}

	public function recover(user $user){
		$result=$this->read('search_if_exists',$user);
		if ($result['success']==true){
			if (count($result['response'])>0){
				$verification_code=password_hash(uniqid(), PASSWORD_BCRYPT);
				$user->setVerification_code($verification_code);
				$user->setId_user($result['response'][0]['id_user']);
				$result=$this->update($user);
				if ($result['success']==true){
					$mail = new PHPMailer();
					try {
						//Server settings
						// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
						$mail->isSMTP();                                            // Send using SMTP
						$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
						$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
						$mail->SMTPSecure = 'ssl';
						$mail->Username   = $_ENV['EMAIL_ADRESS'];                     // SMTP username
						$mail->Password   = $_ENV['EMAIL_PASSWORD'];                               // SMTP password
						$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
						$mail->Port       = 587;                                    // TCP port to connect to
						$mail->CharSet = 'UTF-8';
						//Recipients
						$mail->setFrom($_ENV['EMAIL_ADRESS'], $_ENV['SITE_NAME']);
						$mail->addAddress($user->getEmail());     // Add a recipient
					
						// Content
						$mail->isHTML(true);                                  // Set email format to HTML
						$mail->Subject = 'Recuperação da conta';
						$mail->Body    = 'Código de verificação: <b>'.$verification_code.'</b>';
						$mail->AltBody = 'Código de verificação: '.$verification_code;
					
						$mail->send();
						$result=[
							'success' => true,
							'message' => 'Mailer ok',
							'response' => 'A mensagem foi enviada',
						];
					} catch (Exception $e) {
						$result=[
							'success' => false,
							'message' => 'A mensagem não pode ser enviada. Mailer Error',
							'response' => ['error'=>$mail->ErrorInfo],
						];
					}
				}
			}else{
				$result=[
					'success' => false,
					'message' => 'Falha ao recuperar usuário',
					'response' => 'Usuário não encontrado',
				];
			}
		}
		return $result;
	}

	public function verificateCode(user $user){
		$result=$this->read('search_by_verification_code',$user);
		if (count($result['response'])>0){
			$result=[
				'success' => true,
				'message' => 'Sucesso ao verificar código',
				'response' => 'Código válido',
			];
		}else{
			$result=[
				'success' => false,
				'message' => 'Falha ao verificar código',
				'response' => 'Código inválido',
			];
		}
		return $result;
    }
}
?>