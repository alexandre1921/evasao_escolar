<?php
class Connection{
  static function conectar(){
    try{
      $con = new PDO('mysql:host='.$_ENV['DB_HOST'].';dbname='.$_ENV['DB_DATABASE'].';charset='.$_ENV['DB_CHARSET'],$_ENV['DB_USERNAME'],$_ENV['DB_PASSWORD']);
      $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $con;
    }catch(PDOException $e){
      $result=[
        'success' => false,
        'message' => 'Falha na conexão com o banco de dados',
				'response' => $e->getMessage(),
      ];
    }
    echo json_encode($result);
   }
 }
?>