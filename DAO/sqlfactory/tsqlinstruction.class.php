<?php

	abstract class TsqlInstruction{
		protected $sql;
		protected $criteria;
		protected $entity;

		final public function setEntity($entityparam){
			$this->entity = $entityparam;
		}

		final public function getEntity(){
			return $this->entity;
		}

		final public function setCriteria($criteria){
			$this->criteria = $criteria;
		} 

		final public function getCriteria(){
			return $this->criteria;
		} 
		
		abstract public function getInstruction();


	}

?>