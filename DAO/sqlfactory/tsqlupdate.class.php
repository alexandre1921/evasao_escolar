<?php
	include_once 'tsqlinstruction.class.php';

	final class TsqlUpdate extends TsqlInstruction{	
		private $columnValues; 
	
		public function setRowData($RowValues){
			foreach ($RowValues as $key => $value) {
				$this->columnValues[$key] = $value;
			}
		}

		public function getInstruction(){
			$this->sql = "UPDATE {$this->entity} ";
			if($this->columnValues){
				$set = array_map(function ($v, $k) { return sprintf("%s='%s'", $k, $v); },$this->columnValues,array_keys($this->columnValues));
			}
			$this->sql .= ' SET '. implode(', ', $set);
			if ($this->criteria){
				$this->sql .= ' WHERE ' .$this->criteria." ;";
			}

			return $this->sql;
		}
	}
?>