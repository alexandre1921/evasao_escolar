<?php
	include_once 'tsqlinstruction.class.php';

	final class TsqlDelete extends TsqlInstruction{
		public function getInstruction(){
			$this->sql = "DELETE FROM {$this->entity}". 
			($this->criteria ? ' WHERE '.$this->criteria : '');
			return $this->sql;
		}
	}


?>