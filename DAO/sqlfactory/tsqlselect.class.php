<?php
	include_once 'tsqlinstruction.class.php';

	final class TsqlSelect extends TsqlInstruction{
		private $column;
		private $order;
		private $limit;
		private	$offset;
		private	$InnerJoin;
		private	$addOnAttr;

		public function addColumn($column){
			if (is_array($column)){
				$this->column = is_array($this->column) ? array_merge($this->column,$column) : $column;
			}else{
				$this->column[] = $column;
			}
		}

		public function addInnerJoin($InnerJoin){
			if (is_array($InnerJoin)){
				$this->InnerJoin = is_array($this->InnerJoin)? array_merge($this->InnerJoin,$InnerJoin) : $InnerJoin;
			}else{
				$this->InnerJoin[] = $InnerJoin;
			}
		}

		public function addOnAttr($addOnAttr){
			if (is_array($addOnAttr)){
				$this->addOnAttr = is_array($this->addOnAttr)? array_merge($this->addOnAttr,$addOnAttr) : $addOnAttr;
			}else{
				$this->addOnAttr[] = $addOnAttr;
			}
		}

		public function setOrder($orderparam){
			$this->order = $orderparam;
		}

		public function setLimit($limitparam){
			$this->limit = $limitparam;
		}

		public function setOffset($offsetparam){
			$this->offset = $offsetparam;
		}
		
		public function getInstruction(){
			$this->sql = 'SELECT '.implode(', ', $this->column).' FROM '.$this->entity;
			if ($this->InnerJoin){
				$this->sql .= ' inner join ('.implode(', ', $this->InnerJoin).') ON';
			}
			if ($this->addOnAttr){
				$this->sql .= ' '.implode(' && ', $this->addOnAttr);
			}
			if ($this->criteria){
				if($this->criteria){
					$this->sql.= ' WHERE '.$this->criteria;
				}
				if ($this->order) {
					$this->sql.= ' ORDER BY '.$this->order;
				}
				if ($this->limit) {
					$this->sql.= ' LIMIT '.$this->limit;
				}
				if ($this->offset) {
					$this->sql.= ' OFFSET '.$this->offset;
				}
			}
			return $this->sql;
		}
	}


?>