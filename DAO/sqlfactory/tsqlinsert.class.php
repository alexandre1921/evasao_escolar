<?php
	include_once 'tsqlinstruction.class.php';


	final class TsqlInsert extends TsqlInstruction{
		private $value;
		private $columnValues; 

		public function setRowData(array $RowValues){
			foreach ($RowValues as $key => $value) {
				$this->columnValues[$key] = is_string($value)? "'$value'" : $value;
			}
		}

		public function getInstruction(){
			$this->sql = "INSERT INTO {$this->entity}  
			(".implode(', ', array_keys($this->columnValues)).') 
			values('.implode(', ', array_values($this->columnValues)).')';
			return $this->sql;
		}
	}


?>