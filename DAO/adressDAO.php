<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/adress.class.php';

class adressDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }
   public function create(adress $adress){
		try{
			$stmt = $this->con->prepare('INSERT INTO adress (city,neighborhood,street,number,complement) VALUES(:city,:neighborhood,:street,:number,:complement)');
			$response=$stmt->execute(array(
				':city' => $adress->getCity(),
				':neighborhood' => $adress->getNeighborhood(),
				':street' => $adress->getStreet(),
				':number' => $adress->getNumber(),
				':complement' => $adress->getComplement(),
			));
			$response=array();
			$response[0]=[
				'id_adress' => $this->con->lastInsertId(),
				'city' => $adress->getCity(),
				'neighborhood' => $adress->getNeighborhood(),
				'street' => $adress->getStreet(),
				'number' => $adress->getNumber(),
				'complement' => $adress->getComplement(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar endereço',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar endereço',
				'response' => $e->getMessage(),
			];
		}
		return $result;
   }

   public function read() {
		try{
			$stmt = $this->con->prepare('select * from adress');
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar endereço',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar endereço',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
  public function update(adress $adress){
		try{
			$stmt = $this->con->prepare('UPDATE adress SET city=:city , neighborhood=:neighborhood , street=:street , number=:number , complement=:complement  WHERE id_adress=:id_adress');
			$response=$stmt->execute(array(
				':id_adress' => $adress->getId_adress(),
				':city' => $adress->getCity(),
				':neighborhood' => $adress->getNeighborhood(),
				':street' => $adress->getStreet(),
				':number' => $adress->getNumber(),
				':complement' => $adress->getComplement(),
			));
			$current_user=$response[0];
			$_SESSION['city']=$adress->getCity();
			$_SESSION['neighborhood']=$adress->getNeighborhood();
			$_SESSION['street']=$adress->getStreet();
			$_SESSION['number']=$adress->getNumber();
			$_SESSION['complement']=$adress->getComplement();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao atualizar endereço',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar endereço',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function delete(adress $adress){
		try{
		 $stmt = $this->con->prepare('DELETE FROM adress WHERE id_adress=:id_adress');
		 $response=$stmt->execute(array(
			 ':id_adress' => $adress->getId_adress(),
		 ));
		 $result=[
			 'success' => true,
			 'message' => 'Sucesso ao deletar endereço',
			 'response' => $response,
		 ];
	 }catch(PDOException $e){
		 $result=[
			 'success' => false,
			 'message' => 'Falha ao deletar endereço',
			 'response' => $e->getMessage(),
		 ];
	 }
	 return $result;
 }
}
?>