<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root.'/DAO/connection.php';
include_once $root.'/Model/course.class.php';

class courseDAO{
   
   private $con;
   
   function __construct(){
       $this->con=Connection::conectar();
   }
   public function create(course $course){
		try{
			$stmt = $this->con->prepare('INSERT INTO course (name_course,duration,status_course) VALUES(:name_course,:duration,:status_course)');
			$stmt->execute(array(
				':name_course' => $course->getName_course(),
				':duration' => $course->getDuration(),
				':status_course' => $course->getStatus_course(),
			));
			$response=array();
			$response[0]=[
				'id_course' => $this->con->lastInsertId(),
				'status_course' => $course->getStatus_course(),
			];
			$result=[
				'success' => true,
				'message' => 'Sucesso ao criar curso',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao criar curso',
				'response' => $e->getMessage(),
			];
		}
		return $result;
   }

   public function read($criteria) {
		try{
			switch ($criteria) {
				case "all":
					$statement='select * from course';
					break;
				case "activated":
					$statement='select * from course where status_course=1';
					break;
			}
			$stmt = $this->con->prepare($statement);
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=[
				'success' => true,
				'message' => 'Sucesso ao listar curso',
				'response' => $response,
			];
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao listar curso',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}

  public function update(course $course){
		try{
			$stmt = $this->con->prepare('select distinct c.id_course from course c inner join (user u) where u.id_course=c.id_course && c.id_course='.$course->getId_course());
			$stmt->execute();
			$response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			$response = $stmt->fetchAll();
			$result=count($response)==0;

			$statement='UPDATE course SET name_course="'.$course->getName_course().'" , duration="'.$course->getDuration().'"'.
			($result?' , status_course="'.$course->getStatus_course().'"':'').
			' WHERE id_course="'.$course->getId_course().'"';

			if ($result){
				$stmt = $this->con->prepare($statement);
				$response=$stmt->execute();
				$result=[
					'success' => true,
					'message' => 'Sucesso ao atualizar curso',
					'response' => $response,
				];
			}else{
				$result=[
					'success' => false,
					'message' => 'Falha ao alterar status do curso',
					'response' => 'Este curso já tem pessoas inscritas',
				];
			}

			
		}catch(PDOException $e){
			$result=[
				'success' => false,
				'message' => 'Falha ao atualizar curso',
				'response' => $e->getMessage(),
			];
		}
		return $result;
	}
	public function delete(course $course){
		try{
		 $stmt = $this->con->prepare('DELETE FROM course WHERE id_course=:id_course');
		 $response=$stmt->execute(array(
			 ':id_course' => $course->getId_course(),
		 ));
		 $result=[
			 'success' => true,
			 'message' => 'Sucesso ao deletar curso',
			 'response' => $response,
		 ];
	 }catch(PDOException $e){
		 $result=[
			 'success' => false,
			 'message' => 'Falha ao deletar curso',
			 'response' => $e->getMessage(),
		 ];
	 }
	 return $result;
 }
}
?>