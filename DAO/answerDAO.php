<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include_once $root . '/DAO/connection.php';
include_once $root . '/Model/answer.class.php';

class answerDAO {
    private $con;

    function __construct() {
        $this->con = Connection::conectar();
    }
    public function create(answer $answer) {
        try {
            $stmt = $this->con->prepare('INSERT INTO answer (id_alternative) VALUES(:id_alternative)');
            $stmt->execute(array(
                ':id_alternative' => $answer->getId_alternative() ,
            ));
            $response = array();
            $id_answer = $this->con->lastInsertId();
            $stmt = $this->con->prepare('INSERT INTO user_has_answer (id_user,id_answer) VALUES(:id_user,:id_answer)');
            $stmt->execute(array(
                ':id_user' => $_SESSION['id_user'],
                ':id_answer' => $id_answer,
            ));
            $response[0] = [
				'id_answer' => $id_answer,
			];
			$result = [
				'success' => true,
				'message' => 'Sucesso ao criar resposta',
				'response' => $response,
			];
        }
        catch(PDOException $e) {
            $result = ['success' => false, 'message' => 'Falha ao criar resposta', 'response' => $e->getMessage() , ];
        }
        return $result;
    }

    public function read() {
        try {
            $stmt = $this->con->prepare('select * from answer');
            $stmt->execute();
            $response = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $response = $stmt->fetchAll();
            $result = [
				'success' => true, 
				'message' => 'Sucesso ao listar resposta', 
				'response' => $response, 
			];
        }
        catch(PDOException $e) {
            $result = [
				'success' => false, 
				'message' => 'Falha ao listar resposta', 
				'response' => $e->getMessage() , 
			];
        }
        return $result;
    }

    public function update(answer $answer) {
        try {
			$stmt = $this->con->prepare('UPDATE answer SET id_alternative=:id_alternative WHERE id_answer=:id_answer');
            $response = $stmt->execute(array(
                ':id_answer' => $answer->getId_answer() ,
                ':id_alternative' => $answer->getId_alternative() ,
            ));
            $result = [
				'success' => true, 
				'message' => 'Sucesso ao atualizar resposta', 
				'response' => 'Resposta atualizada', 
			];
        }
        catch(PDOException $e) {
            $result = [
				'success' => false, 
				'message' => 'Falha ao atualizar resposta', 
				'response' => $e->getMessage() , 
			];
        }
        return $result;
    }
    public function delete(answer $answer) {
        try {
            if ($_SESSION['user_type']<3){// deixa apenas usuarios com permisao de 2 pra baixo apagar respostas de outros usuarios
                $stmt = $this->con->prepare('SELECT id_user FROM user_has_answer WHERE id_answer=:id_answer');
                $stmt->execute(array(
                    ':id_answer' => $answer->getId_answer(),
                ));
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $id_user = $stmt->fetchAll()[0]['id_user'];
            }else{
                $id_user = $_SESSION['id_user'];
            }
			$stmt = $this->con->prepare('DELETE FROM user_has_answer WHERE id_user=:id_user && id_answer=:id_answer');
            $response = $stmt->execute(array(
                ':id_user' => $id_user,
                ':id_answer' => $answer->getId_answer(),
            ));
            $stmt = $this->con->prepare('DELETE FROM answer WHERE id_answer=:id_answer');
            $response = $stmt->execute(array(
                ':id_answer' => $answer->getId_answer(),
            ));
            $result = [
				'success' => true, 
				'message' => 'Sucesso ao deletar resposta', 
				'response' => $response, 
			];
        }
        catch(PDOException $e) {
            $result = [
				'success' => false, 
				'message' => 'Falha ao deletar resposta', 
				'response' => $e->getMessage() , 
			];
        }
        return $result;
    }
}

