<?php
class routes{
  const links=array(
    'student_links' => [
      'GET' => [
        '/' => 'userController@defaultView',
        '/formulario' => 'answerController@Formulario',
        '/logout' => 'userController@logout',
      ],
      'POST' => [
        '/answer' => 'answerController@create',
        
        '/ajax/adress' => 'adressController@read',
        '/ajax/contact' => 'contactController@read',
        '/ajax/question' => 'questionController@read',
        '/ajax/alternative' => 'alternativeController@read',
        '/ajax/course' => 'courseController@read',
      ],
      'PUT' => [
        '/user' => 'userController@update',
        '/answer' => 'answerController@update',
        '/contact' => 'contactController@update',
        '/adress' => 'adressController@update',
      ],
      'DELETE' => [],
    ],
    'assistant_links' => [
      'GET' => [
        '/teste' => 'userController@getResultFromWeka',// tirar 
        //------- views ---------------------------
        '/' => 'userController@defaultView',
        '/user' => 'userController@read',
        '/formulario' => 'answerController@Formulario',
        '/curso' => 'courseController@View',
        '/usuario' => 'userController@View',
        //------- deslogar ---------------------------
        '/logout' => 'userController@logout',
        //------- backup ---------------------------
        '/backup' => 'userController@backup',
      ],
      'POST' => [
        '/course' => 'courseController@create',
        '/question' => 'questionController@create',
        '/adress' => 'adressController@create',
        '/alternative' => 'alternativeController@create',
        '/user' => 'userController@create',
        '/contact' => 'contactController@create',

        '/ajax/question' => 'questionController@read',
        '/ajax/adress' => 'adressController@read',
        '/ajax/alternative' => 'alternativeController@read',
        '/ajax/user' => 'userController@read',
        '/ajax/contact' => 'contactController@read',
        '/ajax/course' => 'courseController@read',
        '/ajax/user_type' => 'user_typeController@read',
      ],
      'PUT' => [
        '/course' => 'courseController@update',
        '/question' => 'questionController@update',
        '/adress' => 'adressController@update',
        '/alternative' => 'alternativeController@update',
        '/user' => 'userController@update',
        '/contact' => 'contactController@update',

        '/course/status' => 'courseController@update',
        '/user/status' => 'userController@update',
      ],
      'DELETE' => [
        '/question' => 'questionController@delete',
        '/alternative' => 'alternativeController@delete',
        '/contact' => 'contactController@delete',
      ],
    ],
    'admin_links' => [
      'GET' => [
        //------- views ---------------------------
        '/' => 'userController@defaultView',
        '/user' => 'userController@read',
        '/formulario' => 'answerController@Formulario',
        '/curso' => 'courseController@View',
        '/usuario' => 'userController@View',
        '/weka' => 'userController@ViewWEKA',
        //------- deslogar ---------------------------
        '/logout' => 'userController@logout',
        //------- backup ---------------------------
        '/backup' => 'userController@backup',
      ],
      'POST' => [
        '/course' => 'courseController@create',
        '/question' => 'questionController@create',
        '/adress' => 'adressController@create',
        '/alternative' => 'alternativeController@create',
        '/user' => 'userController@create',
        '/contact' => 'contactController@create',

        '/ajax/question' => 'questionController@read',
        '/ajax/adress' => 'adressController@read',
        '/ajax/alternative' => 'alternativeController@read',
        '/ajax/user' => 'userController@read',
        '/ajax/contact' => 'contactController@read',
        '/ajax/course' => 'courseController@read',
        '/ajax/user_type' => 'user_typeController@read',

        '/weka' => 'userController@getResultFromWeka',
      ],
      'PUT' => [
        '/course' => 'courseController@update',
        '/question' => 'questionController@update',
        '/adress' => 'adressController@update',
        '/alternative' => 'alternativeController@update',
        '/user' => 'userController@update',
        '/contact' => 'contactController@update',

        '/course/status' => 'courseController@update',
        '/user/status' => 'userController@update',
      ],
      'DELETE' => [
        '/question' => 'questionController@delete',
        '/alternative' => 'alternativeController@delete',
        '/contact' => 'contactController@delete',
      ],
    ],
    'unlogged_links' => [
      'GET' => [
        '/' => 'userController@login',
        '/AtivarMinhaConta' => 'userController@activateAccount',
      ],
      'POST' => [
        '/auth' => 'userController@auth',
        '/recover' => 'userController@recover',
        '/user' => 'userController@create',
        '/adress' => 'adressController@create',

        '/ajax/course' => 'courseController@read',
      ],
      'PUT' => [
        '/recover' => 'userController@verificateCode',
        '/user' => 'userController@updateRecover',
      ],
      'DELETE' => [],
    ],
  );
  public static function links($user){
    return SELF::links[
      [
        $_ENV['ADMIN_LINKS']     => 'admin_links',
        $_ENV['ASSISTANT_LINKS'] => 'assistant_links',
        $_ENV['STUDENTS_LINKS']  => 'student_links',
        $_ENV['UNLOGGED_LINKS']  => 'unlogged_links',
      ][$user]
    ];
  }
}

new routes();