<?php
class templates{
  public static function get_Modals(){
    return array(
      'user-modal' => [
            'modal'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/modal.php',
            'id'=>'user-modal',
            'title'=>'Dados pessoais',
            'header'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/header/header.php',
            'body'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/body/user.php',
            'footer'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/footer/save-close.php',
        ],
      'object-modal' => [
            'modal'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/modal.php',
            'id'=>'object-modal',
            'title'=>'Formulário do usuário',
            'header'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/header/header.php',
            'body'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/body/edit.php',
            'footer'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/footer/save-close.php',
        ],
      'recover-modal' => [
          'modal'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/modal.php',
          'id'=>'recover-modal',
          'title'=>'Recuperação de conta',
          'header'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/header/header.php',
          'body'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/body/recover.php',
          'footer'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/footer/recover.php',
      ],
      'more-modal' =>  [
            'modal'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/modal.php',
            'id'=>'more-modal',
            'title'=>'Informações adicionais',
            'header'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/header/header.php',
            'body'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/body/more.php',
            'footer'=>$_SERVER['DOCUMENT_ROOT'].'/View/template/modal/footer/close-only.php',
        ],
      );
  }
  public static function get_UserSession(){
    include_once $_SERVER['DOCUMENT_ROOT']."/Controller/userController.php";
    $userController=new userController;
    $userController->contents['request']=['print'=>false,'criteria'=>'search'];
    $list=$userController->read();
    $list['response'][0]['profile_image']=$list['response'][0]['profile_image_type'].$list['response'][0]['profile_image']; 
    return $list['response'][0];
  }
}

new templates();
