<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/user.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/userDAO.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/user_typeController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/templates.php';


final class userController extends utilController{

    function __construct(){
        $this->VAR_ATTR=user::VAR_ATTR;
        $this->modal=templates::get_Modals();
        $json=json_decode(file_get_contents("php://input"), true);
        $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
        foreach ($this->contents as $key => $value)
            if (gettype($value)==='string')
                $this->contents[$key]=trim($value);
    }

    public function create(){
        $DAO = new userDAO;
        $message='criar usuário';
        $attrs=[
            'name_user',
            'id_course',
            'email',
            'password',
            'entry_year',
            'confirm_password',
            'birth_date',
            'profile_image',
            'profile_image_type'
        ];
        $array=['id_course'=>$this->VAR_ATTR['id_course']];
        if($_SESSION['user_type']!=$_ENV['UNLOGGED_LINKS'])
            array_push($attrs,'egress_year');
            
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->create($this->setObj());
        $this->print_response($result);
    }

    public function read(){
        $DAO = new userDAO;
        $result=$DAO->read($this->contents['request']['criteria']);
        switch ($this->contents['request']['print']) {
            case true:
                $this->print_response($result);
                break;
            case false:
                return $result;
                break;
            default:
                return $result=[
                    'success' => false,
                    'message' => 'Falha ao obter usuário',
                    'response' => 'Impressão indefinida',
                ];
        }
    }
    
    public function update(){
        $DAO = new userDAO;
        $message='atualizar usuário';
        // var_dump(isset($this->contents['password'])||isset($this->contents['confirm_password']),isset($this->contents['password']),isset($this->contents['confirm_password']));
        if(($_SERVER['REQUEST_URI']=='/user' || $_SERVER['REQUEST_URI']=='/user/status') && $_SERVER['REQUEST_METHOD']=='PUT'){
            $attrs=[
                isset($this->contents['name_user'])?'name_user':'',
                isset($this->contents['id_course'])?'id_course':'',
                isset($this->contents['id_user_type'])?'id_user_type':'',
                isset($this->contents['email'])?'email':'',
                isset($this->contents['password'])?'password':'',
                isset($this->contents['password'])||isset($this->contents['confirm_password'])?'confirm_password':'',
                isset($this->contents['entry_year'])?'entry_year':'',
                isset($this->contents['egress_year'])?'egress_year':'',
                isset($this->contents['birth_date'])?'birth_date':'',
                isset($this->contents['status_user'])?'status_user':'',
                // isset($this->contents['profile_image'])?'profile_image':'',
                // isset($this->contents['profile_image'])||isset($this->contents['profile_image_type'])?'profile_image_type':'',
            ];
        }
        $ATTRS=$this->getCastAttr($attrs);
        $validation=$this->validation($message,$ATTRS);
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->update($this->setObj());
        $this->print_response($result);
    }

    public function updateRecover(){
        $DAO = new userDAO;
        $message='recuperar usuário';
        $validation=$this->validation($message,$this->getCastAttr(['email','verification_code','password','confirm_password']));
        $result=$validation;
        if ($validation['success']==true){
            $user=$this->setObj();
            $result=$DAO->verificateCode($user);
            if ($result['success']==true){
                $this->contents['request']=['print'=>false,'criteria'=>'search_by_verification_code'];
                $result=$DAO->read('search_by_verification_code',$this->setObj());
                if ($result['success']==true){
                    $user->setId_user($result['response'][0]['id_user']);
                    $user->setVerification_code(0);
                    $result=$DAO->update($user);
                }
            }
        }
        $this->print_response($result);
    }

    public function delete(){
        $DAO = new userDAO;
        $result=$DAO->delete($this->setObj());
        $this->print_response($result);
    }

    public function auth(){
        $DAO = new userDAO;
        $message='logar do usuário';
        $validation=$this->validation($message,$this->getCastAttr(['email','password']));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->login($this->setObj());
        $this->print_response($result);
    }

    public function logout(){
        logController::Log([
            'success' => true,
            'message' => 'Sucesso ao deslogar usuário',
            'response' => ['Id do usuário'=>$_SESSION['id_user']],
        ],'access');
        session_unset();
        session_destroy();
        header('location:/');
    }

    protected function setObj(){
        $user = new user;
        $user -> setId_user(isset($this->contents["id_user"]) ? ($this->contents["id_user"]):null);
        $user -> setBirth_date(isset($this->contents["birth_date"]) ? ($this->contents["birth_date"]):null);
        $user -> setEntry_year(isset($this->contents["entry_year"]) ? ($this->contents["entry_year"]):null);
        $user -> setEgress_year(isset($this->contents["egress_year"]) ? ($this->contents["egress_year"]):null);
        $user -> setName_user(isset($this->contents["name_user"]) ? ($this->contents["name_user"]):null);
        $user -> setEmail(isset($this->contents["email"]) ? ($this->contents["email"]):null);
        $user -> setPassword(isset($this->contents["password"]) ? ($this->contents["password"]):null);
        $user -> setId_user_type(isset($this->contents["id_user_type"]) ? ($this->contents["id_user_type"]):null);
        $user -> setId_adress(isset($this->contents["id_adress"]) ? ($this->contents["id_adress"]):null);
        $user -> setId_course(isset($this->contents["id_course"]) ? ($this->contents["id_course"]):null);
        $user -> setProfile_image(isset($this->contents["profile_image"]) ? ($this->contents["profile_image"]):null);
        $user -> setProfile_image_type(isset($this->contents["profile_image_type"]) ? ($this->contents["profile_image_type"]):null);
        $user -> setStatus_user(isset($this->contents["status_user"])? ($this->contents["status_user"]):1);
        $user -> setVerification_code(isset($this->contents["verification_code"])? ($this->contents["verification_code"]):null);
        return $user;
    }

    public function View(){
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/adressController.php";
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/contactController.php";
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/courseController.php";
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/user_typeController.php";
        $adressController=new adressController;
        $contactController=new contactController;
        $courseController=new courseController;
        $user_typeController=new user_typeController;
        $this->contents['request']=['print'=>false,'criteria'=>($_SESSION['user_type']<3?'all':'search')];
        $title="Cadastro usuário";
        $tableTitle='Lista dos usuários';
        $object_name="user";
        $list=$this->read();
        $this->contents['request']=['print'=>false,'criteria'=>'search'];
        $UserSession=templates::get_UserSession();
        $courseController->VAR_ATTR['duration']['hidden']=true;
        $courseController->VAR_ATTR['status_course']['hidden']=true;
        $list['format']=array_merge($this->VAR_ATTR,$user_typeController->VAR_ATTR,$courseController->VAR_ATTR,$contactController->VAR_ATTR,$adressController->VAR_ATTR);
        $jScript='<script src="/js/table.js"></script>
        <script>var jsonList='.json_encode($list).';var listUserType='.json_encode($user_typeController->read()).';
        $(document).ready(function () {list(jsonList,`user`,listUserType);});</script>
        <script>var modal_user='.json_encode($_SESSION).'</script>';
        $modal=[$this->modal['user-modal'],$this->modal['object-modal'],$this->modal['more-modal']];
        $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
        $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
        
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/body.php';
        $content=$_SERVER['DOCUMENT_ROOT'].'/View/template/table.php';
        $QuestionMarkImg='/images/help/user.jpg';
        
        include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
    }

    public function defaultView(){
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/courseController.php";
        include_once $_SERVER['DOCUMENT_ROOT']."/Controller/user_typeController.php";
        $courseController=new courseController;
        $user_typeController=new user_typeController;
        $title="Bem vindo";
        $object_name="user";
        $list=$this->read();
        $this->contents['request']=['print'=>false,'criteria'=>'search'];
        $UserSession=templates::get_UserSession();
        $courseController->VAR_ATTR['duration']['hidden']=true;
        $courseController->VAR_ATTR['status_course']['hidden']=true;
        $list['format']=array_merge($this->VAR_ATTR,$courseController->VAR_ATTR,$user_typeController->VAR_ATTR);
        $jScript='<script src="/js/table.js"></script>
        <script>var jsonList='.json_encode($list).'; $(document).ready(function () {list(jsonList,`user`);});</script>
        <script>var modal_user='.json_encode($_SESSION).'</script>';
        $modal=$this->modal;
        $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
        $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
        $QuestionMarkImg='/images/help/default-view'.($_SESSION['user_type']==1?'-admin':'').'.jpg';
        
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/body.php';  
        
        include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
    }

    public function login(){
        $jScript='<script src="/js/login.js"></script><script src="/js/register.js"></script>';
        $title='login';
        $object_name="user";
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/login.php';
        $modal=[$this->modal['object-modal'],$this->modal['recover-modal']];
        $QuestionMarkImg='/images/help/login.jpg';
        include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
    }

    function createArff(){
        include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/questionController.php';
        include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/alternativeController.php';
        $qController=new questionController;
        $alController=new alternativeController;

        $qController->contents['request']=['criteria'=>'answered','print'=>false];
        $qList=$qController->read();

        $alController->contents['request']=['criteria'=>'answered','print'=>false];
        $alList=$alController->read();

        $alController->contents['request']=['criteria'=>'arff','print'=>false];
        $alArffList=$alController->read();

        if ($qList['response'] && $alList['response'] && $alArffList['response']){
            foreach ($qList['response'] as $key => $value) {
                // $array_text['question'][$value['id_question']]["statement"]=$value['id_question'].'_id_'.str_replace([',',' '],['&#44;','&nbsp;'], $value['statement']);
                $array_text['list'][$value['id_question']]["question"]=str_replace([',',' '],['&#44;','&nbsp;'], $value['statement']);
                $array_text['list'][$value['id_question']]["alternative"]=[];
            }
            
            foreach ($alList['response'] as $key => $value) {
                $array_text['list'][$value['id_question']]["alternative"][$value['id_alternative']]=$value['weight'];
            }
            
            $questionQtdcount=count($array_text['list']);
            foreach ($alArffList['response'] as $key => $value) {
                $array_text['user'][$value['id_user']][$value['id_question']]=['id_alternative'=>$value['id_alternative'],'weight'=>$value['weight']];
            }

            $conteudo="@relation users\n";
            foreach ($array_text['list'] as $key => $value) {
                $array=[];
                foreach ($value["alternative"] as $key_1 => $value_1)
                    array_push($array,$key_1."_id_".$value_1);
                $conteudo.="@attribute ".$key."_id_".$value["question"]." {".join(',',$array)."}\n";
            }
            $conteudo.="@data\n";
            
            $array_text['total_users']=count($array_text['user']);
            foreach ($array_text['user'] as $key => $value) {
                if (count($value)==$questionQtdcount){
                    $array=[];
                    foreach ($value as $key_1 => $value_1)
                        array_push($array,$value_1['id_alternative']."_id_".$value_1['weight']);
                    $conteudo.=join(',',$array)."\n";
                }else
                    unset($array_text['user'][$key]);
            }
            $array_text['answered_All_question_users']=count($array_text['user']);

            $file=$_SERVER['DOCUMENT_ROOT'].'/Controller/weka/users.arff';
            if (is_file($file))
                unlink($file);
            $f = fopen($file,'w');
            if ($f){
                fwrite($f, $conteudo); 
                fclose($f);
                chmod($file, 0777);
                return[
                    'success' => true,
                    'message' => 'Sucesso ao obter resultado do weka',
                    'response' => $array_text,
                ];
            }else{
                return [
                    'success' => false,
                    'message' => 'Falha ao criar ARFF',
                    'response' => 'Não foi possível gravar no disco',
                ];
            }
        }else{
            return [
                'success' => false,
                'message' => 'Falha ao criar ARFF',
                'response' => 'Consulta não obteve resultado esperado',
            ];
        }
    }

    function clean_str($regex,$input){
        $output=[];
        preg_match_all($regex, $input, $output);
        return count($output)<2?$output[0]:$output;
    }

    function separate_values($output){
        foreach ($output as $key => $value){
            $explode=explode('=',trim($value));
            $output[$key]=["question"=>$explode[0],'alternative'=>$explode[1]];
        }
    }

    public function getResultFromWeka(){
        $VAR_ATTR=[
            'lowerBoundMinSupport' => [
              'name' => 'suporte minimo',
              'type' => 'NUMERIC',
              'min' => 0,
              'max' => 1,
              'null' => false,
              'hidden' => false,
              'step'=>0.01,
            ],
            'upperBoundMinSupport' => [
                'name' => 'suporte minimo',
                'type' => 'NUMERIC',
                'min' => 0,
                'max' => 1,
                'null' => false,
                'hidden' => false,
                'step'=>0.01,
            ],
            'numRules' => [
                'name' => 'numero de regras',
                'type' => 'NUMERIC',
                'min' => 1,
                'max' => 9999,
                'null' => false,
                'hidden' => false,
            ],
            'metricType' => [
                'name' => 'tipo de metrica',
                'type' => 'STRING',
                'min' => 1,
                'max' => 45,
                'null' => false,
                'hidden' => false,
            ],
            'minMetric' => [
                'name' => 'metrica minima',
                'type' => 'NUMERIC',
                'min' => 0,
                'max' => 1,
                'null' => false,
                'hidden' => false,
                'step'=>0.01,
            ]
          ];
        $message='inserir parametros';
        $attrs=[
            'lowerBoundMinSupport',
            'upperBoundMinSupport',
            'numRules',
            'metricType',
            'minMetric',
        ];
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        $result=$this->createArff();
        if ($result['success']==true){
            $command='java -jar '.$_SERVER['DOCUMENT_ROOT'].'/Controller/weka/weka.jar -t '.$_SERVER['DOCUMENT_ROOT'].
            '/Controller/weka/users.arff -lowerBoundMinSupport '.$this->contents['lowerBoundMinSupport'].' -upperBoundMinSupport '.
            $this->contents['upperBoundMinSupport'].' -numRules '.$this->contents['numRules'].' -metricType '.$this->contents['metricType'].
            ' -minMetric '.$this->contents['minMetric'];
            $input_line = str_replace(['<','>'],['&lt;','&gt;'],shell_exec($command));
            unlink($_SERVER['DOCUMENT_ROOT'].'/Controller/weka/users.arff');

            $array_line=[
                'command'=>$command,
                'raw_input'=>$input_line,
                'support'=>[],
                '[conf|lift|lev|conv]'=>[],
                'QuestionAndAnswerBeforeArrow'=>[],
                'QuestionAndAnswerAfterArrow'=>[],
                'line'=>[],
                'BeforeArrow'=>[],
            ];
            // pega todas as linhas
            foreach ($this->clean_str('/\ ?\d*\.\ .*?\\n/',$input_line) as $key => $value){
                array_push($array_line['line'],$value);
                // pega o supporte na pos[1]
                array_push($array_line['support'],array_map('trim', $this->clean_str('/\S(\ \d*\ )/',$value)[1]));
                // pega conf|lift|lev|conv da linha
                $array_line['[conf|lift|lev|conv]'][$key]=[];
                foreach ($this->clean_str('/(conf|lift|lev|conv):\(.*?\)/',$value)[0] as $key_1 => $value_1){
                    $explode=explode(':',$value_1);
                    $array_line['[conf|lift|lev|conv]'][$key][$explode[0]]=str_replace(['(',')'],[''],$explode[1]);
                }
                array_push($array_line['BeforeArrow'],$this->clean_str('/.*\=\=\&gt\;/',$value));
                foreach ([
                    // Linha antes da seta && pega de uma linha especifica todas suas perguntas usadas
                    $this->clean_str('/\ \S*?\=\d*_id_\d*/', $this->clean_str('/.*\=\=\&gt\;/',$value)[0]),
                    // Linha depois da seta && pega de uma linha especifica todas suas perguntas usadas
                    $this->clean_str('/\ \S*?\=\d*_id_\d*/', $this->clean_str('/\=\=\&gt\;.*/',$value)[0]),
                ] as $key_1 => $value_1){
                    foreach ($value_1 as $key_2 => $value_2){
                        // separando questao e alternativa [0]=>questao,[1]=>alternativa,
                        $explode=explode('=',trim($value_2));
                        if ($explode[0]!=''){
                            $explode_question=explode('_id_',trim($explode[0]));// separando id
                            $explode_alternative=explode('_id_',trim($explode[1]));
                            $array_line[['QuestionAndAnswerBeforeArrow','QuestionAndAnswerAfterArrow'][$key_1]][$key][$key_2]=["id_question"=>$explode_question[0],"id_alternative"=>$explode_alternative[0],"question"=>$explode_question[1],"alternative"=>$explode_alternative[1]];
                        }
                    }
                }
            }

            $result=[
                'success' => true,
                'message' => 'Sucesso ao obter resultado do weka',
                'response' => [
                    'result'=>$array_line,
                    'information'=>$result['response']
                ],
            ];
        }else{
            $result=[
                'success' => false,
                'message' => 'Falha ao obter resultado do weka',
                'response' => 'Arff não foi encontrado',
            ];
        }
        
        $this->print_response($result);
    }

    function ViewWEKA(){
        $title='Resultado Weka';
        $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
        $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
        $UserSession=templates::get_UserSession();
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/body.php';
        $content=$_SERVER['DOCUMENT_ROOT'].'/View/template/weka.php';

        $sessao=$_SESSION;
        unset($sessao['user_type']);
        $jScript='<script src="/js/weka.js"></script>
        <script>var modal_user='.json_encode($sessao).'</script>';
        $modal=[$this->modal['user-modal'],$this->modal['more-modal']];
        $QuestionMarkImg='/images/help/weka.jpg';
        include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
    }

    public function recover(){
        $DAO = new userDAO;
        $message='recuperar usuário';
        $validation=$this->validation($message,$this->getCastAttr(['email']));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->recover($this->setObj());
        $this->print_response($result);
    }

    public function activateAccount(){
        $DAO = new userDAO;
        $user=$this->setObj();
        $result=$DAO->verificateCode($user);
        if ($result['success']==true){
            $this->contents['request']=['print'=>false,'criteria'=>'search_by_verification_code'];
            $result=$DAO->read('search_by_verification_code',$this->setObj());
            if ($result['success']==true){
                $user->setId_user($result['response'][0]['id_user']);
                $user->setVerification_code(0);
                $user->setEmail_status(1);
                $user->setStatus_user(1);
                $result=$DAO->update($user);
            }
        }
        if ($result['success']==true)
            $result['response']='Conta Ativada com sucesso!';
        else
            $result['response']='Url inválida';
        $jScript='<script src="/js/login.js"></script><script src="/js/register.js"></script>
        <script>
            showMessage("#login-form #message",JSON.parse(`'.json_encode($result).'`),`'.($result['success']==true?'success':'danger').'`);
        </script>';
        $title='login';
        $object_name="user";
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/login.php';
        $modal=[$this->modal['object-modal'],$this->modal['recover-modal']];
        include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
    }

    public function verificateCode(){
        $DAO = new userDAO;
        $message='recuperar usuário';
        $validation=$this->validation($message,$this->getCastAttr(['email']));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->verificateCode($this->setObj());
        $this->print_response($result);
    }
}
new userController();