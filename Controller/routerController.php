<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/config/routes.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

class routerController{
  public static function router(){
      if (empty($_SESSION))
        include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/sessionController.php'; 
      if (empty($_SESSION['lastAccess']) && isset ($_SESSION['email']))
        $_SESSION['lastAccess'] = time();

      $redirect_to='';
      if(!isset ($_SESSION['email'])){
        $_SESSION['user_type']='4';
        $request='/';
        $method='GET';
      }
      
      if (!(time() - (isset($_SESSION['lastAccess'])?$_SESSION['lastAccess']:NULL) > $_ENV['COOKIE_LIFETIME']) || $_SESSION['user_type']=='4') {
        $_SESSION['lastAccess'] = time();
        $user_type=$_SESSION['user_type'];
        $request=isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']:null;
        $request=explode("?", $request)[0];
        $method=$_SERVER['REQUEST_METHOD'];
      }else{
        if(isset ($_SESSION['email'])){
          $user_type=$_SESSION['user_type'];
          $request='/logout';
          $method='GET';
        }
      }

      $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
      $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
      $links=routes::links($user_type);
      $string=null;
      
      if (isset($links) && (array_key_exists($method,$links)) && (array_key_exists($request,$links[$method]))){
        $tmp_val=explode("@", $links[$method][$request]);
        $redirect_to='/Controller/'.$tmp_val[0].'.php';
        $string='$Controller = (new '.$tmp_val[0].')->'.$tmp_val[1].'();';
      }

      $Controller=$_SERVER['DOCUMENT_ROOT'].$redirect_to;
      if (is_file($Controller)){
        include_once $_SERVER['DOCUMENT_ROOT'].$redirect_to;
        eval($string);
      }else{
        $title='Pagina não encontrada';
        $redirect_to='/View/page/dashboard.php';
        $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/404.php';
        $logo_href="/formulario";
        include_once $_SERVER['DOCUMENT_ROOT'].$redirect_to;
      }
  }

}

new routerController();