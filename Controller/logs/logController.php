<?php
class logController{
    public static function Log($data,$type){
        date_default_timezone_set('America/Sao_Paulo');
        $dateTime=new DateTime('now');
        $timestamp=$dateTime->format('c');
        $today=$dateTime->format('Y-m-d');
        $data['response']=array_merge(['Timestamp'=>$timestamp],(array)$data['response']);
        $file_path=$_SERVER['DOCUMENT_ROOT'].'/Controller/logs/log_'.$today.'.json';
        $file=[];
        if (is_file($file_path))
            $file=json_decode(file_get_contents($file_path),true);
        if (empty($file[$type]))
            $file[$type]=[];
        array_push($file[$type],$data);
        $file=json_encode($file,JSON_PRETTY_PRINT);
        file_put_contents($file_path, $file);
    }
}

new logController();
?>