<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/alternative.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/alternativeDAO.php'; 

final class alternativeController extends utilController{

    function __construct(){
        $this->VAR_ATTR=alternative::VAR_ATTR;
        $json=json_decode(file_get_contents("php://input"), true);
        $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
        foreach ($this->contents as $key => $value)
            if (gettype($value)==='string')
                $this->contents[$key]=trim($value);
    }

   public function create(){
        $DAO = new alternativeDAO;
        $message='criar opção';
        $attrs=[
            'id_question',
            'weight',
        ];
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->create($this->setObj());
        $this->print_response($result);
    }
    
    public function read(){
        $DAO = new alternativeDAO;
        $result=$DAO->read($this->contents['request']['criteria']);
        switch ($this->contents['request']['print']) {
            case true:
                $this->print_response($result);
                break;
            case false:
                return $result;
                break;
            default:
                return $result=[
                    'success' => false,
                    'message' => 'Falha ao obter usuário',
                    'response' => 'Impressão indefinida',
                ];
        }
    }

    public function update(){
        $DAO = new alternativeDAO;
        $message='atualizar opção';
        $validation=$this->validation($message);
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->update($this->setObj());
        $this->print_response($result);
    }

    public function delete(){
        $DAO = new alternativeDAO;
        $message='criar opção';
        $attrs=[
            'id_alternative',
        ];
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->delete($this->setObj());
        $this->print_response($result);
    }

    protected function setObj(){
        $alternative = new alternative;
        $alternative -> setId_alternative(isset($this->contents["id_alternative"]) ? ($this->contents["id_alternative"]):null);
        $alternative -> setWeight(isset($this->contents["weight"]) ? ($this->contents["weight"]):null);
        $alternative -> setId_question(isset($this->contents["id_question"]) ? ($this->contents["id_question"]):null);
        $alternative -> setStatus(isset($this->contents["status"]) ? ($this->contents["status"]):1);
        return $alternative;
    }
    
}
new alternativeController();