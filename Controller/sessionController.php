<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/logs/logController.php';

class sessionController{// extends SessionHandler
    private $savePath;

    function __construct(){}

    function open($savePath, $sessionName){
        $this->savePath = $savePath;
        if (!is_dir($this->savePath)) {
            mkdir($this->savePath, 0777);
        }
        return true;
    }

    function close(){
        return true;
    }

    function read($id){
        return is_file("$this->savePath/sess_$id")?(string)@file_get_contents("$this->savePath/sess_$id"):'';
    }

    function write($id, $data){
        return file_put_contents("$this->savePath/sess_$id", $data) === false ? false : true;
    }

    function destroy($id){
        $file = "$this->savePath/sess_$id";
        if (file_exists($file)) {
            unlink($file);
        }
        return true;
    }

    function gc($maxlifetime){
        foreach (glob("$this->savePath/sess_*") as $file) {
            if (filemtime($file) + $maxlifetime < time() && file_exists($file)) {
                unlink($file);
            }
        }
        return true;
    }
}


@ini_set('session.use_trans_sid', 0);
@ini_set('session.gc_probability', 1); // in debian this is disabled by default...
$LIFETIME=time() + (10 * 365 * 24 * 60 * 60);// 10 years
session_set_cookie_params($LIFETIME);
$handler = new sessionController();
// enable customized session handler functions
session_set_save_handler(
    array($handler, 'open'),
    array($handler, 'close'),
    array($handler, 'read'),
    array($handler, 'write'),
    array($handler, 'destroy'),
    array($handler, 'gc')
);
// a linha a seguir evita comportamentos não esperados ao usar objetos como manipuladores de gravação
register_shutdown_function('session_write_close');
session_start();
setcookie(session_name(),session_id(),$LIFETIME);

new sessionController();
?>