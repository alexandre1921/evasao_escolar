<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/adress.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/adressDAO.php'; 

final class adressController extends utilController{

    function __construct(){
        $this->VAR_ATTR=adress::VAR_ATTR;
        $json=json_decode(file_get_contents("php://input"), true);
        $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
        foreach ($this->contents as $key => $value)
            if (gettype($value)==='string')
                $this->contents[$key]=trim($value);
    }

   public function create(){
        $DAO = new adressDAO;
        $message='criar endereço';
        $attrs=[
            'city',
            'neighborhood',
            'street',
            'number',
            'complement',
        ];
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->create($this->setObj());
        switch ($this->contents['request']['print']) {
            case true:
                $this->print_response($result);
                break;
            case false:
                return $result;
                break;
            default:
                return $result=[
                    'success' => false,
                    'message' => 'Falha ao obter usuário',
                    'response' => 'Impressão indefinida',
                ];
        }
        return $result;
    }

    public function read(){
        $DAO = new adressDAO;
        $result=$DAO->read();
        switch ($this->contents['request']['print']) {
            case true:
                $this->print_response($result);
                break;
            case false:
                return $result;
                break;
            default:
                return $result=[
                    'success' => false,
                    'message' => 'Falha ao obter usuário',
                    'response' => 'Impressão indefinida',
                ];
        }
    }

    public function update(){
        $DAO = new adressDAO;
        $message='atualizar endereço';
        $attrs=[
            'id_adress',
            'city',
            'neighborhood',
            'street',
            'number',
            'complement'
          ];
        $validation=$this->validation($message,$this->getCastAttr($attrs));
        $result=$validation;
        if ($validation['success']==true)
            $result=$DAO->update($this->setObj());
        $this->print_response($result);
    }

    public function delete(){
        $DAO = new adressDAO;
        $result=$DAO->delete($this->setObj());
        $this->print_response($result);
    }

    protected function setObj(){
        $adress = new adress;
        $adress -> setId_adress(isset($this->contents["id_adress"]) ? ($this->contents["id_adress"]):null);
        $adress -> setCity(isset($this->contents["city"]) ? ($this->contents["city"]):null);
        $adress -> setNeighborhood(isset($this->contents["neighborhood"]) ? ($this->contents["neighborhood"]):null);
        $adress -> setStreet(isset($this->contents["street"]) ? ($this->contents["street"]):null);
        $adress -> setNumber(isset($this->contents["number"]) ? ($this->contents["number"]):null);
        $adress -> setComplement(isset($this->contents["complement"]) ? ($this->contents["complement"]):null);
        $adress -> setStatus(isset($this->contents["status"]) ? ($this->contents["status"]):1);
        return $adress;

    }

}
new adressController();