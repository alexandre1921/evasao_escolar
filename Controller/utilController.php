<?php
abstract class utilController{

  protected function validation(string $message, array $attrVal=[]){
    $error=array();
    $bool=array();
    $attrVal=$attrVal!=[]?$attrVal:$this->VAR_ATTR;
    
    foreach ($attrVal as $key => $value){
      $value_contents=$this->contents[$key];
      
      // if (gettype($this->contents[$key])==='string')
      //   $this->contents[$key]=htmlspecialchars($this->contents[$key]);
      if (isset($attrVal[$key])){
        $error[$key]=[];
        $error[$key]["keep_going"]=true;
        if (!$attrVal[$key]["null"])
          if (strval($value_contents)!=='0')
            if (empty($value_contents)){
              $error[$key]["message"]="O campo <b>".$attrVal[$key]["name"]."</b> não pode ser enviado vazio";
              $error[$key]["keep_going"]=false;
            }
        
        if ($attrVal[$key]["type"]=="STRING"){//validacao string muito longa
          $var_tam=strlen($value_contents);
          if($var_tam<$attrVal[$key]["min"] || $var_tam>$attrVal[$key]["max"]){
            $error[$key]["message"]="O campo <b>".$attrVal[$key]["name"]."</b> não pode ser preenchido com menos do que <b>".$attrVal[$key]["min"]."</b> e mais do que <b>".$attrVal[$key]["max"]."</b> caracteres";
            $error[$key]["keep_going"]=false;
          }
        }

        if ($attrVal[$key]["type"]=="NUMERIC")//validacao valor numerico muito alto
          if($value_contents<$attrVal[$key]["min"] || $value_contents>$attrVal[$key]["max"]){
            $error[$key]["message"]="O valor do campo <b>".$attrVal[$key]["name"]."</b> deve estar entre <b>".$attrVal[$key]["min"]."</b> e <b>".$attrVal[$key]["max"].'</b>';
            $error[$key]["keep_going"]=false;
          }
        
        if (isset($attrVal[$key]["pattern"])&&!$attrVal[$key]["null"]){//validacao pattern regex
          if(!filter_var($value_contents,FILTER_VALIDATE_REGEXP,["options" => ["regexp"=>'/'.$attrVal[$key]["pattern"].'/']])){
            $error[$key]["message"]="O valor do campo <b>".$attrVal[$key]["name"]."</b> deve estar de acordo com o seguinte regex <b>".$attrVal[$key]["pattern"].'</b>';
            $error[$key]["keep_going"]=false;
          }
        }

        if (isset($attrVal[$key]["same"]))//validacao de campos de confirmacao como senha
          if ($this->contents[$attrVal[$key]["same"]["attr"]]!==$value_contents){
            $error[$key]["message"]="O valor do campo <b>".$attrVal[$key]["name"]."</b> é diferente do campo <b>".$attrVal[$attrVal[$key]["same"]["attr"]]["name"].'</b>';
            $error[$key]["keep_going"]=false;
          }

        $bool[$key]=$error[$key]["keep_going"];
      }
    }

    $boole=true;
    foreach ($bool as $key => $value){
      if (!$value){
        unset($error[$key]["keep_going"]);
        $boole=false;
      }else
        unset($error[$key]);
    }
    if (!$boole)
      $result=[
        'success' => false,
        'message' => 'Falha ao '.$message,
        'response' => $error,
      ];
    else
      $result=[
        'success' => true,
        'message' => 'Sucesso ao validar',
        'response' => 'Tudo certo',
      ];
      
    return $result;
   }

   public function print_response($result){
    header('Content-type: application/json');
    echo json_encode($result);
   }

   public function getCastAttr(array $flip){
    return array_intersect_key($this->VAR_ATTR,array_flip($flip));
   }

    // public function treatHtml($result){
    //     $type=gettype($result);
    //     if ($type==='object'||$type==='array'){
    //         foreach ($result as $key => $value) {
    //             $type=gettype($value);
    //             if ($type==='object'||$type==='array'){
    //                 $result[$key]=$this->treatHtml($value);
    //             }else{
		// 			if ($type==='string')
		// 				$result[$key]=htmlentities($value);
    //             }
    //         }
    //     }else{
    //         if ($type==='string')
    //             $result=htmlentities($result);
    //     }
    //     return $result;
    // }

   abstract public function create();
   abstract public function read();
   abstract public function update();
   abstract public function delete();
   abstract protected function setObj();
}