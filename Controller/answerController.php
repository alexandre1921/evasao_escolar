<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/answer.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/answerDAO.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/templates.php';

final class answerController extends utilController{

  function __construct(){
      $this->VAR_ATTR=answer::VAR_ATTR;
      $this->modal=templates::get_Modals();
      $json=json_decode(file_get_contents("php://input"), true);
      $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
      foreach ($this->contents as $key => $value)
        if (gettype($value)==='string')
            $this->contents[$key]=trim($value);
  }

  public function create(){
      $DAO = new answerDAO;
      $message='criar resposta';
      $attrs=[
        'id_alternative',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->create($this->setObj());
      $this->print_response($result);
  }

  public function read(){
      $DAO = new answerDAO;
      $result=$DAO->read();
      switch ($this->contents['request']['print']) {
        case true:
            $this->print_response($result);
            break;
        case false:
            return $result;
            break;
        default:
            return $result=[
                'success' => false,
                'message' => 'Falha ao obter resposta',
                'response' => 'Impressão indefinida',
            ];
      }
  }

  public function update(){
      $DAO = new answerDAO;
      $message='atualizar resposta';
      $attrs=[
        'id_alternative',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->update($this->setObj());
      $this->print_response($result);
  }

  public function delete(){
      $DAO = new answerDAO;
      $result=$DAO->delete($this->setObj());
      $this->print_response($result);
  }

  protected function setObj(){
      $answer = new answer;
      $answer -> setId_answer(isset($this->contents["id_answer"]) ? ($this->contents["id_answer"]):null);
      $answer -> setId_alternative(isset($this->contents["id_alternative"]) ? ($this->contents["id_alternative"]):null);
      $answer -> setStatus(isset($this->contents["status"]) ? ($this->contents["status"]):1);
      return $answer;
  }

  public function ViewFORM(){
    $title="Cadastro resposta";
    $object_name="answer";
    include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/viewFORM.php';
  }

  public function Formulario(){
    $UserSession=templates::get_UserSession();

    include_once $_SERVER['DOCUMENT_ROOT']."/Controller/questionController.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/Controller/alternativeController.php";
    $questionController=new questionController;
    $alternativeController=new alternativeController;
    $questionController->contents['request']=['print'=>false,'criteria'=>'answered'];
    $alternativeController->contents['request']=['print'=>false,'criteria'=>'form'];
    $Question_json=$questionController->read();
    $Alternative_json=$alternativeController->read();

    $title="Cadastro resposta";
    $object_name="answer";
    $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
    $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
    $content=$_SERVER['DOCUMENT_ROOT'].'/View/template/form.php';
    $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/body.php';
    
    
    $sessao=$_SESSION;
    unset($sessao['user_type']);
    $jScript='<script src="/js/form-to-'.($_SESSION['user_type']<3?'review':'answer').'.js"></script>
    <script>ListQuestion('.json_encode($Question_json).','.json_encode($Alternative_json).')</script>
    <script>var modal_user='.json_encode($sessao).'</script>';
    $QuestionMarkImg='/images/help/form-to-'.($_SESSION['user_type']<3?'review':'answer').'.jpg';

    $modal=[$this->modal['user-modal']];
    include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
  }
}
new answerController();