<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/user_type.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/user_typeDAO.php'; 

final class user_typeController extends utilController{

  function __construct(){
      $this->VAR_ATTR=user_type::VAR_ATTR;
      $json=json_decode(file_get_contents("php://input"), true);
      $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
      foreach ($this->contents as $key => $value)
        if (gettype($value)==='string')
            $this->contents[$key]=trim($value);
  }

  public function create(){
      $DAO = new user_typeDAO;
      $message='criar tipo de usuário';
      $attrs=[
        'description',
        'type',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      if ($validation['success']==true)
          $result=$DAO->create($this->setObj());
      $this->print_response($result);
  }

  public function read(){
      $DAO = new user_typeDAO;
      $result=$DAO->read();
      switch ($this->contents['request']['print']) {
        case true:
            $this->print_response($result);
            break;
        case false:
            return $result;
            break;
        default:
            return $result=[
                'success' => false,
                'message' => 'Falha ao obter usuário',
                'response' => 'Impressão indefinida',
            ];
      }
  }

  public function update(){
      $DAO = new user_typeDAO;
      $message='atualizar tipo de usuário';
      $validation=$this->validation($message);
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->update($this->setObj());
      $this->print_response($result);
  }

  public function delete(){
      $DAO = new user_typeDAO;
      $result=$DAO->delete($this->setObj());
      $this->print_response($result);
  }

  protected function setObj(){
        $user_type = new user_type;
        $user_type -> setId_user_type(isset($this->contents["id_user_type"]) ? ($this->contents["id_user_type"]):null);
        $user_type -> setDescription(isset($this->contents["description"]) ? ($this->contents["description"]):null);
        $user_type -> setType(isset($this->contents["type"]) ? ($this->contents["type"]):null);
        return $user_type;
  }

  public function getIdStudent(){
    $DAO = new user_typeDAO;
    $result=$DAO->getIdStudent();
    return $result;
  }

  public function View(){
    $title="Cadastro tipo de usuário";
    $object_name="user_type";
    $list=$this->read();
    $list['format']=$this->VAR_ATTR;
    $jScript='<script> list('.json_encode($list).',`user_type`)</script>';
    include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/viewCRUD.php';
  }
}
new user_typeController();