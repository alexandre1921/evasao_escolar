<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/course.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/courseDAO.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/templates.php';

final class courseController extends utilController{
  const VAR_ATTR=course::VAR_ATTR;

  function __construct(){
      $this->VAR_ATTR=course::VAR_ATTR;
      $this->modal=templates::get_Modals();
      $json=json_decode(file_get_contents("php://input"), true);
      $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
      foreach ($this->contents as $key => $value)
        if (gettype($value)==='string')
           $this->contents[$key]=trim($value);
  }

  public function create(){
      $DAO = new courseDAO;
      $message='criar curso';
      $attrs=[
        'name_course',
        'duration',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->create($this->setObj());
      $this->print_response($result);
  }

  public function read(){
      $DAO = new courseDAO;
      $result=$DAO->read($this->contents['request']['criteria']);
      switch ($this->contents['request']['print']) {
        case true:
            $this->print_response($result);
            break;
        case false:
            return $result;
            break;
        default:
            return $result=[
                'success' => false,
                'message' => 'Falha ao obter usuário',
                'response' => 'Impressão indefinida',
            ];
      }
  }

  public function update(){
      $DAO = new courseDAO;
      $message='atualizar curso';
      $attrs=[
        'name_course',
        'id_course',
        'duration',
        'status_course',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->update($this->setObj());
      $this->print_response($result);
  }

  public function delete(){
      $DAO = new courseDAO;
      $result=$DAO->delete($this->setObj());
      $this->print_response($result);
  }

  protected function setObj(){
      $course = new course;
      $course -> setId_course(isset($this->contents["id_course"]) ? ($this->contents["id_course"]):null);
      $course -> setName_course(isset($this->contents["name_course"]) ? ($this->contents["name_course"]):null);
      $course -> setDuration(isset($this->contents["duration"]) ? ($this->contents["duration"]):null);
      $course -> setStatus_course(isset($this->contents["status_course"]) ? ($this->contents["status_course"]):1);
      return $course;
  }

  public function View(){
    $title="Cadastro curso";
    $tableTitle='Lista dos cursos';
    $object_name="course";
    $this->contents['request']=['print'=>false,'criteria'=>'all'];
    $list=$this->read();
    $list['format']=$this->VAR_ATTR;
    $jScript='<script src="/js/table.js"></script>'.'<script> var jsonList='.json_encode($list).'; $(document).ready(function () {list(jsonList,`course`)});</script>';
    
    $sessao=$_SESSION;
    unset($sessao['user_type']);
    $jScript.='<script>var modal_user='.json_encode($sessao).'</script>';

    $menu=$_SERVER['DOCUMENT_ROOT'].'/View/template/menu.php';
    $account=$_SERVER['DOCUMENT_ROOT'].'/View/template/account.php';
    $page_wrapper=$_SERVER['DOCUMENT_ROOT'].'/View/template/body.php';
    $content=$_SERVER['DOCUMENT_ROOT'].'/View/template/table.php';
    $modal=[$this->modal['user-modal'],$this->modal['object-modal']];
    $UserSession=templates::get_UserSession();
    $QuestionMarkImg='/images/help/course.jpg';
    include_once $_SERVER['DOCUMENT_ROOT'].'/View/page/dashboard.php';
  }

}
new courseController();