-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Out-2018 às 00:46
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weka`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `jogartenis`
--

CREATE TABLE `jogartenis` (
  `cod` int(11) NOT NULL,
  `tempo` varchar(10) NOT NULL,
  `temperatura` varchar(10) NOT NULL,
  `humidade` varchar(10) NOT NULL,
  `vento` varchar(10) NOT NULL,
  `jogar` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `jogartenis`
--

INSERT INTO `jogartenis` (`cod`, `tempo`, `temperatura`, `humidade`, `vento`, `jogar`) VALUES
(1, '0', '0', '0', '1', 1),
(2, '0', '0', '0', '0', 1),
(3, '1', '0', '1', '1', 0),
(4, '2', ' 1', '0', '1', 0),
(5, '2', '2', '0', '1', 0),
(6, '1', '2', '0', '0', 1),
(7, '0', '1', '1', '1', 0),
(8, '0', '2', '1', '1', 1),
(9, '2', '1', '0', '0', 0),
(10, '0', '1', '0', '0', 0),
(11, '1', '1', '1', '1', 0),
(12, '1', '0', '1', '1', 0),
(13, '2', '1', '0', '0', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jogartenis`
--
ALTER TABLE `jogartenis`
  ADD PRIMARY KEY (`cod`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jogartenis`
--
ALTER TABLE `jogartenis`
  MODIFY `cod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
