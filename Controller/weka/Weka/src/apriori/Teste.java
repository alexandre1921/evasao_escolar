package apriori;

import weka.associations.Apriori;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.converters.ConverterUtils.DataSource;

public class Teste{
    public static void main(String[] args) throws Exception {
    	String local_storage = "superior1.arff";
    	double lowerBoundMinSupport = 0.1;
    	double upperBoundMinSupport = 1.0;
    	int numRules = 10;
    	int metricType = 0;//Confidence
    	double minMetric = 0.9;
    	for (int i=0; i<args.length; i++) {
    		switch (args[i]) {
	    		case "-t":
	    			local_storage=args[i+1];
	    		break;
				case "-lowerBoundMinSupport":
					lowerBoundMinSupport=Double.parseDouble(args[i+1]);
				break;
				case "-upperBoundMinSupport":
					upperBoundMinSupport=Double.parseDouble(args[i+1]);
				break;
				case "-numRules":
					numRules=Integer.parseInt(args[i+1]);
				break;
				case "-metricType":
					switch (args[i+1]) {
			    		case "Confidence":
			    			metricType=0;
			    		break;
						case "Lift":
							metricType=1;
						break;
						case "Leverage":
							metricType=2;
						break;
						case "Conviction":
							metricType=3;
						break;
		    		}
				break;
				case "-minMetric":
					minMetric=Double.parseDouble(args[i+1]);
				break;
    		}
    	}
    	if (DataSource.isArff(local_storage)) {
    		DataSource ds = new DataSource(local_storage);
			Instances data = ds.getDataSet();
			data.setClassIndex(data.numAttributes()-1);
	    	Apriori Apriori = new Apriori();
	    	Apriori.setLowerBoundMinSupport(lowerBoundMinSupport);
	    	Apriori.setUpperBoundMinSupport(upperBoundMinSupport);
	    	Apriori.setNumRules(numRules);
	    	SelectedTag metricTypeClass=new SelectedTag(metricType,weka.associations.Apriori.TAGS_SELECTION);
	    	Apriori.setMetricType(metricTypeClass);
	    	Apriori.setMinMetric(minMetric);
	    	Apriori.buildAssociations(data);
	    	System.out.println(Apriori.toString());
    	}else {
    		System.out.println("ERRO");
    		System.out.println("Arff não encontrado ou inválido");
    	}
	}
}