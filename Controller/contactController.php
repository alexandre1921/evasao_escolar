<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/contact.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/contactDAO.php'; 

final class contactController extends utilController{

  function __construct(){
      $this->VAR_ATTR=contact::VAR_ATTR;
      $json=json_decode(file_get_contents("php://input"), true);
      $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
      foreach ($this->contents as $key => $value)
        if (gettype($value)==='string')
            $this->contents[$key]=trim($value);
  }

  public function create(){
      $DAO = new contactDAO;
      $message='criar contato';
      $attrs=[
        'id_user',
        'telephone',
        'cellphone',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->create($this->setObj());
      $this->print_response($result);
  }

  public function read(){
      $DAO = new contactDAO;
      $result=$DAO->read();
      switch ($this->contents['request']['print']) {
        case true:
            $this->print_response($result);
            break;
        case false:
            return $result;
            break;
        default:
            return $result=[
                'success' => false,
                'message' => 'Falha ao obter contato',
                'response' => 'Impressão indefinida',
            ];
      }
  }

  public function update(){
      $DAO = new contactDAO;
      $message='atualizar contato';
      $attrs=[
        'id_contact',
        'telephone',
        'cellphone',
        'id_contact',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->update($this->setObj());
      $this->print_response($result);
  }

  public function delete(){
      $DAO = new contactDAO;
      $result=$DAO->delete($this->setObj());
      $this->print_response($result);
  }

  protected function setObj(){
      $contact = new contact;
      $contact -> setId_contact(isset($this->contents["id_contact"]) ? ($this->contents["id_contact"]):null);
      $contact -> setTelephone(isset($this->contents["telephone"]) ? ($this->contents["telephone"]):null);
      $contact -> setCellphone(isset($this->contents["cellphone"]) ? ($this->contents["cellphone"]):null);
      $contact -> setId_user(isset($this->contents["id_user"]) ? ($this->contents["id_user"]):null);
      $contact -> setStatus(isset($this->contents["status"]) ? ($this->contents["status"]):1);
      return $contact;
  }
}
new contactController();