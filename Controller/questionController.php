<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/Controller/utilController.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/Model/question.class.php'; 
include_once $_SERVER['DOCUMENT_ROOT'].'/DAO/questionDAO.php'; 

final class questionController extends utilController{
    
    function __construct(){
        $this->VAR_ATTR=question::VAR_ATTR;
        $json=json_decode(file_get_contents("php://input"), true);
        $this->contents=array_merge($json!=null?$json:[],$_GET!=null?$_GET:[]);
        foreach ($this->contents as $key => $value)
            if (gettype($value)==='string')
                $this->contents[$key]=trim($value);
    }

  public function create(){
      $DAO = new questionDAO;
      $message='criar questão';
      $attrs=[
        'statement',
      ];
      $validation=$this->validation($message,$this->getCastAttr($attrs));
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->create($this->setObj());
      $this->print_response($result);
  }

  public function read(){
        $DAO = new questionDAO;
        $result=$DAO->read($this->contents['request']['criteria']);
        switch ($this->contents['request']['print']) {
            case true:
                $this->print_response($result);
                break;
            case false:
                // code...
                break;
            default:
                $result=[
                    'success' => false,
                    'message' => 'Falha ao obter usuário',
                    'response' => 'Impressão indefinida',
                ];
        }
    return $result;
  }

  public function update(){
      $DAO = new questionDAO;
      $message='atualizar questão';
      $validation=$this->validation($message);
      $result=$validation;
      if ($validation['success']==true)
          $result=$DAO->update($this->setObj());
      $this->print_response($result);
  }

  public function delete(){
      $DAO = new questionDAO;
      $result=$DAO->delete($this->setObj());
      $this->print_response($result);
  }

  protected function setObj(){
        $question = new question;
        $question -> setId_question(isset($this->contents["id_question"]) ? ($this->contents["id_question"]):null);
        $question -> setStatement(isset($this->contents["statement"]) ? ($this->contents["statement"]):null);
        $question -> setStatus(isset($this->contents["status"]) ? ($this->contents["status"]):1);
        return $question;
  }
}
new questionController();